import os
from PIL import Image

#platzhalter = "Death_Screen_klein" # hier die dateinamen ohne png hereinschreiben

h = open("sprites.h", "w")
c = open("sprites.c", "w")

h.write("#ifndef SPRITES_H\n")
h.write("#define SPRITES_H\n")
h.write("\n")
h.write("#include <avr/pgmspace.h>\n")
h.write("\n")
h.write("#include <inttypes.h>\n")
h.write("\n")
h.write("#include <avr/pgmspace.h>\n")
h.write("\n")
c.write("#include \"sprites.h\"\n")
c.write("\n")


for dirpath, dirnames, filenames in os.walk('../Sprites'):
	dirnames.sort()
	filenames.sort()
	for filename in filenames:
		base, extension = os.path.splitext(filename)
		if extension != '.png':
			continue
	
		img = Image.open(base + extension)
		pixels = img.getdata()

		u1 = '00' #unterstes bit
		u2 = 'a'
		o1 = 'a' 
		o2 = 'a' #oberstes bit
		temp = 'a'
		j = 0
		z = -1

		width, height = img.size
		height = height
		
		
		c.write("const uint8_t " + base + "[" + str(height /4) + "][" + str(width) + "] PROGMEM = {\n")
		c.write("	{")
		h.write("extern const uint8_t " + base + "[" + str(height /4) + "][" + str(width) + "] PROGMEM ;\n")
		
		sprite = [[]]

		for i in range (0,width * (height / 4)):
			#if(i % 10 == 0):
			#	print(i)
			#print(pixels[i][0])
			#if(i == width * height - 3 * width):
			#	break
			if(i % width == 0):
				if(i == 0):
					j = 0 
				else:
					j = j + 3 * width
					c.write("},\n")
					c.write("	{")
					
			#print(j)
			o2 = pixels[j][0]
			o1 = pixels[j + width][0]
			u2 = pixels[j + 2 * width][0]
			u1 = pixels[j + 3 * width][0]	
			
			if(o2 == 255):
				o2 = '00'	# weiss
			elif(o2 == 0):
				o2 = '11'  # schwarz
			elif(o2 < 255 and o2 > 100):
				o2 = '01'  # hellgrau
			elif(o2 > 0 and o2 < 101):
				o2 = '10'  # dunkelgrau

			if(o1 == 255):
				o1 = '00'	# weiss
			elif(o1 == 0):
				o1 = '11'  # schwarz
			elif(o1 < 255 and o2 > 100):
				o1 = '01'  # hellgrau
			elif(o1 > 0 and o1 < 101):
				o1 = '10'  # dunkelgrau

			if(u2 == 255):
				u2 = '00'	# weiss
			elif(u2 == 0):
				u2 = '11'  # schwarz
			elif(u2 < 255 and u2 > 100):
				u2 = '01'  # hellgrau
			elif(u2 > 0 and u2 < 101):
				u2 = '10'  # dunkelgrau

			if(u1 == 255):
				u1 = '00'	# weiss
			elif(u1 == 0):
				u1 = '11'  # schwarz
			elif(u1 < 255 and u1 > 100):
				u1 = '01'  # hellgrau
			elif(u1 > 0 and u1 < 101):
				u1 = '10'  # dunkelgrau
			temp = u1+u2+o1+o2
			#print(hex(int(temp, 2)))
			
			temp = hex(int(temp, 2))
			if(i % width != width - 1):
				c.write(temp+",")
			else:
				c.write(temp)
			j += 1

		c.write("}};\n")
		c.write("\n")



h.write("\n")
h.write("#endif")
c.close()
h.close()
