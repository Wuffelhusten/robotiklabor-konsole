#include <stdio.h>
//#include <stdlib.h>
#include <inttypes.h>

#define map_x_length 15
#define map_y_height 13

typedef enum {
    PLAYER = 255,
    INDESTRUCTIBLE = 254,
    DESTRUCTIBLE = 253,
    TRAP = 252,
    FREE = 251,
	WHITE = 250,
	DOORCLOSED = 248

} FieldType;


typedef struct {
	uint8_t ID;									// unique ID for every room
	uint8_t type;								// saves the type of the room, used to check if rooms which should be unique on a level are existing or not
	uint8_t size[2];							// first value is the width of the room; second value is the height of the room
	uint8_t startPos[2];						// first drawed field of the room (top left wall edge)
	uint8_t endPos[2];							// last drawed field of the room (bottom right wall edge)
	uint8_t doors;								// every room has a random amount of doors but allways the door back to the room where the player came from
	uint8_t map[map_x_length][map_y_height];	// describes all fields of the playable field of the display; fields outside the room size are WHITE
} Room;


typedef struct {uint8_t posX; uint8_t posY;} Player;
typedef struct {uint8_t id; uint8_t posX; uint8_t posY; uint8_t size[2];} Enemy;

Room generateRoom();
void writeRoomFields(Room* room);
void moveEnemy(Enemy* e, Room* r, Player* p);
void buildGrid(const Room* room);

int main(void){
    Room room = generateRoom();
	Player p;
	p.posX = map_x_length / 2;
	p.posY = map_y_height / 2;
	room.map[p.posX][p.posY] = PLAYER;
	Enemy e = {1,3,3 , {2,2}};
	uint8_t k,j;
	for(k = 0;k<e.size[0];k++){
		for(j=0;j<e.size[1];j++){
			room.map[e.posX+k][e.posY+j] = e.id;		
		}
	}
	

	//obstacles
	room.map[5][5] = DESTRUCTIBLE;
	room.map[6][4] = DESTRUCTIBLE;
	room.map[7][3] = DESTRUCTIBLE;
	// draw the room
	printf("original:\n");
	buildGrid(&room);
	uint8_t i = 0;
	//while(abs(e.posX - p.posX) > 0 || abs(e.posY - p.posY) > 0){
	//i++;
	for(i=0; i<10; i++){	
		printf("move %d:\n", i+1);	
		moveEnemy(&e,&room,&p);
		buildGrid(&room);
	}
	

return 0;
}

uint8_t abs(int x){
	return x < 0 ? -x : x;
}
uint8_t sign(int x){
	return x < 0 ? -1 : x > 0 ? 1 : 0;
}


void moveEnemy(Enemy* e, Room* r, Player* p){
	int xDiff = p->posX - e->posX; //diff between the player & the enemy. >0 -> player is right of enemy
	int yDiff = p->posY - e->posY; // --> xDiff,yDiff is the vector pointing to the player
	uint8_t x,y,moveOk = 0;
	printf("xDiff: %d   yDiff: %d \n",xDiff,yDiff);
	if(abs(yDiff) > abs(xDiff)){ // try to move vertical
		moveOk = 1;
		for(x=0; x< e->size[0];x++){
			for(y=0;y<e->size[1];y++){
				if( r->map[e->posX+x][e->posY+sign(yDiff)+y] != FREE && r->map[e->posX+x][e->posY+sign(yDiff)+y] != e->id){
					moveOk = 0;
				}
				if(r->map[e->posX+x][e->posY+sign(yDiff)+y] == PLAYER){
					printf("HIT!\n");
					return;
				}
			}
		}
		if(moveOk){ // move
			for(x=0;x< e->size[0]; x++){
				for(y=0;y<e->size[1];y++){
					r->map[e->posX+x][e->posY+y] = FREE;
				}
			}
			for(x=0;x< e->size[0]; x++){
				for(y=0; y< e->size[1]; y++){
					r->map[e->posX+x][e->posY+sign(yDiff)+y] = e->id;
				}
			}
			e->posY+= sign(yDiff);

		}else { //best move not possible, try 2. best -> horizontal
			moveOk = 1;
			for(x=0; x< e->size[0];x++){
				for(y=0;y<e->size[1];y++){
					if( r->map[e->posX +sign(xDiff) +x][e->posY+y] != FREE && r->map[e->posX +sign(xDiff) +x][e->posY+y] != e->id){
						moveOk = 0;
					}
					if(r->map[e->posX +sign(xDiff) +x][e->posY+y] == PLAYER){
						printf("HIT!\n");
						return;
					}
				}
			}
			if(moveOk){ // move
				for(x=0;x< e->size[0]; x++){
					for(y=0;y<e->size[1];y++){
						r->map[e->posX+x][e->posY+y] = FREE;
					}
				}
				for(x=0;x< e->size[0]; x++){
					for(y=0; y< e->size[1]; y++){
						r->map[e->posX +sign(xDiff) +x][e->posY+y] = e->id;
					}
				}
				e->posX+= sign(xDiff);

			}else { //try 3. best -> horizontal but wrong direction
				moveOk = 1;
				for(x=0; x< e->size[0];x++){
					for(y=0;y<e->size[1];y++){
						if( r->map[e->posX -sign(xDiff) +x][e->posY+y] != FREE && r->map[e->posX -sign(xDiff) +x][e->posY+y] != e->id){
							moveOk = 0;
						}
						if(r->map[e->posX -sign(xDiff) +x][e->posY+y] == PLAYER){
							printf("HIT!\n");
							return;
						}
					}
				}
				if(moveOk){ // move
					for(x=0;x< e->size[0]; x++){
						for(y=0;y<e->size[1];y++){
							r->map[e->posX+x][e->posY+y] = FREE;
						}
					}
					for(x=0;x< e->size[0]; x++){
						for(y=0; y< e->size[1]; y++){
							r->map[e->posX -sign(xDiff) +x][e->posY+y] = e->id;
						}
					}
					e->posX-= sign(xDiff);
				}else { // try the worst move -> vertical but wrong direction
					moveOk = 1;
					for(x=0; x< e->size[0];x++){
						for(y=0;y<e->size[1];y++){
							if( r->map[e->posX+x][e->posY-sign(yDiff)+y] != FREE && r->map[e->posX+x][e->posY-sign(yDiff)+y] != e->id){
								moveOk = 0;
							}
							if(r->map[e->posX+x][e->posY-sign(yDiff)+y] == PLAYER){
								printf("HIT!\n");
								return;
							}
						}
					}
					if(moveOk){ // move
						for(x=0;x< e->size[0]; x++){
							for(y=0;y<e->size[1];y++){
								r->map[e->posX+x][e->posY+y] = FREE;
							}
						}
						for(x=0;x< e->size[0]; x++){
							for(y=0; y< e->size[1]; y++){
								r->map[e->posX+x][e->posY-sign(yDiff)+y] = e->id;
							}
						}
						e->posY-= sign(yDiff);
					}else{ // can't move. Dont move
						printf("BLOCKED!\n");
					}
				}

			}

		}
	}else if(abs(yDiff) <= abs(xDiff)){ // try to move horizontal
		moveOk = 1;
		for(x=0; x< e->size[0];x++){
			for(y=0;y<e->size[1];y++){
				if( r->map[e->posX +sign(xDiff) +x][e->posY+y] != FREE && r->map[e->posX +sign(xDiff) +x][e->posY+y] != e->id){
					moveOk = 0;
				}
				if(r->map[e->posX +sign(xDiff) +x][e->posY+y] == PLAYER){
					printf("HIT!\n");
					return;
				}
			}
		}
		if(moveOk){ // move
			for(x=0;x< e->size[0]; x++){
				for(y=0;y<e->size[1];y++){
					r->map[e->posX+x][e->posY+y] = FREE;
				}
			}
			for(x=0;x< e->size[0]; x++){
				for(y=0; y< e->size[1]; y++){
					r->map[e->posX +sign(xDiff) +x][e->posY+y] = e->id;
				}
			}
			e->posX+= sign(xDiff);

		}else { //best move not possible, try 2. best -> vertical
			moveOk = 1;
			for(x=0; x< e->size[0];x++){
				for(y=0;y<e->size[1];y++){
					if( r->map[e->posX+x][e->posY +sign(yDiff) +y] != FREE && r->map[e->posX+x][e->posY +sign(yDiff) +y] != e->id){
						moveOk = 0;
					}
					if(r->map[e->posX+x][e->posY +sign(yDiff) +y] == PLAYER){
						printf("HIT!\n");
						return;
					}
				}
			}
			if(moveOk){ // move
				for(x=0;x< e->size[0]; x++){
					for(y=0;y<e->size[1];y++){
						r->map[e->posX+x][e->posY+y] = FREE;
					}
				}
				for(x=0;x< e->size[0]; x++){
					for(y=0; y< e->size[1]; y++){
						r->map[e->posX+x][e->posY +sign(yDiff) +y] = e->id;
					}
				}
				e->posY+= sign(yDiff);

			}else { //try 3. best -> vertical but wrong direction
				moveOk = 1;
				for(x=0; x< e->size[0];x++){
					for(y=0;y<e->size[1];y++){
						if( r->map[e->posX+x][e->posY -sign(yDiff) +y] != FREE && r->map[e->posX+x][e->posY -sign(yDiff) +y] != e->id){
							moveOk = 0;
						}
						if(r->map[e->posX+x][e->posY -sign(yDiff) +y] == PLAYER){
							printf("HIT!\n");
							return;
						}
					}
				}
				if(moveOk){ // move
					for(x=0;x< e->size[0]; x++){
						for(y=0;y<e->size[1];y++){
							r->map[e->posX+x][e->posY+y] = FREE;
						}
					}
					for(x=0;x< e->size[0]; x++){
						for(y=0; y< e->size[1]; y++){
							r->map[e->posX+x][e->posY -sign(yDiff) +y] = e->id;
						}
					}
					e->posY-= sign(yDiff);
				}else { // try the worst move -> horizontal but wrong direction
					moveOk = 1;
					for(x=0; x< e->size[0];x++){
						for(y=0;y<e->size[1];y++){
							if( r->map[e->posX -sign(xDiff) +x][e->posY+y] != FREE && r->map[e->posX -sign(xDiff) +x][e->posY+y] != e->id){
								moveOk = 0;
							}
							if(r->map[e->posX -sign(xDiff) +x][e->posY+y] == PLAYER){
								printf("HIT!\n");
								return;
							}
						}
					}
					if(moveOk){ // move
						for(x=0;x< e->size[0]; x++){
							for(y=0;y<e->size[1];y++){
								r->map[e->posX+x][e->posY+y] = FREE;
							}
						}
						for(x=0;x< e->size[0]; x++){
							for(y=0; y< e->size[1]; y++){
								r->map[e->posX -sign(xDiff) +x][e->posY+y] = e->id;
							}
						}
						e->posX-= sign(xDiff);
					}else{ // can't move. Dont move
						printf("BLOCKED!\n");
					}
				}

			}

		}
	}

}

Room generateRoom() {
	Room room;

	room.ID = 0;
	room.size[0] = 11;
	room.size[1] = 9;
	room.doors = 2 + 1; //oben rechts

	room.startPos[0] = (map_x_length - room.size[0]) / 2;
	room.startPos[1] = (map_y_height - room.size[1]) / 2;
	room.endPos[0] = (map_x_length + room.size[0]) / 2 - 1;
	room.endPos[1] = (map_y_height + room.size[1]) / 2 - 1;

	writeRoomFields(&room);
	//buildGrid(&room);

	return room;
}


void writeRoomFields(Room* room) {
	uint8_t x, y;

    uint8_t doors = room->doors;
    
	// set white fields outside the room
	for (x = 0; x < map_x_length; x++) {
		for (y = 0; y < map_y_height; y++) {
			room->map[x][y] = WHITE;
		}
	}

	// set the horizontal walls of the room
	for (x = room->startPos[0]; x <= room->endPos[0]; x++) {
		room->map[x][room->startPos[1]] = INDESTRUCTIBLE;
		room->map[x][room->endPos[1]] = INDESTRUCTIBLE;
	}

	// set the vertical walls of the room
	for (y = room->startPos[1]; y <= room->endPos[1]; y++) {
		room->map[room->startPos[0]][y] = INDESTRUCTIBLE;
		room->map[room->endPos[0]][y] = INDESTRUCTIBLE;
	}

	// set the inside of the room
	for (x = room->startPos[0] + 1; x < room->endPos[0]; x++) {
		for (y = room->startPos[1] + 1; y < room->endPos[1]; y++) {
			room->map[x][y] = FREE;
		}
	}

	// set the door fields
	if (doors >= 8) { // unten
		room->map[(map_x_length - 1) / 2][room->endPos[1]] = DOORCLOSED;
		doors -= 8;
	}
	if (doors >= 4) { // links
		room->map[room->startPos[0]][(map_y_height - 1) / 2] = DOORCLOSED;
		doors -= 4;
	}
	if (doors >= 2) { // oben
		room->map[(map_x_length - 1) / 2][room->startPos[1]] = DOORCLOSED;
		doors -= 2;
	}
	if (doors >= 1) { // rechts
		room->map[room->endPos[0]][(map_y_height - 1) / 2] = DOORCLOSED;
		doors -= 1;
	}
}


void buildGrid(const Room* room) {
	uint8_t i, j;
	for (j = 0; j < map_y_height; j++) {
		for (i = 0; i < map_x_length; i++) {
			if(room->map[i][j] == FREE){
				printf(". ");
			}else if(room->map[i][j] == PLAYER){
				printf("P ");
			}else if(room->map[i][j] == INDESTRUCTIBLE){
				printf("X ");
			}else if(room->map[i][j] == DESTRUCTIBLE){
				printf("D ");
			}else if(room->map[i][j] == TRAP){
				printf("T ");
			}else if(room->map[i][j] == DOORCLOSED){
				printf("C ");
			}else if(room->map[i][j] == WHITE){
				printf("W ");
			}else{ // Enemy!
				printf("E ");
			}
		}
		printf("\n");
	}
}


/*
int convertToMapX(uint8_t i){
    return (int) (i % map_x_length);
}
int convertToMapY(uint8_t i){
    return (int) (i / map_x_length);
}
uint8_t convertToPathIndex(uint8_t x, uint8_t y){
    return y * map_x_length + x; 
}

#define INF 255
#define noNodes 195 // map_x_lenght * map_y_height

void dijkstra(const Room* room, Enemy* enemy, const Player* player ){
    
    uint8_t cost[noNodes][noNodes]; // cost[i][j] = kosten von node i zu j. ; i,j =  posY * map_x_lenght + posX
    uint8_t distance[noNodes],predecessor[noNodes],isVisited[noNodes], mindistance, count, nextnode;
    uint8_t i,j; 
    uint8_t startNode = convertToPathIndex(enemy.posX,enemy.posY);   
    // init cost matrix
    for(i=0; i < noNodes; i++){
        for(j=0; j< noNodes; j++){
            // knoten nicht benachbart: x1 - x2 < -1 || x2 - x1 < -1 || y1...
            if((convertToMapX(i)-convertToMapX(j) < -1) || (convertToMapX(j)-convertToMapX(j) < -1) || (convertToMapY(i)-convertToMapY(j) < -1) || (convertToMapY(j)-convertToMapY(j) < -1)){ 
                cost[i][j] = INF;
            }else {
                cost[i][j] = 1; // weg länge zw nachbarn = 1
            }
        }
    }
    // init pred -> vorgänger: hier startknoten enemypos; 
    // init isVisited 0
    // init distance[i] as cost startknoten zu i
    for(i=0; i < noNodes; i++){
        predecessor[i] = startNode;
        distance[i] = cost[startNode][i];
        visited[i] = 0;
    }
    // init startNode stuff
    distance[startNode] = 0;
    visited[startnode] = 1;    
	count = 1;

	while(count< noNodes-1){
		minDistance = INF;
		//nextnode -> node at minDistance
		for(i = 0; i<noNodes; i++){
			if(distance[i]<minDistance && !visited[i]){
				minDistance
			}
		}
	}


}
*/





