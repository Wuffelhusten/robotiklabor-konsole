import os
from PIL import Image

c = open("alphabet.c", "w")
#start of drawLetter
c.write("uint8_t drawletter(uint8_t posX, uint8_t posY, char letter){\n")
c.write("	uint8_t returnValue = 0;\n")
c.write("	switch(letter)\n")
c.write("	{\n")

for dirpath, dirnames, filenames in os.walk('../Alphabet'):
	dirnames.sort()
	filenames.sort()
	for filename in filenames:
		base, extension = os.path.splitext(filename)
		if extension != '.png':
			continue
	
		img = Image.open(base + extension)
		pixels = img.getdata()

		u1 = '00' #unterstes bit
		u2 = 'a'
		o1 = 'a' 
		o2 = 'a' #oberstes bit
		temp = 'a'
		j = 0
		z = -1
		addedToX = 0
		addedToY = 0

		width, height = img.size
		height = height
		caseNameSplit = base.split("_")
		caseName = caseNameSplit[1]
		
		c.write("		case " + "\'" + caseName + "\' : returnValue = " + str(width) + " + 1; " )

		for i in range (0,width * (height / 4)):
			
			if(i % width == 0):
				if(i == 0):
					j = 0 
				else:
					j = j + 3 * width
					c.write("\n            ")
					addedToX = 0
					addedToY += 1
					
			#print(j)
			o2 = pixels[j][0]
			o1 = pixels[j + width][0]
			u2 = pixels[j + 2 * width][0]
			u1 = pixels[j + 3 * width][0]	
			
			if(o2 == 255):
				o2 = '00'	# weiss
			elif(o2 == 0):
				o2 = '11'  # schwarz
			elif(o2 < 255 and o2 > 100):
				o2 = '01'  # hellgrau
			elif(o2 > 0 and o2 < 101):
				o2 = '10'  # dunkelgrau

			if(o1 == 255):
				o1 = '00'	# weiss
			elif(o1 == 0):
				o1 = '11'  # schwarz
			elif(o1 < 255 and o2 > 100):
				o1 = '01'  # hellgrau
			elif(o1 > 0 and o1 < 101):
				o1 = '10'  # dunkelgrau

			if(u2 == 255):
				u2 = '00'	# weiss
			elif(u2 == 0):
				u2 = '11'  # schwarz
			elif(u2 < 255 and u2 > 100):
				u2 = '01'  # hellgrau
			elif(u2 > 0 and u2 < 101):
				u2 = '10'  # dunkelgrau

			if(u1 == 255):
				u1 = '00'	# weiss
			elif(u1 == 0):
				u1 = '11'  # schwarz
			elif(u1 < 255 and u1 > 100):
				u1 = '01'  # hellgrau
			elif(u1 > 0 and u1 < 101):
				u1 = '10'  # dunkelgrau
			temp = u1+u2+o1+o2 #baut die page auf
			
			temp = hex(int(temp, 2))
			c.write("page(posX + " + str(addedToX) + ", posY + " + str(addedToY) + ", " + temp + ");")

			j += 1
			addedToX += 1

		c.write("break;\n")

c.write("		default: returnValue = 4; break;\n")
c.write("	}\n")
c.write("	return returnValue;\n")
c.write("}\n")
#end of drawLetter

#start of drawText
c.write("void drawText(uint8_t posX, uint8_t posY, char text[], uint8_t textLength){\n")
c.write("	uint8_t i = 0;\n")
c.write("	uint8_t tempPos = posX\n")
c.write("	for(i = 0; i < textLength; i++){\n")
c.write("		tempPos += drawLetter(tempPos, posY, text[i])\n")
c.write("	}\n")
c.write("}\n")
#end of drawText


c.close()	#end of Filestream
