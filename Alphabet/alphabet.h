#ifndef SPRITES_H
#define SPRITES_H

#include <inttypes.h>

extern const uint8_t alphabet_0[2][4];
extern const uint8_t alphabet_1[2][4];
extern const uint8_t alphabet_2[2][4];
extern const uint8_t alphabet_3[2][4];
extern const uint8_t alphabet_4[2][4];
extern const uint8_t alphabet_5[2][4];
extern const uint8_t alphabet_6[2][4];
extern const uint8_t alphabet_7[2][4];
extern const uint8_t alphabet_8[2][4];
extern const uint8_t alphabet_9[2][4];
extern const uint8_t alphabet_A[2][4];
extern const uint8_t alphabet_B[2][4];
extern const uint8_t alphabet_C[2][3];
extern const uint8_t alphabet_D[2][4];
extern const uint8_t alphabet_E[2][4];
extern const uint8_t alphabet_F[2][4];
extern const uint8_t alphabet_G[2][4];
extern const uint8_t alphabet_H[2][4];
extern const uint8_t alphabet_I[2][1];
extern const uint8_t alphabet_J[2][3];
extern const uint8_t alphabet_K[2][4];
extern const uint8_t alphabet_L[2][3];
extern const uint8_t alphabet_M[2][5];
extern const uint8_t alphabet_N[2][5];
extern const uint8_t alphabet_O[2][3];
extern const uint8_t alphabet_P[2][3];
extern const uint8_t alphabet_Q[2][4];
extern const uint8_t alphabet_R[2][4];
extern const uint8_t alphabet_S[2][4];
extern const uint8_t alphabet_T[2][5];
extern const uint8_t alphabet_U[2][4];
extern const uint8_t alphabet_V[2][5];
extern const uint8_t alphabet_W[2][5];
extern const uint8_t alphabet_X[2][5];
extern const uint8_t alphabet_Y[2][5];
extern const uint8_t alphabet_Z[2][4];

#endif