uint8_t drawletter(uint8_t posX, uint8_t posY, char letter){
	uint8_t returnValue = 0;
	switch(letter)
	{
		case '!' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x3);break;
		case '$' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xf0);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0xff);page(posX + 3, posY + 0, 0xc);page(posX + 4, posY + 0, 0xc);
            page(posX + 0, posY + 1, 0xf);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0xff);page(posX + 3, posY + 1, 0x30);page(posX + 4, posY + 1, 0x30);break;
		case '.' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0x0);
            page(posX + 0, posY + 1, 0x3);break;
		case '0' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '1' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x30);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0xff);page(posX + 3, posY + 0, 0x0);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x3f);page(posX + 3, posY + 1, 0x30);break;
		case '2' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xc3);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x30);break;
		case '3' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '4' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc0);page(posX + 2, posY + 0, 0xc0);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3f);break;
		case '5' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xc3);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '6' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xc3);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '7' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3f);break;
		case '8' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '9' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case ':' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0xcc);
            page(posX + 0, posY + 1, 0x0);break;
		case 'A' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'B' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xcc);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);break;
		case 'C' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'D' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);page(posX + 3, posY + 0, 0xfc);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);break;
		case 'E' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'F' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);break;
		case 'G' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xf3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'H' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x30);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'I' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);break;
		case 'J' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xc3);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'K' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x30);page(posX + 2, posY + 0, 0xcc);page(posX + 3, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'L' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x0);page(posX + 2, posY + 0, 0x0);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'M' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xc);page(posX + 4, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'N' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xc0);page(posX + 4, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'O' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'P' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);break;
		case 'Q' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'R' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0xf3);page(posX + 3, posY + 0, 0xc);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'S' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3f);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xf3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'T' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xff);page(posX + 3, posY + 0, 0x3);page(posX + 4, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x0);break;
		case 'U' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x0);page(posX + 2, posY + 0, 0x0);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'V' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3f);page(posX + 1, posY + 0, 0xc0);page(posX + 2, posY + 0, 0x0);page(posX + 3, posY + 0, 0xc0);page(posX + 4, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x0);break;
		case 'W' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc0);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xc0);page(posX + 4, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'X' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xcc);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xcc);page(posX + 4, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'Y' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0xf0);page(posX + 3, posY + 0, 0xc);page(posX + 4, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x0);break;
		case 'Z' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xf);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		default: returnValue = 4; break;
	}
	return returnValue;
}
void drawText(uint8_t posX, uint8_t posY, char text[], uint8_t textLength){
	uint8_t i = 0;
	uint8_t tempPos = posX
	for(i = 0; i < textLength; i++){
		tempPos += drawLetter(tempPos, posY, text[i])
	}
}
