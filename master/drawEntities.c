#include <inttypes.h>
#include <util/delay.h>
#include "drawEntities.h"
#include "adc.h"
#include "uart.h"



void drawCharacter(const Player* p) {
	uint8_t posX = p->position[0] * columnsPerField;
	uint8_t posY = p->position[1] * rowsPerField;

	uint8_t i, j;
	for (i = 0; i < rowsPerField; i++) {
		for (j = 0; j < columnsPerField; j++) {
			page(posX + j, posY + i, p->sprite[i * columnsPerField + j]);
		}
	}
}


void drawEnemy(const Enemy* e, const uint8_t noOfMovements) {
    if (e->ID == DEADENEMY) {
        return;
    }
	uint8_t posX = e->position[0] * columnsPerField;
	uint8_t posY = e->position[1] * rowsPerField;

	uint8_t i, j;
	for (i = 0; i < e->size[1] * rowsPerField; i++) {
		for (j = 0; j < e->size[0] * columnsPerField; j++) {
			page(posX + j, posY + i, noOfMovements % 2 == 0 ?
				pgm_read_byte(e->sprite1 + i * e->size[0] * columnsPerField + j) : pgm_read_byte(e->sprite2 + i * e->size[0] * columnsPerField + j));
		}
	}
}

#define daggerPrice 10
#define swordPrice 60
#define spearPrice 130
#define bootsPrice 50
#define pantsPrice 100
#define helmetPrice 150
#define chestPlatePrice 200
#define potionPrice 25
#define broadswordPrice 170


void drawMerchantMenu(Player* p, Room* room){
    //Merchant musik
    uart_putc('f'); // f for merchant musik

	deleteRectangleAt(15*8,13*8,0,0); // clear screen
	drawRectangleAt(4,13*8,0,0);    // left
	drawRectangleAt(4,13*8,116,0);  // right
	drawRectangleAt(15*8,4,0,0);    // up
	drawRectangleAt(15*8,4,0,25);  //down
	
	drawText((((map_x_length -1) / 2)) * 8 - 7 * 5, 2, "HEY KAUF ETWAS!", 15);
	drawText(10,23,"A: KAUFEN",9);
	drawText(10+10*5,23,"B: ZURUECK", 10);

	uint8_t frame1X = 20; uint8_t frame1Y = 4;
	drawMerchantItemFrame(frame1X,frame1Y);
	drawMerchantItemFrame(frame1X+30,frame1Y);
	drawMerchantItemFrame(frame1X+60, frame1Y);

	// generate merchants items
	// 0 = NONE, 1= Dagger, 2 = SWORD, 3 = SPEAR, 4 = Bots, 5 = PANTS, 6 = HELMET, 7 = CHESTPLATE, 8 = POTION, 9 = Broadsword
	
	uint8_t i;
	for(i=0;i<3;i++){
		drawIteminSlot(i, merchantItems[i], frame1X, frame1Y);
	}

	
	uint8_t selectionPos = 0;
	drawSelectionArrow(selectionPos);
	while(!(!(PIND&(1<<7)))){ // B_B
		if((!(PIND&(1<<5)))){ // B_RIGHT
			deleteRectangleAt(3,4,30 + 30 * selectionPos -1, 15); // delete the selectionArrow
			selectionPos = (selectionPos +1) % 3;
			drawSelectionArrow(selectionPos);
			
		}else if((!(PIND&(1<<4)))){ //B_LEFT
			if(selectionPos == 0){
				deleteRectangleAt(3,4,30 + 30 * selectionPos -1, 15); // delete the selectionArrow
				selectionPos = 2;
				drawSelectionArrow(selectionPos);
				
			}else{
				deleteRectangleAt(3,4,30 + 30 * selectionPos -1, 15); // delete the selectionArrow
				selectionPos = (selectionPos -1) % 3;
				drawSelectionArrow(selectionPos);
				
			}
		}else if((!(PIND & (1<<6)))){ //B_A
			if(merchantItems[selectionPos] == 1 && p->inventory.money >= daggerPrice){
				p->inventory.money -= daggerPrice;
				swapWeapon(p,DAGGER);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 2 && p->inventory.money >= swordPrice){
				p->inventory.money -= swordPrice;
				swapWeapon(p,SWORD);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 3 && p->inventory.money >= spearPrice){
				p->inventory.money -= spearPrice;
				swapWeapon(p,SPEAR);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 4 && p->inventory.money >= bootsPrice){
				p->inventory.money -= bootsPrice;
				swapArmor(p,BOOTS);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 5 && p->inventory.money >= pantsPrice){
				p->inventory.money -= pantsPrice;
				swapArmor(p,PANTS);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 6 && p->inventory.money >= helmetPrice){
				p->inventory.money -= helmetPrice;
				swapArmor(p,HELMET);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 7 && p->inventory.money >= chestPlatePrice){
				p->inventory.money -= chestPlatePrice;
				swapArmor(p,CHESTPLATE);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 8 && p->inventory.money >= potionPrice){
				p->inventory.money -= potionPrice;
				swapItem(p,POTION);
				merchantItems[selectionPos] = 0;
			}else if(merchantItems[selectionPos] == 9 && p->inventory.money >= broadswordPrice){
				p->inventory.money -= broadswordPrice;
				swapWeapon(p,BROADSWORD);
				merchantItems[selectionPos] = 0;
			}else{
				// either no item or not enough cash 
			}
			drawIteminSlot(selectionPos, merchantItems[selectionPos], frame1X, frame1Y);
			drawMoney(p->inventory.money);
		}
		_delay_ms(200);
	}
	/*room->map[p->position[0]][p->position[1]] = FREE;
	p->position[0]--;
	room->map[p->position[0]][p->position[1]] = PLAYER;*/
	buildGrid(room);
	drawMerchant(room, 0);
	drawCharacter(p);
	
	resetUart();
	_delay_ms(5);
	uart_putc('F'); // F for stop merchant musik
	
}
void drawMerchantItemFrame(const uint8_t posX, const uint8_t posY){
	uint8_t i;
	for(i=0; i<8; i++){ // left / right edge
		page(posX, posY+i, 0xFF);
		page(posX+21, posY+i,0xFF);
	}
	for(i=0; i<22; i++){ // up/down
		page(posX+i, posY, 0xC0);	
		page(posX+i, posY+8, 0x03);
	}
}
void drawSelectionArrow(const uint8_t selectionPos){
	page(30 + 30 * selectionPos -1, 15, 0xFC);
	page(30 + 30 * selectionPos, 15, 0xFF); // TODO: draw an arrow
	page(30 + 30 * selectionPos +1, 15, 0xFC);
}

// 0 = NONE, 1= Dagger, 2 = SWORD, 3 = SPEAR, 4 = Bots, 5 = PANTS, 6 = HELMET, 7 = CHESTPLATE, 8 = POTION, 9 = broadsword
void drawIteminSlot(const uint8_t selectionPos, const uint8_t item, const uint8_t frame1X, const uint8_t frame1Y){
	char price[4] = "000$";
	uint8_t i,j;
	if(item == 0){
		deleteRectangleAt(20,28,frame1X + selectionPos * 30 +1, frame1Y +1);
		drawText(frame1X + selectionPos* 30, frame1Y + 9, "      ", 6);
		return;
	}else if(item == 1){
		for (i = 0; i < 7; i++) { // sprite1
			for (j = 0; j < 17; j++) {
				page(frame1X + selectionPos* 30 + j +3, frame1Y + i +1, pgm_read_byte(&dagger_sprite[i][j]));
			}
		}
		price[0] = daggerPrice / 100 +'0';
		price[1]= daggerPrice / 10 % 10 +'0';
		price[2]= daggerPrice % 10 +'0';
	}else if(item == 2){
		for (i = 0; i < 7; i++) { // sprite1
			for (j = 0; j < 17; j++) {
				page(frame1X + selectionPos* 30 + j +3, frame1Y + i +1, pgm_read_byte(&sword_sprite[i][j]));
			}
		}
		price[0] = swordPrice / 100 +'0';
		price[1]= swordPrice / 10 % 10 +'0';
		price[2]= swordPrice % 10 +'0';
	}else if(item == 3){
		for (i = 0; i < 7; i++) { // sprite1
			for (j = 0; j < 17; j++) {
				page(frame1X + selectionPos* 30 + j +3, frame1Y + i +1, pgm_read_byte(&spear_sprite[i][j]));
			}
		}
		price[0] = spearPrice / 100 +'0';
		price[1]= spearPrice / 10 % 10 +'0';
		price[2]= spearPrice % 10 +'0';
	}else if(item == 4){
		for (i = 0; i < 3; i++) { // sprite1
			for (j = 0; j < 11; j++) {
				page(frame1X + selectionPos* 30 + j +5, frame1Y + i +3, pgm_read_byte(&boots_sprite[i][j]));
			}
		}
		price[0] = bootsPrice / 100 +'0';
		price[1]= bootsPrice / 10 % 10 +'0';
		price[2]= bootsPrice % 10 +'0';
	}else if(item == 5){
		for (i = 0; i < 3; i++) { // sprite1
			for (j = 0; j < 11; j++) {
				page(frame1X + selectionPos* 30 + j +6, frame1Y + i +3, pgm_read_byte(&pants_sprite[i][j]));
			}
		}
		price[0] = pantsPrice / 100 +'0';
		price[1]= pantsPrice / 10 % 10 +'0';
		price[2]= pantsPrice % 10 +'0';
	}else if(item == 6){
		for (i = 0; i < 3; i++) { // sprite1
			for (j = 0; j < 11; j++) {
				page(frame1X + selectionPos* 30 + j +6, frame1Y + i +3, pgm_read_byte(&helmet_sprite[i][j]));
			}
		}
		price[0] = helmetPrice / 100 +'0';
		price[1]= helmetPrice / 10 % 10 +'0';
		price[2]= helmetPrice % 10 +'0';
	}else if(item == 7){
		for (i = 0; i < 3; i++) { // sprite1
			for (j = 0; j < 11; j++) {
				page(frame1X + selectionPos* 30 + j +6, frame1Y + i +3, pgm_read_byte(&chestplate_sprite[i][j]));
			}
		}
		price[0] = chestPlatePrice / 100 +'0';
		price[1]= chestPlatePrice / 10 % 10 +'0';
		price[2]= chestPlatePrice % 10 +'0';
	}else if(item == 8){
		for (i = 0; i < 3; i++) { // sprite1
			for (j = 0; j < 11; j++) {
				page(frame1X + selectionPos* 30 + j +6, frame1Y + i +3, pgm_read_byte(&potion_sprite[i][j]));
			}
		}
		price[0] = potionPrice / 100 +'0';
		price[1]= potionPrice / 10 % 10 +'0';
		price[2]= potionPrice % 10 +'0';
	}else if(item == 9){
		for (i = 0; i < 7; i++) { // sprite1
			for (j = 0; j < 17; j++) {
				page(frame1X + selectionPos* 30 + j +3, frame1Y + i +1, pgm_read_byte(&broadsword_sprite[i][j]));
			}
		}
		price[0] = broadswordPrice / 100 +'0';
		price[1]= broadswordPrice / 10 % 10 +'0';
		price[2]= broadswordPrice % 10 +'0';
	}
	//drawText(frame1X + 30, frame1Y + 13, "TEST", 4);		
	drawText(frame1X + selectionPos* 30, frame1Y + 9, price, 4);
}

