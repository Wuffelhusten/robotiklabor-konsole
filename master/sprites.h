#ifndef SPRITES_H
#define SPRITES_H

#include <avr/pgmspace.h>
#include <inttypes.h>


// Screen elements
extern const uint8_t titleScreen[9][66] PROGMEM ;
extern const uint8_t deathScreen[11][67] PROGMEM ;
extern const uint8_t UIslime_sprite1[3][12] PROGMEM ;
extern const uint8_t bonfireBottom_sprite[4][43] PROGMEM ;
extern const uint8_t bonfireMiddle_sprite1[8][23] PROGMEM ;
extern const uint8_t bonfireMiddle_sprite2[8][23] PROGMEM ;
extern const uint8_t bonfireTop_sprite[3][6] PROGMEM ;
extern const uint8_t arrow[2][6] PROGMEM;
// Weapons
extern const uint8_t broadsword_sprite[7][17] PROGMEM ;
extern const uint8_t dagger_sprite[7][17] PROGMEM ;
extern const uint8_t spear_sprite[7][17] PROGMEM ;
extern const uint8_t sword_sprite[7][17] PROGMEM ;

// Items
extern const uint8_t potion_sprite[3][11] PROGMEM ;

// Armor
extern const uint8_t chestplate_sprite[3][11] PROGMEM ;
extern const uint8_t boots_sprite[3][11] PROGMEM ;
extern const uint8_t helmet_sprite[3][11] PROGMEM ;
extern const uint8_t pants_sprite[3][11] PROGMEM ;

// Special Room Parts
extern const uint8_t merchant_sprite1[6][24] PROGMEM ;
extern const uint8_t merchant_sprite2[6][24] PROGMEM ;
extern const uint8_t buyField_sprite[2][8] PROGMEM ;
extern const uint8_t table_sprite[2][8] PROGMEM ;
extern const uint8_t portal_sprite1[6][24] PROGMEM ;
extern const uint8_t portal_sprite2[6][24] PROGMEM ;
extern const uint8_t obstacle_sprite[2][8] PROGMEM ;
extern const uint8_t wall_sprite[2][8] PROGMEM ;
extern const uint8_t trap_sprite[2][8] PROGMEM ;

// Enemies
extern const uint8_t SlimeBoss_sprite1[6][24] PROGMEM ;
extern const uint8_t SlimeBoss_sprite2[6][24] PROGMEM ;
extern const uint8_t SlimeMiniBoss_sprite1[4][16] PROGMEM ;
extern const uint8_t SlimeMiniBoss_sprite2[4][16] PROGMEM ;
extern const uint8_t dragonBoss_sprite[6][24] PROGMEM ;
extern const uint8_t heavy_sprite1[4][16] PROGMEM ;
extern const uint8_t heavy_sprite2[4][16] PROGMEM ;
extern const uint8_t pacmanGhost_sprite1[2][8] PROGMEM ;
extern const uint8_t pacmanGhost_sprite2[2][8] PROGMEM ;
extern const uint8_t skeleton_sprite1[4][16] PROGMEM ;
extern const uint8_t skeleton_sprite2[4][16] PROGMEM ;
extern const uint8_t slime_sprite1[2][8] PROGMEM ;
extern const uint8_t slime_sprite2[2][8] PROGMEM ;


/*
const uint8_t table_sprite[2][8] PROGMEM = {
    {0x9a,0x9a,0xa6,0xa6,0xa6,0x9a,0x9a,0x9a},
    {0xa6,0xa6,0xa6,0xa6,0x9a,0x9a,0x9a,0xa6}};
    
const uint8_t wall_sprite[2][8] PROGMEM = {
    {0x6a,0x6a,0x6a,0x55,0x6a,0x6a,0x6a,0x6a},
    {0x6a,0x6a,0x6a,0x6a,0x6a,0x6a,0x6a,0x55}};
*/

#endif
