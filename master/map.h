#ifndef MAP_H
#define MAP_H

#include "enemy.h"
#include "player.h"
#include "screens.h"

// playable display area defines
#define map_x_length  15		// defines the playable field of the display in the horizontal dimension
#define map_y_height  13		// defines the playable field of the display in the vertical dimension
#define columnsPerField 8		// 8 pages next to each other	--> 1 field
#define rowsPerField 2			// 2 pages above each other		--> 1 field

// room defines
#define minRoomWidth 9
#define minRoomHeight 7
#define maxEnemiesPerRoom 10	// defines the possible maximum amount of enemies in a single room
#define maxTrapsPerRoom 5	    // defines the possible maximum amount of traps in a single room
#define maxObstaclesPerRoom 9  // defines the possible maximum amount of obstacles in a room

// level defines
#define minRoomsPerLevel 5		// defines the possible minimum amount of rooms in a single level including start and portal room !!!! Must be >= 5
#define maxRoomsPerLevel 12 	// defines the possible maximum amount of rooms in a single level including start and portal room
#define mapLayoutSize 9         // defines the width and the height of the mapLayoutArray           

extern char roomsOnLevel[maxRoomsPerLevel * 10]; //"r ID XX YY T '\0'" (1 + 7) + 1
extern char tmpRoom[10];

extern uint8_t levelIDcounter;
extern uint8_t roomIDcounter;
extern uint8_t portalCreated;
extern uint8_t fieldType;

extern uint8_t merchantItems[3]; // the items the merchant sells, gets updated with every levelinit.

typedef enum {
	PLAYER = 255,				// defines the room->map field where the player is
	INDESTRUCTIBLE = 254,		// only walls are of type INDESTRUCTIBLE
	OBSTACLE = 253,			    // destructible obstacles or walls
	TRAP = 252,					// different types of traps
	FREE = 251,					// fields where the enemies and player can move on
	WHITE = 250,				// only optical block, draw this white, not inside a Room
	DOOROPEN = 249,				// is used to move the player to the next room
	DOORCLOSED = 248,			// this field changes to DOOROPEN when all enemies in a room are eliminated
	NOTRAPGENERATE = 247,		// fields around the player where no traps should be generated, minimum 1 field radius
	PORTAL = 246,				// middlepoint of the portal, draw from map[x-1][y-1] to map[x+1][y+1]
	PORTAL_SPRITE = 245,        // fields around the portal middlepoint to draw the portal
	MERCHANT = 244,
	BUYFIELD = 243,
	MERCHANT_TABLE = 242,
	DEADENEMY = 220
} FieldType;


typedef enum {
	START_ROOM = 0,
	STANDARD_ROOM = 1,
	PORTAL_ROOM = 2,
	MERCHANT_ROOM = 3,
	BOSS_ROOM = 4,
	BOSS_START_ROOM = 5
} RoomType;


typedef struct {
	uint8_t ID;									// unique ID for every room
	uint8_t type;								// saves the type of the room, used to check if rooms which should be unique on a level are existing or not
	uint8_t size[2];							// first value is the width of the room; second value is the height of the room
	uint8_t startPos[2];						// first drawed field of the room (top left wall edge)
	uint8_t endPos[2];							// last drawed field of the room (bottom right wall edge)
	uint8_t doors;								// every room has a random amount of doors but allways the door back to the room where the player came from
	uint8_t map[map_x_length][map_y_height];	// describes all fields of the playable field of the display; fields outside the room size are WHITE

	uint8_t enemiesAlive;						// defines how many enemies in a room should exist, this number is generated randomly
	uint8_t roomChanged;						// is set when obstacles are removed
} Room;


typedef struct {
	uint8_t amountOfRooms;						        // defines how many rooms are on the level, the amount is generated randomly in [minRoomsPerLevel, maxRoomsPerLevel]
	uint8_t currentRoomID;						        // defines the room in which the player actual is
	uint8_t mapLayout[mapLayoutSize][mapLayoutSize];    // saves the layout of the rooms
	uint8_t portalExists;
	uint8_t merchantExists;
	Room room;				                            // saves the current room generated on one level
} Level;


void levelInit(Level* l, Player * p);// a level is a whole floor; it consists of rooms; player position = middle of the room

void generateBossLevel(Level* l);

Room generateRoom(RoomType type, const uint8_t ID, const uint8_t doors);	// generates a room of the specific type; e.g when levelInit() roomType = START_ROOM
void setObstaclesAndTraps(Room* room); // sets some fields to OBSTACLE, TRAP. uses roomID, levelID, and doors as a seed
void writeRoomFields(Room* room);						// defines all fields of the map of the room
void buildGrid(const Room * room);
void drawField(const uint8_t x, const uint8_t y, const uint8_t type);
void freeRectangle(Room* room, const uint8_t sizeX, const uint8_t sizeY, const uint8_t x, const uint8_t y); // used to set the fields of an old enemy pos to free and draw them free
void setRectangleToEnemy(Room* room, Enemy* e); // used to set the fields of an enemy to its ID
void generatePortal(Room* room);
void drawPortal(Room* room, const uint8_t noOfMovements);
void generateMerchant(Room* room);
void drawMerchant(Room* room, const uint8_t noOfMovements);

void generateEnemies(Enemy* enemies, Room* room, const Player* p);
void calculateEnemyPos(Room* room, const Player* p, Enemy* e);
void drawOpenDoor(Room * room);
void generateMapLayout(Level *l); //creates a layout of Rooms for the level. this will be used for navigation

uint8_t isFree(const Room* room, const uint8_t x, const uint8_t y, const uint8_t sizeX, const uint8_t sizeY);
uint8_t isWallAt(const Room* room, const uint8_t x, const uint8_t y, const uint8_t sizeX, const uint8_t sizeY); // returns true if a wall is inside the specified area
uint8_t isPlayerAt(const Room* room, const uint8_t x, const uint8_t y, const uint8_t sizeX, const uint8_t sizeY); // '-' but for player
uint8_t isFreeForEnemy(const Room* room, const uint8_t newX, const uint8_t newY,const Enemy* e);

// helper fct for seeding, ranges in [min, max] NOT (min,max)
uint8_t randWithin(uint8_t min, uint8_t max); 


char* getRoomData(uint8_t ID);
void saveRoom(Room* room);
Room reloadRoom(Level* l, uint8_t ID, uint8_t doors); // reloads an already visited room

extern uint8_t door_horizontal[2][8];
extern uint8_t door_vertical[2][8];

#endif
