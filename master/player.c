#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "player.h"
#include "display.h"

Player playerInit() {
	Player player;

	player.position[0] = 0;
	player.position[1] = 0;
	player.health = 6;
	player.maxHealth = 6;

	player.inventory.weapon = createDagger();
	player.inventory.money = 0;
	player.inventory.armor = NOARMOR;
	player.inventory.item = NOITEM;

	player.sprite = &character_sprite[0][0];

	drawPlayerLife(&player);
	drawUI();
	drawDagger();
	drawMoney(player.inventory.money);
	return player;
}


void drawPlayerLife(Player* player) {
	if (player->health == 0) {
		return;
	}

	if (player->health > player->maxHealth) {
		player->health = player->maxHealth;
	}

	uint8_t i;
	for (i = 0; i < player->maxHealth; i += 2) {
		if (i < 6) {
			drawEmptyHeart(124 + 6 * (i), 1); // 1. Zeile
		}
		else {
			drawEmptyHeart(124 + 6 * (i - 6), 4); // 2. Zeile
		}
	}

	for (i = 0; i < player->health; i++) {
		if (i < 6) {
			if (i % 2 == 0) {
				drawHalfHeart(124 + 6 * (i), 1); // 1. Zeile
			}
			else {
				drawFullHeart(124 + 6 * (i - 1), 1);
			}
		}
		else {
			if (i % 2 == 0) {
				drawHalfHeart(124 + 6 * (i - 6), 4); // 2. Zeile
			}
			else {
				drawFullHeart(124 + 6 * (i - 1 - 6), 4);
			}
		}
	}
}


void drawEmptyHeart(uint8_t posX, uint8_t posY) {
	uint8_t pageRows[3][9] = {
		{0xF0,0x0C,0x03,0x0C,0x30,0x0C,0x03,0x0C,0xF0},
		{0x0F,0x30,0xC0,0x00,0x00,0x00,0xC0,0x30,0x0F},
		{0x00,0x00,0x00,0x03,0x0C,0x03,0x00,0x00,0x00}
	};

	uint8_t i, j;
	for (i = 0; i < sizeof(pageRows) / sizeof(pageRows[0]); i++) {
		for (j = 0; j < sizeof(pageRows[i]) / sizeof(pageRows[i][0]); j++) {
			page(posX + j, posY + i, pageRows[i][j]);
		}
	}
}


void drawHalfHeart(uint8_t posX, uint8_t posY) {
	uint8_t pageRows[3][9] = {
		{0xF0,0xAC,0xAB,0xAC,0xF0,0x0C,0x03,0x0C,0xF0},
		{0x0F,0x3A,0xEA,0xAA,0xFF,0x00,0xC0,0x30,0x0F},
		{0x00,0x00,0x00,0x03,0x0F,0x03,0x00,0x00,0x00}
	};

	uint8_t i, j;
	for (i = 0; i < sizeof(pageRows) / sizeof(pageRows[0]); i++) {
		for (j = 0; j < sizeof(pageRows[i]) / sizeof(pageRows[i][0]); j++) {
			page(posX + j, posY + i, pageRows[i][j]);
		}
	}
}


void drawFullHeart(uint8_t posX, uint8_t posY) {
	uint8_t pageRows[3][9] = {
		{0xF0,0xAC,0xAB,0xAC,0xB0,0xAC,0xAB,0xAC,0xF0},
		{0x0F,0x3A,0xEA,0xAA,0xAA,0xAA,0xEA,0x3A,0x0F},
		{0x00,0x00,0x00,0x03,0x0E,0x03,0x00,0x00,0x00}
	};

	uint8_t i, j;
	for (i = 0; i < sizeof(pageRows) / sizeof(pageRows[0]); i++) {
		for (j = 0; j < sizeof(pageRows[i]) / sizeof(pageRows[i][0]); j++) {
			page(posX + j, posY + i, pageRows[i][j]);
		}
	}
}

// Weapons stuff TODO: maybe put this in a seperate file
Weapon createDagger() {
	Weapon dagger = { 1, &standardPattern[0][0], &dagger_sprite[0][0] };
	return dagger;
}

Weapon createSpear() {
	Weapon spear = { 2, &longRangePattern[0][0], &spear_sprite[0][0] };
	return spear;
}

Weapon createSword() {
	Weapon sword = { 3, &standardPattern[0][0], &sword_sprite[0][0] };
	return sword;
}

Weapon createBroadsword() {
	Weapon broadsword = { 2, &sweepAttackPattern[0][0], &broadsword_sprite[0][0]};
	return broadsword;
}


void drawUI() {
    uint8_t i;
	/*for (i = 0; i < 11; i++) { // sprite1
		for (j = 0; j < 40; j++) {
			page(121 + j, 7 + i, pgm_read_byte(&UI[i][j]));
		}
	}
*/
	for(i=0;i<35;i++){ // draw Upper and lower edge
		page(123 + i, 7, 0xC0);
		page(123+i,15,0x0C);
	}
	for(i=0;i< 7; i++){ // draw vertical lines
		page(123,8+i,0xFF);
		page(143,8+i,0xFF);
		page(157,8+i,0xFF);
	}
	for(i=0;i<13;i++){
		page(144+i,11,0xCC);
	}
	page(123,15,0x0F); // lower left corner
	page(157,15,0x0F); // lower right corner
	page(143,15,0x0F); // lower middle connection

	drawText(128,16,"MONEY",5);
	

}

void drawUIslime(uint8_t posX, uint8_t posY) {
    uint8_t i, j;
    for (i = 0; i < 3; i++) { // sprite1
	    for (j = 0; j < 12; j++) {
		    page(posX + j, posY + i, pgm_read_byte(&UIslime_sprite1[i][j]));
	    }
    }    
}


void drawDagger() {
	uint8_t i, j;
	for (i = 0; i < 7; i++) { // sprite1
		for (j = 0; j < 17; j++) {
			page(125 + j, 8 + i, pgm_read_byte(&dagger_sprite[i][j]));
		}
	}
}

void drawSword() {
	uint8_t i, j;
	for (i = 0; i < 7; i++) { // sprite1
		for (j = 0; j < 17; j++) {
			page(125 + j, 8 + i, pgm_read_byte(&sword_sprite[i][j]));
		}
	}
}

void drawSpear() {
    uint8_t i, j;
	for (i = 0; i < 7; i++) { // sprite1
		for (j = 0; j < 17; j++) {
			page(125 + j, 8 + i, pgm_read_byte(&spear_sprite[i][j]));
		}
	}
}


void drawBroadsword() {
	uint8_t i, j;
	for (i = 0; i < 7; i++) { // sprite1
		for (j = 0; j < 17; j++) {
			page(125 + j, 8 + i, pgm_read_byte(&broadsword_sprite[i][j]));
		}
	}
}


void drawPotion() {
    uint8_t i, j;
	for (i = 0; i < 3; i++) { // sprite1
		for (j = 0; j < 11; j++) {
			page(145 + j, 8 + i, pgm_read_byte(&potion_sprite[i][j]));
		}
	}
}


void drawBoots() {
    uint8_t i, j;
	for (i = 0; i < 3; i++) { // sprite1
		for (j = 0; j < 11; j++) {
			page(145 + j, 12 + i, pgm_read_byte(&boots_sprite[i][j]));
		}
	}
}


void drawPants() {
	uint8_t i, j;
	for (i = 0; i < 3; i++) { // sprite1
		for (j = 0; j < 11; j++) {
			page(145 + j, 12 + i, pgm_read_byte(&pants_sprite[i][j]));
		}
	}
}


void drawChestPlate() {
	uint8_t i, j;
	for (i = 0; i < 3; i++) { // sprite1
		for (j = 0; j < 11; j++) {
			page(145 + j, 12 + i, pgm_read_byte(&chestplate_sprite[i][j]));
		}
	}
}


void drawHelmet() {
	uint8_t i, j;
	for (i = 0; i < 3; i++) { // sprite1
		for (j = 0; j < 11; j++) {
			page(145 + j, 12 + i, pgm_read_byte(&helmet_sprite[i][j]));
		}
	}
}


void drawMoney(const uint16_t money) {
    deleteRectangleAt(36, 8, 123, 18);
    char str[8] = { '0' }; // First 0, never reach 1 million coins, just astetics
    
    if (money < 10000) {
        strcat(str, "0");
    }
    if (money < 1000) {
        strcat(str, "0");
    }
    if (money < 100) {
        strcat(str, "0");
    }
    if (money < 10) {
        strcat(str, "0");
    }
    
    char tmp[8];
    itoa(money, tmp, 10);
    strcat(str, tmp);
    
    drawText(125, 18, str, 6);
}


void swapWeapon(Player* p, const uint8_t weapon) {
	if (weapon == DAGGER) {
	    p->inventory.weapon = createDagger();
		drawDagger();
	}else if (weapon == SWORD) {
	    p->inventory.weapon = createSword();
		drawSword();
    }else if (weapon == SPEAR) {
        p->inventory.weapon = createSpear();
		drawSpear();
	}else if (weapon == BROADSWORD) {
        p->inventory.weapon = createBroadsword();
		drawBroadsword();
	}
}


void swapItem(Player* p, const uint8_t item) {
	p->inventory.item = item;
	if (item == POTION) {
		drawPotion();
	}
}


void swapArmor(Player* p, const uint8_t armor) {
	p->inventory.armor = armor;
	if (armor == BOOTS) {
		drawBoots();
	} else if (armor == PANTS) {
	    drawPants();
	} else if (armor == HELMET) {
	    drawHelmet();
	} else if (armor == CHESTPLATE) {
	    drawChestPlate();
	}
}


void useItem(Player* p) {
    if (p->inventory.item == POTION) {
        p->health = p->health + 2;
        drawPlayerLife(p);
    }
    p->inventory.item = NOITEM;
	deleteRectangleAt(11, 12, 145, 8);
}


uint8_t character_sprite[2][8] = {
	{0x3C,0xC3,0xCF,0x33,0x33,0xCF,0xC3,0x3C},
	{0x0F,0x3F,0xF0,0x33,0x3C,0xF0,0x3F,0x0F}
};

//attack patterns; Playerpos is at [2,1]
uint8_t standardPattern[3][3] = {
	{0,0,0},
	{0,1,0},
	{0,0,0}
};

uint8_t longRangePattern[3][3] = {
	{0,0,0},
	{0,1,1},
	{0,0,0}
};

uint8_t sweepAttackPattern[3][3] = {
	{0,1,0},
	{0,1,0},
	{0,1,0}
};
