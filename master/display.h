#ifndef DISPLAY_H
#define DISPLAY_H

#include "sprites.h" // <-- TODO: maybe put this somewhere else...

void displayInit(void);
void clear(void);
void sendbyte(uint8_t byte,uint8_t cd);
void page(uint8_t x,uint8_t y,uint8_t h);

// own stuff
void blackDisplay(uint8_t x, uint8_t y);
void whiteDisplay(uint8_t x, uint8_t y);
void drawRectangleAt(int width, int height, int x, int y);
void deleteRectangleAt(int width, int height, int x, int y); 
uint8_t drawLetter(const uint8_t posX, const uint8_t posY, const char letter);
void drawText(const uint8_t posX, const uint8_t posY, const char text[], const uint8_t textLength);
void drawText_pgm(const uint8_t posX, const uint8_t posY, const uint8_t text[], const uint8_t textLength);

#endif
