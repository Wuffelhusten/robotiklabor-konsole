#include <inttypes.h>
#include <util/delay.h>
#include "movement.h"
#include "adc.h"
#include "uart.h"

void movePlayer(Player* p, Level* l, Enemy* enemies, const uint8_t dir) {
	if (!checkEnemiesInPattern(l, p, enemies, dir)) { 
		uint8_t posX = dir == 2 ? p->position[0] + 1 : dir == 3 ? p->position[0] - 1 : p->position[0];
		uint8_t posY = dir == 0 ? p->position[1] - 1 : dir == 1 ? p->position[1] + 1 : p->position[1];
		FieldType type = l->room.map[posX][posY];

		switch (type) {
		case DOOROPEN:
			changeRoom(p, l, enemies, dir);
			return;

		case PORTAL:
		    if(levelIDcounter % 3 == 1){
		        uart_putc('C'); //end disco descent
		        _delay_ms(20);
		        uart_putc('z');  //start zelda
		    }else if(levelIDcounter % 3 == 2){
		        uart_putc('Z'); //end zelda
		        _delay_ms(20); 
		        uart_putc('b'); //start bonestrousle
		    }else if(levelIDcounter % 3 == 0){
		        uart_putc('B'); //end bonetrousle
		        _delay_ms(20);  
		        uart_putc('c'); //start disco descent
		         p->health += 2;
		        if (p->maxHealth <= 10) {
		            p->maxHealth += 2;
		            drawPlayerLife(p);
		        }
		    }
		    _delay_ms(20);
			levelInit(l, p);
			drawCharacter(p);
			generateEnemies(enemies, &l->room, p);
			return;

		case BUYFIELD:
			drawMerchantMenu(p, &l->room);
			break;

		case FREE:
			break;
			
	    case PORTAL_SPRITE:
	        break;

		case TRAP:
			p->health--;
			drawPlayerLife(p);
			break;

		default:
			return;
		}

		// Restore old field
		l->room.map[p->position[0]][p->position[1]] = fieldType;
		drawField(p->position[0], p->position[1], fieldType);

		// Set player to new Field
		p->position[0] = posX;
		p->position[1] = posY;

		fieldType = l->room.map[p->position[0]][p->position[1]];
		l->room.map[p->position[0]][p->position[1]] = PLAYER;

		drawCharacter(p);
	}
}

uint8_t checkEnemiesInPattern(Level* l, Player* p, Enemy* enemies, const uint8_t dir) {
	char tmpEnemies[9] = { "" };
	char tmp[2] = { "" };
	uint8_t x, y;
	uint8_t xLimit = 3; // used to limit x to prevent oob
	uint8_t yLimit = 3; // '-' y '-'
	uint8_t enemy = 0;	// iterator & indicator how many Enemies are in tmpEnemies

	if(p->position[0] == 1 || p-> position[0] == map_x_length -2){ // player is too close too x - boundary -> limit x checks
		xLimit = 2;
	}
	if(p->position[1] == 1 || p-> position[0] == map_y_height -2){ // player is too close too y- boundary -> limit y checks
		yLimit = 2;
	}
	// Check enemy to the right of the player
	if (dir == 2) {
		for (y = 0; y < yLimit; y++) {
			for (x = 0; x < xLimit; x++) {
				if (l->room.map[p->position[0] + x][p->position[1] - 1 + y] < 220 && p->inventory.weapon.pattern[x + y*3] == 1) {
					tmp[0] = l->room.map[p->position[0] + x][p->position[1] - 1 + y] + 65;	// Convert ID into ascii-code beginning with A = 65 to avoid NULL = 0 or other bad chars
					if (!strpbrk(tmpEnemies, tmp)) {
						tmpEnemies[enemy] = tmp[0];
						tmp[0] = '\0';
						enemy++;
					}
				}
			}
		}
	}

	// Check enemy to the left of the player
	else if (dir == 3) {
		for (y = 0; y < yLimit; y++) {
			for (x = 0; x < xLimit; x++) {
				if (l->room.map[p->position[0] - x][p->position[1] + 1 - y] < 220 && p->inventory.weapon.pattern[x + y*3] == 1) {
					tmp[0] = l->room.map[p->position[0] - x][p->position[1] + 1 - y] + 65;
					if (!strpbrk(tmpEnemies, tmp)) {
						tmpEnemies[enemy] = tmp[0];
						tmp[0] = '\0';
						enemy++;
					}
				}
			}
		}
	}

	// Check enemy to the top of the player
	else if (dir == 0) {
		for (x = 0; x < xLimit; x++) {
			for (y = 0; y < yLimit; y++) {
				if (l->room.map[p->position[0] - 1 + x][p->position[1] - y] < 220 && p->inventory.weapon.pattern[x*3 + y] == 1) {
					tmp[0] = l->room.map[p->position[0] - 1 + x][p->position[1] - y] + 65;
					if (!strpbrk(tmpEnemies, tmp)) {
						tmpEnemies[enemy] = tmp[0];
						tmp[0] = '\0';
						enemy++;
					}
				}
			}
		}
	}

	// Check enemy to the bottom of the player
	else if (dir == 1) {
		for (x = 0; x < xLimit; x++) {
			for (y = 0; y < yLimit; y++) {
				if (l->room.map[p->position[0] + 1 - x][p->position[1] + y] < 220 && p->inventory.weapon.pattern[x*3 + y] == 1) {
					tmp[0] = l->room.map[p->position[0] + 1 - x][p->position[1] + y] + 65;
					if (!strpbrk(tmpEnemies, tmp)) {
						tmpEnemies[enemy] = tmp[0];
						tmp[0] = '\0';
						enemy++;
					}
				}
			}
		}
	}
	if (tmpEnemies[0] == '\0') { // Check if an enemy was detected
		return 0;
	}
	else {	// There are enemies in pattern => deal damage
		for (x = 0; x < enemy; x++) {
			for (y = 0; y < maxEnemiesPerRoom; y++) {
				uint8_t tmp1 = tmpEnemies[x] - 65;
				if (enemies[y].ID == tmp1) {
					if(l->room.type == BOSS_ROOM){
						damageToBOSS(p, enemies, &l->room, y);
					}else{
						damageToEnemy(p, &enemies[y], &l->room);
					}

				}
			}
		}
	}
	return 1;
}


void damageToEnemy(Player* p, Enemy* e, Room* room) {
	if (e->health > p->inventory.weapon.damage) {
	    //freeRectangle(room, e->size[0], e->size[1], e->position[0], e->position[1]); // used to set the fields of an old enemy pos to free and draw them free
	    uart_putc('h');
		e->health -= p->inventory.weapon.damage;
		//_delay_ms(50);
		//setRectangleToEnemy(room, e); // used to set the fields of an enemy to its ID
		//drawEnemy(e, 0);
		//_delay_ms(50);
	}
	else {
	    uart_putc('d');
		e->health = 0;
		p->inventory.money += e->moneyValue;
		drawMoney(p->inventory.money);		
		e->ID = DEADENEMY;		
		room->enemiesAlive--;

		// free the map data
		uint8_t i, j;
		for (i = 0; i < e->size[0]; i++) {
			for (j = 0; j < e->size[1]; j++) {
				room->map[e->position[0] + i][e->position[1] + j] = FREE;
				drawField(e->position[0] + i, e->position[1] + j, FREE);
			}
		}
	}
}

// like damageToEnemy, but can spawn specific enemies / do sth else if Boss is killed 
void damageToBOSS(Player* p, Enemy* enemies, Room* room, uint8_t damagedEnemyID){

	if (enemies[damagedEnemyID].health > p->inventory.weapon.damage) {
	    uart_putc('h');
		enemies[damagedEnemyID].health -= p->inventory.weapon.damage;
		if(enemies[damagedEnemyID].moneyValue == 50){ // draw Boss Life
		    char bHealth = enemies[damagedEnemyID].health + '0';
		    drawText(64,0,"   ",3);
		    drawText(66, 0, &bHealth, 1);
		}
	}
	else {
	    uart_putc('d');
		enemies[damagedEnemyID].health = 0;
		p->inventory.money += enemies[damagedEnemyID].moneyValue;
		drawMoney(p->inventory.money);		
		enemies[damagedEnemyID].ID = DEADENEMY;		
		room->enemiesAlive--;

		// free the map data
		uint8_t i, j;
		for (i = 0; i < enemies[damagedEnemyID].size[0]; i++) {
			for (j = 0; j < enemies[damagedEnemyID].size[1]; j++) {
				room->map[enemies[damagedEnemyID].position[0] + i][enemies[damagedEnemyID].position[1] + j] = FREE;
				drawField(enemies[damagedEnemyID].position[0] + i, enemies[damagedEnemyID].position[1] + j, FREE);
			}
		}
		// spawn minons
		if(enemies[damagedEnemyID].moneyValue == 50){ // slimeboss
		    drawText(40,0,"         ",9);
			uint8_t posX = enemies[damagedEnemyID].position[0]; 
			uint8_t posY = enemies[damagedEnemyID].position[1];
			createSlimeMiniBoss(&enemies[damagedEnemyID]);
			enemies[damagedEnemyID].ID = damagedEnemyID;
			enemies[damagedEnemyID].position[0] = posX;
			enemies[damagedEnemyID].position[1] = posY;
			uint8_t i,j;
			for(i=0;i<enemies[damagedEnemyID].size[0];i++){ // set map fields
				for(j=0;j<enemies[damagedEnemyID].size[1];j++){
					room->map[enemies[damagedEnemyID].position[0]+i][enemies[damagedEnemyID].position[1]+j] = enemies[damagedEnemyID].ID;
				}
			}
			uint8_t nextID = getMinimumFreeEnemyID(enemies);	
			createSlimeMiniBoss(&enemies[nextID]);
			enemies[nextID].ID = nextID;
			room->enemiesAlive++;

			uint8_t direction = getFreeDirection(room, &enemies[damagedEnemyID]); // returns the driection in which the 2nd SlimeMiniBoss can be spawned
			// 0 = NONE, 1 = UP, 2 = right, 3 = down, 4 = left
			if(!direction){
			return;
			}else if(direction == 1){ // UP
			enemies[nextID].position[0] = posX;
			enemies[nextID].position[1] = posY - enemies[nextID].size[1];

			}else if(direction == 2){ // right
			enemies[nextID].position[0] = posX + enemies[damagedEnemyID].size[0];
			enemies[nextID].position[1] = posY;

			}else if(direction == 3){ //down
			enemies[nextID].position[0] = posX;
			enemies[nextID].position[1] = posY + enemies[damagedEnemyID].size[1];

			}else if(direction == 4){ // left
			enemies[nextID].position[0] = posX - enemies[nextID].size[0];
			enemies[nextID].position[1] = posY;
			
			}
			for(i=0;i<enemies[nextID].size[0];i++){ // set map fields
				for(j=0;j<enemies[nextID].size[1];j++){
					room->map[enemies[nextID].position[0]+i][enemies[nextID].position[1]+j] = enemies[nextID].ID;
				}
			}
			room->enemiesAlive++;
			currentEnemyID++;	// this should be ok, since theres only 1 Slimeboss in the room..
		}else if(enemies[damagedEnemyID].moneyValue == 25){ // slimeboss
			uint8_t posX = enemies[damagedEnemyID].position[0];
			uint8_t posY = enemies[damagedEnemyID].position[1];
			createSlimeMiniMiniBoss(&enemies[damagedEnemyID]);
			enemies[damagedEnemyID].ID = damagedEnemyID;
			enemies[damagedEnemyID].position[0] = posX;
			enemies[damagedEnemyID].position[1] = posY;
			room->enemiesAlive++;
			room->map[posX][posY] = damagedEnemyID;
			
			uint8_t nextID = getMinimumFreeEnemyID(enemies);
			if(nextID == maxEnemiesPerRoom){return;}
			createSlimeMiniMiniBoss(&enemies[nextID]);
			enemies[nextID].ID = nextID;
			enemies[nextID].position[0] = posX+1;
			enemies[nextID].position[1] = posY;
			room->enemiesAlive++;
			currentEnemyID++;
			room->map[posX+1][posY] = nextID;

			nextID = getMinimumFreeEnemyID(enemies);
			if(nextID == maxEnemiesPerRoom){return;}
			createSlimeMiniMiniBoss(&enemies[nextID]);
			enemies[nextID].ID = nextID;
			enemies[nextID].position[0] = posX;
			enemies[nextID].position[1] = posY+1;
			room->enemiesAlive++;
			currentEnemyID++;
			room->map[posX][posY+1] = nextID;

			nextID = getMinimumFreeEnemyID(enemies);
			if(nextID == maxEnemiesPerRoom){return;}
			createSlimeMiniMiniBoss(&enemies[nextID]);
			enemies[nextID].ID = nextID;
			enemies[nextID].position[0] = posX+1;
			enemies[nextID].position[1] = posY+1;
			room->enemiesAlive++;
			currentEnemyID++;
			room->map[posX+1][posY+1] = nextID;
		}


	}
}

// helper fct for damageBoss
uint8_t getMinimumFreeEnemyID(const Enemy* enemies){
	uint8_t i;
	for(i=0; i< maxEnemiesPerRoom; i++){
		if(enemies[i].ID == DEADENEMY){
			return i;
		}
	}
	return maxEnemiesPerRoom; // -> no Free Space, but this shouldnt ever be called
}

// Checks in which direction another Enemy of the same size can be spawned
// 0 = NONE, 1 = UP, 2 = right, 3 = down, 4 = left
uint8_t getFreeDirection(const Room* room, const Enemy* e){
	uint8_t x,y, moveOk = 2;
	for(x=0; x< e->size[0]; x++){	//RIGHT
		for(y=0; y< e->size[1]; y++){
			if(!moveOk){break;}
			else if(room->map[e->position[0]+x +e->size[0]][e->position[1]+y] != FREE){
				moveOk = 0;
				break;
			}
		}
	}
	if(moveOk){return moveOk;}
	moveOk = 4; // left
	for(x=0; x< e->size[0]; x++){
		for(y=0; y< e->size[1]; y++){
			if(!moveOk){break;}
			else if(room->map[e->position[0]-x-1][e->position[1]+y] != FREE){
				moveOk = 0;
				break;
			}
		}
	}
	if(moveOk){return moveOk;}
	moveOk = 1; // UP
	for(x=0; x< e->size[0]; x++){
		for(y=0; y< e->size[1]; y++){
			if(!moveOk){break;}
			else if(room->map[e->position[0]+x][e->position[1]-y-1] != FREE){
				moveOk = 0;
				break;
			}
		}
	}
	if(moveOk){return moveOk;}

	moveOk = 3; // down
	for(x=0; x< e->size[0]; x++){
		for(y=0; y< e->size[1]; y++){
			if(!moveOk){break;}
			else if(room->map[e->position[0]+x][e->position[1]+y+e->size[1]] != FREE){
				moveOk = 0;
				break;
			}
		}
	}
	if(moveOk){return moveOk;}

	return 0; // no free directions, somehow...
}

void moveEnemy(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p) {
    
    if(e->movementState == STUNNED){
        if(e->remainingStunnedTicks != 0){
            e->remainingStunnedTicks--;
            drawEnemy(e, 0);
            return;
        }else{
            e->movementState = IDLE;
        }
    }
    
    if (noOfMovements % e->moveTime != 0) { // enemy doesn't move this turn
		drawEnemy(e, noOfMovements);
		return;
	}
    freeRectangle(room, e->size[0], e->size[1], e->position[0], e->position[1]); // free the fields the enemy is standing on.. maybe dont do this for stationary enemies...

	if (e->enemyType == STATIONARY) { // stationary enemy
	}else if(e->enemyType == WANDERING){
	    moveWandering(e,noOfMovements, room, p); // move_ only modifies the e->pos, not the map fields!!
	}else if(e->enemyType == FOLLOWING){
	    moveFollowing(e,noOfMovements, room, p);
	}else if(e->enemyType == CHARGING){
	    moveCharging(e,noOfMovements, room, p);
	}else if(e->enemyType == RANDOM){
		moveRandom(e,noOfMovements, room, p);
	}
	
    setRectangleToEnemy(room, e); // sets the fields of the map
    drawEnemy(e, noOfMovements);
}

//handles WANDERING-Type Enemies
void moveWandering(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p){
    int moveX = *(e->movement + (noOfMovements % e->movementLength + e->movementStep) * 2) - 127;
	int moveY = *(e->movement + (noOfMovements % e->movementLength + e->movementStep) * 2 + 1) - 127;

	uint8_t newPosX = e->position[0] + moveX;
	uint8_t newPosY = e->position[1] + moveY;

	if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room, newPosX, newPosY, e)){
		damageToPlayer(e,p);
		return;
	}
	if(isFreeForEnemy(room, newPosX, newPosY, e)){
		// the move is doable, -> move enemy 
		e->position[0] = newPosX;
		e->position[1] = newPosY;		
	}

	e->movementStep++;
	e->movementStep = e->movementStep % e->movementLength;

	return;
}
// handles enemies that are following the player
void moveFollowing(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p){
// rule34 based enemyAi here!. Enemies only move 1 Field l/r/u/d
	int xDiff = p->position[0] - e->position[0]; //diff between the player & the enemy. >0 -> player is right of enemy
	int yDiff = p->position[1] - e->position[1]; // yDiff > 0: p is below enemy --> xDiff,yDiff is the vector pointing to the player
	uint8_t newPosX = e->position[0];
	uint8_t newPosY = e->position[1];
	//printf("xDiff: %d   yDiff: %d \n",xDiff,yDiff);
	if(abso(yDiff) > abso(xDiff)){ // try to move vertical
		newPosX = e->position[0]; // reset the newPos values
		newPosY = e->position[1]; 
		newPosY += sign(yDiff); // modify the pos

		if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
			damageToPlayer(e,p);
			return;		
		}
		if(isFreeForEnemy(room, newPosX, newPosY, e)){
			e->position[0] = newPosX;
			e->position[1] = newPosY;

		}else { //best move not possible, try 2. best -> horizontal
			newPosX = e->position[0]; // reset the newPos values
			newPosY = e->position[1]; 
			newPosX += sign(xDiff); // modify the pos
			if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
				damageToPlayer(e,p);
				return;		
			}
			if(isFreeForEnemy(room, newPosX, newPosY, e)){
				e->position[0] = newPosX;
				e->position[1] = newPosY;
			}else { //try 3. best -> horizontal but wrong direction
				newPosX = e->position[0]; // reset the newPos values
				newPosY = e->position[1]; 
				newPosX += -sign(xDiff); // modify the pos
				if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
					damageToPlayer(e,p);
					return;		
				}
				if(isFreeForEnemy(room, newPosX, newPosY, e)){
					e->position[0] = newPosX;
					e->position[1] = newPosY;

				}else { // try the worst move -> vertical but wrong direction
					newPosX = e->position[0]; // reset the newPos values
					newPosY = e->position[1]; 
					newPosY += -sign(yDiff); // modify the pos
					if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
						damageToPlayer(e,p);
						return;		
					}
					if(isFreeForEnemy(room, newPosX, newPosY, e)){
						e->position[0] = newPosX;
						e->position[1] = newPosY;
								
					}else{ // can't move. Dont move
						//printf("BLOCKED!\n");
					}
				}

			}

		}
	}else if(abso(yDiff) <= abso(xDiff)){ // try to move horizontal
		newPosX = e->position[0]; // reset the newPos values
		newPosY = e->position[1]; 
		newPosX += sign(xDiff); // modify the pos
		if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
			damageToPlayer(e,p);
			return;		
		}
		if(isFreeForEnemy(room, newPosX, newPosY, e)){
			e->position[0] = newPosX;
			e->position[1] = newPosY;

		}else { //best move not possible, try 2. best -> vertical
			newPosX = e->position[0]; // reset the newPos values
			newPosY = e->position[1]; 
			newPosY += sign(yDiff); // modify the pos
			if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
				damageToPlayer(e,p);
				return;		
			}
			if(isFreeForEnemy(room, newPosX, newPosY, e)){
				e->position[0] = newPosX;
				e->position[1] = newPosY;

			}else { //try 3. best -> vertical but wrong direction
				newPosX = e->position[0]; // reset the newPos values
				newPosY = e->position[1]; 
				newPosY += -sign(yDiff); // modify the pos
				if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
					damageToPlayer(e,p);
					return;		
				}
				if(isFreeForEnemy(room, newPosX, newPosY, e)){
					e->position[0] = newPosX;
					e->position[1] = newPosY;
				}else { // try the worst move -> horizontal but wrong direction
					newPosX = e->position[0]; // reset the newPos values
					newPosY = e->position[1]; 
					newPosX += -sign(xDiff); // modify the pos
					if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
						damageToPlayer(e,p);
						return;		
					}
					if(isFreeForEnemy(room, newPosX, newPosY, e)){
						e->position[0] = newPosX;
						e->position[1] = newPosY;
					}else{ // can't move. Dont move
						//printf("BLOCKED!\n");
					}
				}

			}

		}
		//drawEnemy(e, noOfMovements);
	}
		
	
}


void moveCharging(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p){
    if(e->movementState == IDLE){ 
        // look for player
        int xDiff = p->position[0] - e->position[0]; //diff between the player & the enemy. >0 -> player is right of enemy
	    int yDiff = p->position[1] - e->position[1]; // yDiff > 0: p is below enemy --> xDiff,yDiff is the vector pointing to the player
        
        if(yDiff >= 0 && yDiff <= e->size[0] - 1){
            if(xDiff < 0 ){
                e->movementState = CHARGE_LEFT;
            }else {
                e->movementState = CHARGE_RIGHT;
            }
        }else if(xDiff >= 0 && xDiff <= e->size[1] - 1){
            if(yDiff < 0 ){
                e->movementState = CHARGE_UP;
            }else {
                e->movementState = CHARGE_DOWN;
            }
        } 
    }
    uint8_t newPosX = e->position[0];
    uint8_t newPosY = e->position[1];
    
    if(e->movementState == CHARGE_LEFT){
       newPosX--;
    } else if(e->movementState == CHARGE_RIGHT){
        newPosX++;
    } else if(e->movementState == CHARGE_UP){
        newPosY--;
    } else if(e->movementState == CHARGE_DOWN){
        newPosY++;
    }
	if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
		// Stun for a short time, to give Players a chance to move
		e->movementState = STUNNED;
		e->remainingStunnedTicks = 2;
        damageToPlayer(e,p);
    }else if (isFreeForEnemy(room, newPosX, newPosY, e)){
	    e->position[0] = newPosX;
    	e->position[1] = newPosY;
	}else {
		// enemy ran into another enemy or a trap or a wall...
		e->movementState = STUNNED;
        e->remainingStunnedTicks = 4;
	}

    
}

// handles enemies which move in random directions, at constant intervalls
void moveRandom(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p){
	if(noOfMovements % e->moveTime != 0){ // enemy cant move
		return;
	}
	uint8_t move;
	uint8_t newPosX = e->position[0];
	uint8_t newPosY = e->position[1];
	uint8_t moveOk = 0;	
	uint8_t counter = 0;
	while(!moveOk && counter < 500){
		moveOk = 1;
		move = generateRand(0,3);
		newPosX = e->position[0]; // reset the newPos
		newPosY = e->position[1];	
		if(move == 0){ // up
			newPosY--;
		}else if(move == 1){ // down
			newPosY++;
		}else if(move == 2){ // left
			newPosX--;
		}else if(move == 3){ // right
			newPosX++;	
		}

		if(isPlayerAt(room, newPosX, newPosY, e->size[0], e->size[1]) && isFreeForEnemy(room,newPosX,newPosY,e)){
			damageToPlayer(e,p);
			return;
		}else if(!isFreeForEnemy(room, newPosX, newPosY, e)){
			moveOk = 0;
		}
		counter++;
	}
	if(counter >= 500){
		uint8_t free = isFreeForEnemy(room,e->position[0], e->position[1]-1,e); // up
		if(free){
			if(isPlayerAt(room, e->position[0], e->position[1]-1, e->size[0], e->size[1])){
				damageToPlayer(e,p);
			}else {
				e->position[1]--;
			}
			return;	
		}
		free = isFreeForEnemy(room,e->position[0], e->position[1]+1,e); // down
		if(free){
			if(isPlayerAt(room, e->position[0], e->position[1]+1, e->size[0], e->size[1])){
				damageToPlayer(e,p);
			}else {
				e->position[1]++;
			}	
			return;
		}
		free = isFreeForEnemy(room,e->position[0]+1, e->position[1],e); // right
		if(free){
			if(isPlayerAt(room, e->position[0]+1, e->position[1], e->size[0], e->size[1])){
				damageToPlayer(e,p);
			}else {
				e->position[0]++;
			}
			return;	
		}
		free = isFreeForEnemy(room,e->position[0]-1, e->position[1],e); // left
		if(free){
			if(isPlayerAt(room, e->position[0]-1, e->position[1], e->size[0], e->size[1])){
				damageToPlayer(e,p);
			}else {
				e->position[0]--;
			}
			return;	
		}
		// NOT Possible to move here, This has some bugs and is unlikely to happen, so just do nothing
		/*e->ID = DEADENEMY;
		freeRectangle(room, e->size[0], e->size[1], e->position[0], e->position[1]);
		room->enemiesAlive--;*/

	}

	// move should be ok here
	e->position[0] = newPosX;
	e->position[1] = newPosY; 

}


uint8_t abso(int x) {
	return x < 0 ? -x : x;
}

int sign(int x) {
	return x < 0 ? -1 : x > 0 ? 1 : 0;
}

void damageToPlayer(const Enemy* e, Player* p) {
	if(p->inventory.armor != NOARMOR){
		p->health = (p->health + p->inventory.armor-1) <= e->damage ? 0 :
	    e->damage <= p->inventory.armor ? p->health - 1 : p->health + p->inventory.armor - e->damage;
		
	}else {
		p->health = p->health <= e->damage ? 0 : p->health - e->damage;
	}    
	drawPlayerLife(p);
}


void changeRoom(Player* p, Level* level, Enemy* enemies, const uint8_t direction) {

	// SAVING
	if (!getRoomData(level->room.ID)) { //if room doesn't exist in string
		saveRoom(&level->room);
	}

	// LOADING or GENERATING
	uint8_t newRoomPosX = level->currentRoomID % mapLayoutSize;
	uint8_t newRoomPosY = level->currentRoomID / mapLayoutSize;
	if (direction == 0) { //UP
		newRoomPosY--;
	}
	else if (direction == 1) {//DOWN
		newRoomPosY++;
	}
	else if (direction == 2) {//RIGHT
		newRoomPosX++;
	}
	else if (direction == 3) {//LEFT
		newRoomPosX--;
	}

	uint8_t ID = newRoomPosY * mapLayoutSize + newRoomPosX;
	uint8_t doors = level->mapLayout[newRoomPosX][newRoomPosY];
	if (!getRoomData(ID)) {
		uint8_t roomCreated = 0;
        if (levelIDcounter % 3 == 0) {
            if (!level->portalExists) {
			    uint8_t i;
			    i = generateRand(0, level->amountOfRooms);
			    if (i <= roomIDcounter + 2) {
				    level->room = generateRoom(PORTAL_ROOM, ID, doors);
				    level->portalExists = 1;
				    roomCreated = 1;
			    }
		    }
		    if (!roomCreated) {
			    level->room = generateRoom(BOSS_ROOM, ID, doors);
			    level->portalExists = 0;
		    }
        } else {
		    if (!level->portalExists) {
			    uint8_t i;
			    i = generateRand(0, level->amountOfRooms);
			    if (i <= roomIDcounter + 2) {
				    level->room = generateRoom(PORTAL_ROOM, ID, doors);
				    level->portalExists = 1;
				    roomCreated = 1;
			    }
		    }
		    if ((!roomCreated) && (!level->merchantExists)) {
			    uint8_t i;
			    i = generateRand(0, level->amountOfRooms);
			    if (i <= roomIDcounter + 1) {
				    level->room = generateRoom(MERCHANT_ROOM, ID, doors);
				    level->merchantExists = 1;
				    roomCreated = 1;
			    }
		    }
		    if (!roomCreated) {
			    level->room = generateRoom(STANDARD_ROOM, ID, doors);
		    }
        }
	}
	else {
	    level->room = reloadRoom(level, ID, doors);
	}
    level->currentRoomID = level->room.ID;
    
	// SETTING THE PLAYER
	if (direction == 0) { //UP
		p->position[0] = (map_x_length - 1) / 2;
		p->position[1] = level->room.endPos[1] - 1;
	}
	else if (direction == 1) {//DOWN
		p->position[0] = (map_x_length - 1) / 2;
		p->position[1] = level->room.startPos[1] + 1;
	}
	else if (direction == 2) {//RIGHT
		p->position[0] = level->room.startPos[0] + 1;
		p->position[1] = (map_y_height - 1) / 2;
	}
	else if (direction == 3) {//LEFT
		p->position[0] = level->room.endPos[0] - 1;
		p->position[1] = (map_y_height - 1) / 2;
	}

	level->room.map[p->position[0]][p->position[1]] = PLAYER; // set the playerfield
	drawCharacter(p); //draw the player

	// CREATING ENEMIES
    generateEnemies(enemies, &(level->room), p);

    uint8_t i;
	for (i = 0; i < maxEnemiesPerRoom; i++) { // draw alive enemies
		drawEnemy(&enemies[i], 0);
	}
    resetUart();
	// TODO: CREATE TRAPS & OBSTACLES
}
