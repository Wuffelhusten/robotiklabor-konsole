#ifndef SCREENS_H
#define SCREENS_H

#include <avr/eeprom.h>
#include <stdlib.h>
#include <string.h>

#include "display.h"

#define SCREEN_WIDTH 160
#define SCREEN_HEIGHT 26

#define maxScoreboardPlayer 6
#define maxNameLength 12
#define scoreLength 6

extern const uint8_t screenMiddlePoint[2];
extern uint16_t score;

typedef enum {
	WHITE_SCREEN = 0,
	TITLE_SCREEN = 1,
	MENU_SCREEN = 2,
	DEATH_SCREEN = 3,
	BONFIRE_SETUP = 4,
	BONFIRE1 = 5,
	BONFIRE2 = 6,
	ARROW_UP = 7,
	ARROW_MIDDLE = 8,
	ARROW_BOTTOM = 9,
	HOW_TO_PLAY = 10,
	ENTRY_NAME = 11,
	SCOREBOARD = 12,
	MENU_NORMAL = 13,
	MENU_EASY = 14,
    ARROW_MIDDLE_DOWN = 15,
    HOW_TO_PLAY2 = 16
    
} Screen;


void drawScreen(const Screen screen);
void drawSelectArrow(const uint8_t field);
void deleteSelectArrow(const uint8_t field);

uint8_t getScorePosition();
void prepareProm(const uint8_t scorePos);

extern const uint8_t castle_sprite[2][9] PROGMEM;
extern const uint8_t back_sprite[2][11] PROGMEM;
extern const uint8_t ok_sprite[2][12] PROGMEM;
extern const uint8_t selection_arrow[2][5] PROGMEM;

#endif
