#include <util/delay.h>
#include <avr/io.h>
#include <inttypes.h>
#include "display.h"

void displayInit(void)
{
	DDRB |= (1<<5);//SCK
	DDRB |= (1<<3);//SDA
	DDRB |= (1<<4);//RST
	DDRB |= (1<<1);//CD
	DDRB |= (1<<0);//CS0
	PORTB |= (1<<4);//kein RST
	PORTB &= ~(1<<1);//CD auf low
	PORTB &= ~(1<<5);//SCK auf low
	PORTB |= (1<<0);_delay_us(10);PORTB &= ~(1<<0);//CS0 auf low
	sendbyte(241,0);
	sendbyte(103,0);
	sendbyte(198,0);//LCD mapping Hierum
	//~ sendbyte(192,0);//LCD mapping Hierum
	sendbyte(64,0);
	sendbyte(80,0);
	sendbyte(43,0);
	sendbyte(235,0);
	sendbyte(129,0);
	sendbyte(95,0);
	sendbyte(137,0);
	uint16_t i;
	for(i=0;i<4160;i++)//RAM-Aussp�hlen, alles wei�
	{
		sendbyte(0,1);
	}
	//~ DDRC |= 1;//Debug-LED
	sendbyte(0,0);//RAM-Zeigerpostition zur�ck auf 0/0
	sendbyte(16,0);
	sendbyte(96,0);
	sendbyte(175,0);//Bildschirm an
	//~ PORTC |= 1;//Debug-LED an==Init zu Ende
}

void clear(void)
{
	uint16_t i;
	sendbyte(0,0);//RAM-Zeigerpostition zur�ck auf 0/0
	sendbyte(16,0);
	sendbyte(96,0);
	for(i=0;i<4160;i++)//RAM-Aussp�hlen, alles wei�
	{
		sendbyte(0,1);
	}
	sendbyte(0,0);//RAM-Zeigerpostition zur�ck auf 0/0
	sendbyte(16,0);
	sendbyte(96,0);
}

void sendbyte(uint8_t byte,uint8_t cd)
{
	int8_t i;
	if(cd)
		PORTB |= (1<<1);
	for(i=7;i>-1;i--)
	{
		PORTB &= ~(1<<5);
		if((1<<i)&byte)
			PORTB |= (1<<3);
		else
		{
			PORTB &= ~(1<<3);
		}
		PORTB |= (1<<5);
	}
	PORTB &= ~(1<<1);
}

void page(uint8_t x,uint8_t y,uint8_t h)//alle Pixel einer Page beschreiben
{
	sendbyte(15&x,0);//Adressentranslation
	sendbyte(16+(x>>4),0);
	sendbyte(96+y,0);
	sendbyte(h,1);
}

//own stuff
void blackDisplay(uint8_t x, uint8_t y){
	int _x = 0;
	int _y = 0;
	for (_y=0; _y< y; _y++){
		for (_x=0; _x< x; _x++){
		page(_x,_y,0xFF);	
		}
	}
}

void whiteDisplay(uint8_t x, uint8_t y){
	uint8_t _x = 0;
	uint8_t _y = 0;
	for (_y=0; _y< y; _y++){
		for (_x=0; _x< x; _x++){
		page(_x,_y,0x00);	
		}
	}
}

void drawRectangleAt(int width, int height, int x, int y){
	uint8_t _x = 0;
	uint8_t _y = 0;
	for (_y=0; _y< height/4; _y++){
		for (_x=0; _x< width; _x++){
		page(_x+x,_y+y,0xAA);	
		}
	}
}
void deleteRectangleAt(int width, int height, int x, int y){
	uint8_t _x = 0;
	uint8_t _y = 0;
	for (_y=0; _y< height/4; _y++){
		for (_x=0; _x< width; _x++){
			page(_x+x,_y+y,0x00);	
		}
	}
}

uint8_t drawLetter(const uint8_t posX, const uint8_t posY, const char letter){
	uint8_t returnValue = 0;
	switch(letter)
	{
		case '!' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x3);break;
		case '$' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xf0);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0xff);page(posX + 3, posY + 0, 0xc);page(posX + 4, posY + 0, 0xc);
            page(posX + 0, posY + 1, 0xf);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0xff);page(posX + 3, posY + 1, 0x30);page(posX + 4, posY + 1, 0x30);break;
		case '.' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0x0);
            page(posX + 0, posY + 1, 0x3);break;
		case '0' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '1' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x30);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0xff);page(posX + 3, posY + 0, 0x0);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x3f);page(posX + 3, posY + 1, 0x30);break;
		case '2' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xc3);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x30);break;
		case '3' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '4' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc0);page(posX + 2, posY + 0, 0xc0);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3f);break;
		case '5' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xc3);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '6' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xc3);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '7' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3f);break;
		case '8' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3f);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case '9' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x30);page(posX + 1, posY + 1, 0x30);page(posX + 2, posY + 1, 0x30);page(posX + 3, posY + 1, 0x3f);break;
		case ':' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0xcc);
            page(posX + 0, posY + 1, 0x0);break;
		case 'A' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'B' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xcc);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);break;
		case 'C' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'D' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x3);page(posX + 3, posY + 0, 0xfc);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);break;
		case 'E' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'F' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);break;
		case 'G' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xf3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'H' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x30);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'I' : returnValue = 1 + 1; page(posX + 0, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);break;
		case 'J' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xc3);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'K' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x30);page(posX + 2, posY + 0, 0xcc);page(posX + 3, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'L' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x0);page(posX + 2, posY + 0, 0x0);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'M' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xc);page(posX + 4, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'N' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xc0);page(posX + 4, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'O' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);break;
		case 'P' : returnValue = 3 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);break;
		case 'Q' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xc3);page(posX + 3, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'R' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0xf3);page(posX + 3, posY + 0, 0xc);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x3);break;
		case 'S' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3f);page(posX + 1, posY + 0, 0x33);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xf3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'T' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0x3);page(posX + 2, posY + 0, 0xff);page(posX + 3, posY + 0, 0x3);page(posX + 4, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x0);break;
		case 'U' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0x0);page(posX + 2, posY + 0, 0x0);page(posX + 3, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		case 'V' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3f);page(posX + 1, posY + 0, 0xc0);page(posX + 2, posY + 0, 0x0);page(posX + 3, posY + 0, 0xc0);page(posX + 4, posY + 0, 0x3f);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x0);break;
		case 'W' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0xff);page(posX + 1, posY + 0, 0xc0);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xc0);page(posX + 4, posY + 0, 0xff);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'X' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xcc);page(posX + 2, posY + 0, 0x30);page(posX + 3, posY + 0, 0xcc);page(posX + 4, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x0);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x3);break;
		case 'Y' : returnValue = 5 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xc);page(posX + 2, posY + 0, 0xf0);page(posX + 3, posY + 0, 0xc);page(posX + 4, posY + 0, 0x3);
            page(posX + 0, posY + 1, 0x0);page(posX + 1, posY + 1, 0x0);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x0);page(posX + 4, posY + 1, 0x0);break;
		case 'Z' : returnValue = 4 + 1; page(posX + 0, posY + 0, 0x3);page(posX + 1, posY + 0, 0xc3);page(posX + 2, posY + 0, 0x33);page(posX + 3, posY + 0, 0xf);
            page(posX + 0, posY + 1, 0x3);page(posX + 1, posY + 1, 0x3);page(posX + 2, posY + 1, 0x3);page(posX + 3, posY + 1, 0x3);break;
		default: returnValue = 4; page(posX + 0, posY + 0, 0x00);page(posX + 1, posY + 0, 0x00);page(posX + 2, posY + 0, 0x00);page(posX + 3, posY + 0, 0x00);
            page(posX + 0, posY + 1, 0x00);page(posX + 1, posY + 1, 0x00);page(posX + 2, posY + 1, 0x00);page(posX + 3, posY + 1, 0x00); break;
	}
	return returnValue;
}
void drawText(const uint8_t posX, const uint8_t posY, const char text[], const uint8_t textLength){
	uint8_t i = 0;
	uint8_t tempPos = posX;
	for(i = 0; i < textLength; i++){
		tempPos += drawLetter(tempPos, posY, text[i]);
	}
}

void drawText_pgm(const uint8_t posX, const uint8_t posY, const uint8_t text[], const uint8_t textLength){
    uint8_t i = 0;
	uint8_t tempPos = posX;
	for(i = 0; i < textLength; i++){
		tempPos += drawLetter(tempPos, posY, (char)pgm_read_byte(&text[i]));
	}
}



