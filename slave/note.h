// Defining BPM, Default: 90	 And multiply by 0.94488188976 to fit it
#define BPM 115
#define ms 60000

// Ghost note for synchronization of master & slave
#define sync 0, 0

// Defining basic tone frequencies
#define deathsound 256, 50
#define deathsound2 220, 60
#define deathsound3 190, 70

#define hitsound 160, 50
#define hitsound2 130, 50

#define C0 16
#define CIS0 17
#define D0 18
#define DIS0 19
#define E0 20
#define F0 21
#define FIS0 23
#define G0 24
#define GIS0 25
#define A0 27
#define AIS0 29
#define B0 30
#define C1 32
#define CIS1 34
#define D1 36
#define DIS1 38
#define E1 41
#define F1 43
#define FIS1 46
#define G1 48
#define GIS1 51
#define A1 55
#define AIS1 58
#define B1 61
#define C2 65
#define CIS2 69
#define D2 73
#define DIS2 77
#define E2 82
#define F2 87
#define FIS2 92
#define G2 97
#define GIS2 103
#define A2 110
#define AIS2 116
#define B2 123
#define C3 130
#define CIS3 138
#define D3 146
#define DIS3 155
#define E3 164
#define F3 174
#define FIS3 184
#define G3 195
#define GIS3 207
#define A3 220
#define AIS3 233
#define B3 246
#define C4 261
#define CIS4 277
#define D4 293
#define DIS4 311
#define E4 329
#define F4 349
#define FIS4 369
#define G4 391
#define GIS4 415
#define A4 440
#define AIS4 466
#define B4 493
#define C5 523
#define CIS5 554
#define D5 587
#define DIS5 622
#define E5 659
#define F5 698
#define FIS5 739
#define G5 783
#define GIS5 830
#define A5 880
#define AIS5 932
#define B5 987
#define C6 1046
#define CIS6 1108
#define D6 1174
#define DIS6 1244
#define E6 1318
#define F6 1396
#define FIS6 1479
#define G6 1567
#define GIS6 1661
#define A6 1760
#define AIS6 1864
#define B6 1975
#define C7 2093
#define CIS7 2217
#define D7 2349
#define DIS7 2489
#define E7 2637
#define F7 2793
#define FIS7 2959
#define G7 3135
#define GIS7 3322
#define A7 3520
#define AIS7 3729
#define B7 3951
//#define G 4 * ms / BPM
//#define H 2 * ms / BPM
//#define V ms / BPM
//#define A ms / (BPM * 2)
//#define S ms / (BPM * 4)

 // Defining whole notes {frequency, length}
#define C0G C0, 4 * ms / BPM
#define CIS0G CIS0, 4 * ms / BPM
#define D0G D0, 4 * ms / BPM
#define DIS0G DIS0, 4 * ms / BPM
#define E0G E0, 4 * ms / BPM
#define F0G F0, 4 * ms / BPM
#define FIS0G FIS0, 4 * ms / BPM
#define G0G G0, 4 * ms / BPM
#define GIS0G GIS0, 4 * ms / BPM
#define A0G A0, 4 * ms / BPM
#define AIS0G AIS0, 4 * ms / BPM
#define B0G B0, 4 * ms / BPM
#define C1G C1, 4 * ms / BPM
#define CIS1G CIS1, 4 * ms / BPM
#define D1G D1, 4 * ms / BPM
#define DIS1G DIS1, 4 * ms / BPM
#define E1G E1, 4 * ms / BPM
#define F1G F1, 4 * ms / BPM
#define FIS1G FIS1, 4 * ms / BPM
#define G1G G1, 4 * ms / BPM
#define GIS1G GIS1, 4 * ms / BPM
#define A1G A1, 4 * ms / BPM
#define AIS1G AIS1, 4 * ms / BPM
#define B1G B1, 4 * ms / BPM
#define C2G C2, 4 * ms / BPM
#define CIS2G CIS2, 4 * ms / BPM
#define D2G D2, 4 * ms / BPM
#define DIS2G DIS2, 4 * ms / BPM
#define E2G E2, 4 * ms / BPM
#define F2G F2, 4 * ms / BPM
#define FIS2G FIS2, 4 * ms / BPM
#define G2G G2, 4 * ms / BPM
#define GIS2G GIS2, 4 * ms / BPM
#define A2G A2, 4 * ms / BPM
#define AIS2G AIS2, 4 * ms / BPM
#define B2G B2, 4 * ms / BPM
#define C3G C3, 4 * ms / BPM
#define CIS3G CIS3, 4 * ms / BPM
#define D3G D3, 4 * ms / BPM
#define DIS3G DIS3, 4 * ms / BPM
#define E3G E3, 4 * ms / BPM
#define F3G F3, 4 * ms / BPM
#define FIS3G FIS3, 4 * ms / BPM
#define G3G G3, 4 * ms / BPM
#define GIS3G GIS3, 4 * ms / BPM
#define A3G A3, 4 * ms / BPM
#define AIS3G AIS3, 4 * ms / BPM
#define B3G B3, 4 * ms / BPM
#define C4G C4, 4 * ms / BPM
#define CIS4G CIS4, 4 * ms / BPM
#define D4G D4, 4 * ms / BPM
#define DIS4G DIS4, 4 * ms / BPM
#define E4G E4, 4 * ms / BPM
#define F4G F4, 4 * ms / BPM
#define FIS4G FIS4, 4 * ms / BPM
#define G4G G4, 4 * ms / BPM
#define GIS4G GIS4, 4 * ms / BPM
#define A4G A4, 4 * ms / BPM
#define AIS4G AIS4, 4 * ms / BPM
#define B4G B4, 4 * ms / BPM
#define C5G C5, 4 * ms / BPM
#define CIS5G CIS5, 4 * ms / BPM
#define D5G D5, 4 * ms / BPM
#define DIS5G DIS5, 4 * ms / BPM
#define E5G E5, 4 * ms / BPM
#define F5G F5, 4 * ms / BPM
#define FIS5G FIS5, 4 * ms / BPM
#define G5G G5, 4 * ms / BPM
#define GIS5G GIS5, 4 * ms / BPM
#define A5G A5, 4 * ms / BPM
#define AIS5G AIS5, 4 * ms / BPM
#define B5G B5, 4 * ms / BPM
#define C6G C6, 4 * ms / BPM
#define CIS6G CIS6, 4 * ms / BPM
#define D6G D6, 4 * ms / BPM
#define DIS6G DIS6, 4 * ms / BPM
#define E6G E6, 4 * ms / BPM
#define F6G F6, 4 * ms / BPM
#define FIS6G FIS6, 4 * ms / BPM
#define G6G G6, 4 * ms / BPM
#define GIS6G GIS6, 4 * ms / BPM
#define A6G A6, 4 * ms / BPM
#define AIS6G AIS6, 4 * ms / BPM
#define B6G B6, 4 * ms / BPM
#define C7G C7, 4 * ms / BPM
#define CIS7G CIS7, 4 * ms / BPM
#define D7G D7, 4 * ms / BPM
#define DIS7G DIS7, 4 * ms / BPM
#define E7G E7, 4 * ms / BPM
#define F7G F7, 4 * ms / BPM
#define FIS7G FIS7, 4 * ms / BPM
#define G7G G7, 4 * ms / BPM
#define GIS7G GIS7, 4 * ms / BPM
#define A7G A7, 4 * ms / BPM
#define AIS7G AIS7, 4 * ms / BPM
#define B7G B7, 4 * ms / BPM
#define PauseG 0, 4 * ms / BPM

 // Defining half notes {frequency, length}
#define C0H C0, 2 * ms / (BPM)
#define CIS0H CIS0, 2 * ms / (BPM)
#define D0H D0, 2 * ms / (BPM)
#define DIS0H DIS0, 2 * ms / (BPM)
#define E0H E0, 2 * ms / (BPM)
#define F0H F0, 2 * ms / (BPM)
#define FIS0H FIS0, 2 * ms / (BPM)
#define G0H G0, 2 * ms / (BPM)
#define GIS0H GIS0, 2 * ms / (BPM)
#define A0H A0, 2 * ms / (BPM)
#define AIS0H AIS0, 2 * ms / (BPM)
#define B0H B0, 2 * ms / (BPM)
#define C1H C1, 2 * ms / (BPM)
#define CIS1H CIS1, 2 * ms / (BPM)
#define D1H D1, 2 * ms / (BPM)
#define DIS1H DIS1, 2 * ms / (BPM)
#define E1H E1, 2 * ms / (BPM)
#define F1H F1, 2 * ms / (BPM)
#define FIS1H FIS1, 2 * ms / (BPM)
#define G1H G1, 2 * ms / (BPM)
#define GIS1H GIS1, 2 * ms / (BPM)
#define A1H A1, 2 * ms / (BPM)
#define AIS1H AIS1, 2 * ms / (BPM)
#define B1H B1, 2 * ms / (BPM)
#define C2H C2, 2 * ms / (BPM)
#define CIS2H CIS2, 2 * ms / (BPM)
#define D2H D2, 2 * ms / (BPM)
#define DIS2H DIS2, 2 * ms / (BPM)
#define E2H E2, 2 * ms / (BPM)
#define F2H F2, 2 * ms / (BPM)
#define FIS2H FIS2, 2 * ms / (BPM)
#define G2H G2, 2 * ms / (BPM)
#define GIS2H GIS2, 2 * ms / (BPM)
#define A2H A2, 2 * ms / (BPM)
#define AIS2H AIS2, 2 * ms / (BPM)
#define B2H B2, 2 * ms / (BPM)
#define C3H C3, 2 * ms / (BPM)
#define CIS3H CIS3, 2 * ms / (BPM)
#define D3H D3, 2 * ms / (BPM)
#define DIS3H DIS3, 2 * ms / (BPM)
#define E3H E3, 2 * ms / (BPM)
#define F3H F3, 2 * ms / (BPM)
#define FIS3H FIS3, 2 * ms / (BPM)
#define G3H G3, 2 * ms / (BPM)
#define GIS3H GIS3, 2 * ms / (BPM)
#define A3H A3, 2 * ms / (BPM)
#define AIS3H AIS3, 2 * ms / (BPM)
#define B3H B3, 2 * ms / (BPM)
#define C4H C4, 2 * ms / (BPM)
#define CIS4H CIS4, 2 * ms / (BPM)
#define D4H D4, 2 * ms / (BPM)
#define DIS4H DIS4, 2 * ms / (BPM)
#define E4H E4, 2 * ms / (BPM)
#define F4H F4, 2 * ms / (BPM)
#define FIS4H FIS4, 2 * ms / (BPM)
#define G4H G4, 2 * ms / (BPM)
#define GIS4H GIS4, 2 * ms / (BPM)
#define A4H A4, 2 * ms / (BPM)
#define AIS4H AIS4, 2 * ms / (BPM)
#define B4H B4, 2 * ms / (BPM)
#define C5H C5, 2 * ms / (BPM)
#define CIS5H CIS5, 2 * ms / (BPM)
#define D5H D5, 2 * ms / (BPM)
#define DIS5H DIS5, 2 * ms / (BPM)
#define E5H E5, 2 * ms / (BPM)
#define F5H F5, 2 * ms / (BPM)
#define FIS5H FIS5, 2 * ms / (BPM)
#define G5H G5, 2 * ms / (BPM)
#define GIS5H GIS5, 2 * ms / (BPM)
#define A5H A5, 2 * ms / (BPM)
#define AIS5H AIS5, 2 * ms / (BPM)
#define B5H B5, 2 * ms / (BPM)
#define C6H C6, 2 * ms / (BPM)
#define CIS6H CIS6, 2 * ms / (BPM)
#define D6H D6, 2 * ms / (BPM)
#define DIS6H DIS6, 2 * ms / (BPM)
#define E6H E6, 2 * ms / (BPM)
#define F6H F6, 2 * ms / (BPM)
#define FIS6H FIS6, 2 * ms / (BPM)
#define G6H G6, 2 * ms / (BPM)
#define GIS6H GIS6, 2 * ms / (BPM)
#define A6H A6, 2 * ms / (BPM)
#define AIS6H AIS6, 2 * ms / (BPM)
#define B6H B6, 2 * ms / (BPM)
#define C7H C7, 2 * ms / (BPM)
#define CIS7H CIS7, 2 * ms / (BPM)
#define D7H D7, 2 * ms / (BPM)
#define DIS7H DIS7, 2 * ms / (BPM)
#define E7H E7, 2 * ms / (BPM)
#define F7H F7, 2 * ms / (BPM)
#define FIS7H FIS7, 2 * ms / (BPM)
#define G7H G7, 2 * ms / (BPM)
#define GIS7H GIS7, 2 * ms / (BPM)
#define A7H A7, 2 * ms / (BPM)
#define AIS7H AIS7, 2 * ms / (BPM)
#define B7H B7, 2 * ms / (BPM)
#define PauseH 0, 2 * ms / (BPM)

 // Defining quarter notes {frequency, length}
#define C0V C0, ms / (BPM)
#define CIS0V CIS0, ms / (BPM)
#define D0V D0, ms / (BPM)
#define DIS0V DIS0, ms / (BPM)
#define E0V E0, ms / (BPM)
#define F0V F0, ms / (BPM)
#define FIS0V FIS0, ms / (BPM)
#define G0V G0, ms / (BPM)
#define GIS0V GIS0, ms / (BPM)
#define A0V A0, ms / (BPM)
#define AIS0V AIS0, ms / (BPM)
#define B0V B0, ms / (BPM)
#define C1V C1, ms / (BPM)
#define CIS1V CIS1, ms / (BPM)
#define D1V D1, ms / (BPM)
#define DIS1V DIS1, ms / (BPM)
#define E1V E1, ms / (BPM)
#define F1V F1, ms / (BPM)
#define FIS1V FIS1, ms / (BPM)
#define G1V G1, ms / (BPM)
#define GIS1V GIS1, ms / (BPM)
#define A1V A1, ms / (BPM)
#define AIS1V AIS1, ms / (BPM)
#define B1V B1, ms / (BPM)
#define C2V C2, ms / (BPM)
#define CIS2V CIS2, ms / (BPM)
#define D2V D2, ms / (BPM)
#define DIS2V DIS2, ms / (BPM)
#define E2V E2, ms / (BPM)
#define F2V F2, ms / (BPM)
#define FIS2V FIS2, ms / (BPM)
#define G2V G2, ms / (BPM)
#define GIS2V GIS2, ms / (BPM)
#define A2V A2, ms / (BPM)
#define AIS2V AIS2, ms / (BPM)
#define B2V B2, ms / (BPM)
#define C3V C3, ms / (BPM)
#define CIS3V CIS3, ms / (BPM)
#define D3V D3, ms / (BPM)
#define DIS3V DIS3, ms / (BPM)
#define E3V E3, ms / (BPM)
#define F3V F3, ms / (BPM)
#define FIS3V FIS3, ms / (BPM)
#define G3V G3, ms / (BPM)
#define GIS3V GIS3, ms / (BPM)
#define A3V A3, ms / (BPM)
#define AIS3V AIS3, ms / (BPM)
#define B3V B3, ms / (BPM)
#define C4V C4, ms / (BPM)
#define CIS4V CIS4, ms / (BPM)
#define D4V D4, ms / (BPM)
#define DIS4V DIS4, ms / (BPM)
#define E4V E4, ms / (BPM)
#define F4V F4, ms / (BPM)
#define FIS4V FIS4, ms / (BPM)
#define G4V G4, ms / (BPM)
#define GIS4V GIS4, ms / (BPM)
#define A4V A4, ms / (BPM)
#define AIS4V AIS4, ms / (BPM)
#define B4V B4, ms / (BPM)
#define C5V C5, ms / (BPM)
#define CIS5V CIS5, ms / (BPM)
#define D5V D5, ms / (BPM)
#define DIS5V DIS5, ms / (BPM)
#define E5V E5, ms / (BPM)
#define F5V F5, ms / (BPM)
#define FIS5V FIS5, ms / (BPM)
#define G5V G5, ms / (BPM)
#define GIS5V GIS5, ms / (BPM)
#define A5V A5, ms / (BPM)
#define AIS5V AIS5, ms / (BPM)
#define B5V B5, ms / (BPM)
#define C6V C6, ms / (BPM)
#define CIS6V CIS6, ms / (BPM)
#define D6V D6, ms / (BPM)
#define DIS6V DIS6, ms / (BPM)
#define E6V E6, ms / (BPM)
#define F6V F6, ms / (BPM)
#define FIS6V FIS6, ms / (BPM)
#define G6V G6, ms / (BPM)
#define GIS6V GIS6, ms / (BPM)
#define A6V A6, ms / (BPM)
#define AIS6V AIS6, ms / (BPM)
#define B6V B6, ms / (BPM)
#define C7V C7, ms / (BPM)
#define CIS7V CIS7, ms / (BPM)
#define D7V D7, ms / (BPM)
#define DIS7V DIS7, ms / (BPM)
#define E7V E7, ms / (BPM)
#define F7V F7, ms / (BPM)
#define FIS7V FIS7, ms / (BPM)
#define G7V G7, ms / (BPM)
#define GIS7V GIS7, ms / (BPM)
#define A7V A7, ms / (BPM)
#define AIS7V AIS7, ms / (BPM)
#define B7V B7, ms / (BPM)
#define PauseV 0, ms / (BPM)

 // Defining eighth notes {frequency, length}
#define C0A C0, ms / (BPM * 2)
#define CIS0A CIS0, ms / (BPM * 2)
#define D0A D0, ms / (BPM * 2)
#define DIS0A DIS0, ms / (BPM * 2)
#define E0A E0, ms / (BPM * 2)
#define F0A F0, ms / (BPM * 2)
#define FIS0A FIS0, ms / (BPM * 2)
#define G0A G0, ms / (BPM * 2)
#define GIS0A GIS0, ms / (BPM * 2)
#define A0A A0, ms / (BPM * 2)
#define AIS0A AIS0, ms / (BPM * 2)
#define B0A B0, ms / (BPM * 2)
#define C1A C1, ms / (BPM * 2)
#define CIS1A CIS1, ms / (BPM * 2)
#define D1A D1, ms / (BPM * 2)
#define DIS1A DIS1, ms / (BPM * 2)
#define E1A E1, ms / (BPM * 2)
#define F1A F1, ms / (BPM * 2)
#define FIS1A FIS1, ms / (BPM * 2)
#define G1A G1, ms / (BPM * 2)
#define GIS1A GIS1, ms / (BPM * 2)
#define A1A A1, ms / (BPM * 2)
#define AIS1A AIS1, ms / (BPM * 2)
#define B1A B1, ms / (BPM * 2)
#define C2A C2, ms / (BPM * 2)
#define CIS2A CIS2, ms / (BPM * 2)
#define D2A D2, ms / (BPM * 2)
#define DIS2A DIS2, ms / (BPM * 2)
#define E2A E2, ms / (BPM * 2)
#define F2A F2, ms / (BPM * 2)
#define FIS2A FIS2, ms / (BPM * 2)
#define G2A G2, ms / (BPM * 2)
#define GIS2A GIS2, ms / (BPM * 2)
#define A2A A2, ms / (BPM * 2)
#define AIS2A AIS2, ms / (BPM * 2)
#define B2A B2, ms / (BPM * 2)
#define C3A C3, ms / (BPM * 2)
#define CIS3A CIS3, ms / (BPM * 2)
#define D3A D3, ms / (BPM * 2)
#define DIS3A DIS3, ms / (BPM * 2)
#define E3A E3, ms / (BPM * 2)
#define F3A F3, ms / (BPM * 2)
#define FIS3A FIS3, ms / (BPM * 2)
#define G3A G3, ms / (BPM * 2)
#define GIS3A GIS3, ms / (BPM * 2)
#define A3A A3, ms / (BPM * 2)
#define AIS3A AIS3, ms / (BPM * 2)
#define B3A B3, ms / (BPM * 2)
#define C4A C4, ms / (BPM * 2)
#define CIS4A CIS4, ms / (BPM * 2)
#define D4A D4, ms / (BPM * 2)
#define DIS4A DIS4, ms / (BPM * 2)
#define E4A E4, ms / (BPM * 2)
#define F4A F4, ms / (BPM * 2)
#define FIS4A FIS4, ms / (BPM * 2)
#define G4A G4, ms / (BPM * 2)
#define GIS4A GIS4, ms / (BPM * 2)
#define A4A A4, ms / (BPM * 2)
#define AIS4A AIS4, ms / (BPM * 2)
#define B4A B4, ms / (BPM * 2)
#define C5A C5, ms / (BPM * 2)
#define CIS5A CIS5, ms / (BPM * 2)
#define D5A D5, ms / (BPM * 2)
#define DIS5A DIS5, ms / (BPM * 2)
#define E5A E5, ms / (BPM * 2)
#define F5A F5, ms / (BPM * 2)
#define FIS5A FIS5, ms / (BPM * 2)
#define G5A G5, ms / (BPM * 2)
#define GIS5A GIS5, ms / (BPM * 2)
#define A5A A5, ms / (BPM * 2)
#define AIS5A AIS5, ms / (BPM * 2)
#define B5A B5, ms / (BPM * 2)
#define C6A C6, ms / (BPM * 2)
#define CIS6A CIS6, ms / (BPM * 2)
#define D6A D6, ms / (BPM * 2)
#define DIS6A DIS6, ms / (BPM * 2)
#define E6A E6, ms / (BPM * 2)
#define F6A F6, ms / (BPM * 2)
#define FIS6A FIS6, ms / (BPM * 2)
#define G6A G6, ms / (BPM * 2)
#define GIS6A GIS6, ms / (BPM * 2)
#define A6A A6, ms / (BPM * 2)
#define AIS6A AIS6, ms / (BPM * 2)
#define B6A B6, ms / (BPM * 2)
#define C7A C7, ms / (BPM * 2)
#define CIS7A CIS7, ms / (BPM * 2)
#define D7A D7, ms / (BPM * 2)
#define DIS7A DIS7, ms / (BPM * 2)
#define E7A E7, ms / (BPM * 2)
#define F7A F7, ms / (BPM * 2)
#define FIS7A FIS7, ms / (BPM * 2)
#define G7A G7, ms / (BPM * 2)
#define GIS7A GIS7, ms / (BPM * 2)
#define A7A A7, ms / (BPM * 2)
#define AIS7A AIS7, ms / (BPM * 2)
#define B7A B7, ms / (BPM * 2)
#define PauseA 0, ms / (BPM * 2)

 // Defining sixteenth notes {frequency, length}
#define C0S C0, ms / (BPM * 4)
#define CIS0S CIS0, ms / (BPM * 4)
#define D0S D0, ms / (BPM * 4)
#define DIS0S DIS0, ms / (BPM * 4)
#define E0S E0, ms / (BPM * 4)
#define F0S F0, ms / (BPM * 4)
#define FIS0S FIS0, ms / (BPM * 4)
#define G0S G0, ms / (BPM * 4)
#define GIS0S GIS0, ms / (BPM * 4)
#define A0S A0, ms / (BPM * 4)
#define AIS0S AIS0, ms / (BPM * 4)
#define B0S B0, ms / (BPM * 4)
#define C1S C1, ms / (BPM * 4)
#define CIS1S CIS1, ms / (BPM * 4)
#define D1S D1, ms / (BPM * 4)
#define DIS1S DIS1, ms / (BPM * 4)
#define E1S E1, ms / (BPM * 4)
#define F1S F1, ms / (BPM * 4)
#define FIS1S FIS1, ms / (BPM * 4)
#define G1S G1, ms / (BPM * 4)
#define GIS1S GIS1, ms / (BPM * 4)
#define A1S A1, ms / (BPM * 4)
#define AIS1S AIS1, ms / (BPM * 4)
#define B1S B1, ms / (BPM * 4)
#define C2S C2, ms / (BPM * 4)
#define CIS2S CIS2, ms / (BPM * 4)
#define D2S D2, ms / (BPM * 4)
#define DIS2S DIS2, ms / (BPM * 4)
#define E2S E2, ms / (BPM * 4)
#define F2S F2, ms / (BPM * 4)
#define FIS2S FIS2, ms / (BPM * 4)
#define G2S G2, ms / (BPM * 4)
#define GIS2S GIS2, ms / (BPM * 4)
#define A2S A2, ms / (BPM * 4)
#define AIS2S AIS2, ms / (BPM * 4)
#define B2S B2, ms / (BPM * 4)
#define C3S C3, ms / (BPM * 4)
#define CIS3S CIS3, ms / (BPM * 4)
#define D3S D3, ms / (BPM * 4)
#define DIS3S DIS3, ms / (BPM * 4)
#define E3S E3, ms / (BPM * 4)
#define F3S F3, ms / (BPM * 4)
#define FIS3S FIS3, ms / (BPM * 4)
#define G3S G3, ms / (BPM * 4)
#define GIS3S GIS3, ms / (BPM * 4)
#define A3S A3, ms / (BPM * 4)
#define AIS3S AIS3, ms / (BPM * 4)
#define B3S B3, ms / (BPM * 4)
#define C4S C4, ms / (BPM * 4)
#define CIS4S CIS4, ms / (BPM * 4)
#define D4S D4, ms / (BPM * 4)
#define DIS4S DIS4, ms / (BPM * 4)
#define E4S E4, ms / (BPM * 4)
#define F4S F4, ms / (BPM * 4)
#define FIS4S FIS4, ms / (BPM * 4)
#define G4S G4, ms / (BPM * 4)
#define GIS4S GIS4, ms / (BPM * 4)
#define A4S A4, ms / (BPM * 4)
#define AIS4S AIS4, ms / (BPM * 4)
#define B4S B4, ms / (BPM * 4)
#define C5S C5, ms / (BPM * 4)
#define CIS5S CIS5, ms / (BPM * 4)
#define D5S D5, ms / (BPM * 4)
#define DIS5S DIS5, ms / (BPM * 4)
#define E5S E5, ms / (BPM * 4)
#define F5S F5, ms / (BPM * 4)
#define FIS5S FIS5, ms / (BPM * 4)
#define G5S G5, ms / (BPM * 4)
#define GIS5S GIS5, ms / (BPM * 4)
#define A5S A5, ms / (BPM * 4)
#define AIS5S AIS5, ms / (BPM * 4)
#define B5S B5, ms / (BPM * 4)
#define C6S C6, ms / (BPM * 4)
#define CIS6S CIS6, ms / (BPM * 4)
#define D6S D6, ms / (BPM * 4)
#define DIS6S DIS6, ms / (BPM * 4)
#define E6S E6, ms / (BPM * 4)
#define F6S F6, ms / (BPM * 4)
#define FIS6S FIS6, ms / (BPM * 4)
#define G6S G6, ms / (BPM * 4)
#define GIS6S GIS6, ms / (BPM * 4)
#define A6S A6, ms / (BPM * 4)
#define AIS6S AIS6, ms / (BPM * 4)
#define B6S B6, ms / (BPM * 4)
#define C7S C7, ms / (BPM * 4)
#define CIS7S CIS7, ms / (BPM * 4)
#define D7S D7, ms / (BPM * 4)
#define DIS7S DIS7, ms / (BPM * 4)
#define E7S E7, ms / (BPM * 4)
#define F7S F7, ms / (BPM * 4)
#define FIS7S FIS7, ms / (BPM * 4)
#define G7S G7, ms / (BPM * 4)
#define GIS7S GIS7, ms / (BPM * 4)
#define A7S A7, ms / (BPM * 4)
#define AIS7S AIS7, ms / (BPM * 4)
#define B7S B7, ms / (BPM * 4)
#define PauseS 0, ms / (BPM * 4)

// Pause for soundeffects
#define PauseZ 0, 5
