import math

note = ["C", "CIS", "D", "DIS", "E", "F", "FIS", "G", "GIS", "A", "AIS", "B"]
frequency = []

for i in range(-4 * 12 + 1,12 * 4 + 1):
	frequency.append((440 * math.pow(2, (i-10) / 12.)))

f = open("note.h", "w")
f.write("// Defining BPM, Default: 90\t And multiply by 0.94488188976 to fit it\n #define BPM 90 * 0.83989501312 \n")
f.write("#define ms 60000\n\n")
f.write("// Defining basic tone frequencies\n")

counter = 0
for i in range(0, len(frequency)):
	f.write("#define " + note[i % len(note)] + str(counter) + " " + str(int(frequency[i])) + "\n")
	if(i % len(note) == 11):
		counter += 1

f.write("\n // Defining whole notes {frequency, length}\n")

counter = 0
for i in range(0, len(frequency)):
	f.write("#define " + note[i % len(note)] + str(counter) + "G "
					   + note[i % len(note)] + str(counter) + ", "
					   + "4 * ms / BPM\n")
	if(i % len(note) == 11):
		counter += 1
f.write("#define PauseG 0, 4 * ms / BPM\n")
f.write("\n // Defining half notes {frequency, length}\n")

counter = 0
for i in range(0, len(frequency)):
	f.write("#define " + note[i % len(note)] + str(counter) + "H "
					   + note[i % len(note)] + str(counter) + ", "
					   + "2 * ms / (BPM)\n")
	if(i % len(note) == 11):
		counter += 1
f.write("#define PauseH 0, 2 * ms / (BPM)\n")
f.write("\n // Defining quarter notes {frequency, length}\n")

counter = 0
for i in range(0, len(frequency)):
	f.write("#define " + note[i % len(note)] + str(counter) + "V "
					   + note[i % len(note)] + str(counter) + ", "
					   + "ms / (BPM)\n")
	if(i % len(note) == 11):
		counter += 1
f.write("#define PauseV 0, ms / (BPM)\n")
f.write("\n // Defining eighth notes {frequency, length}\n")

counter = 0
for i in range(0, len(frequency)):
	f.write("#define " + note[i % len(note)] + str(counter) + "A "
					   + note[i % len(note)] + str(counter) + ", "
					   + "ms / (BPM * 2)\n")
	if(i % len(note) == 11):
		counter += 1
f.write("#define PauseA 0, ms / (BPM * 2)\n")
f.write("\n // Defining sixteenth notes {frequency, length}\n")

counter = 0
for i in range(0, len(frequency)):
	f.write("#define " + note[i % len(note)] + str(counter) + "S "
					   + note[i % len(note)] + str(counter) + ", "
					   + "ms / (BPM * 4)\n")
	if(i % len(note) == 11):
		counter += 1
f.write("#define PauseS 0, ms / (BPM * 4)\n")
f.close()
