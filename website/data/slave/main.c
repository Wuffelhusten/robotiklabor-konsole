#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include "uart.h"
#include "note.h"
//#include "tracks.h"


#define beforeBeat 125

// CODE: http://www.ermicro.com/blog/?p=1971
// Notes Frequency from http://www.phy.mtu.edu/~suits/notefreqs.html

uint16_t intervall = 60000 / BPM;

uint16_t j = 0;
uint16_t z = 0;
uint8_t merchantTheme = 1;
uint8_t zelda = 1;
uint8_t bone = 1;
uint8_t disco = 1;
uint8_t storms = 1;
uint8_t title = 1;
	
// variables for Sound Effects
uint8_t soundOn = 1;
uint16_t lengthOfSoundEffect = 0;
char c = '0';
uint16_t itt = 0;
uint16_t tmp2 =0;

// variables used for the synthesizer
uint16_t i = 0;
uint32_t sendSync = 0; //gives the move Signal
unsigned int top_value = 0;
unsigned int duty_cycle = 0;
uint16_t note_length = 0;

// important for before beat movement 
uint8_t timeWindow = 0; // how long I have to wait until i can count sendsync up

// Funktion to play Soundeffect
const static uint16_t hitEnemy[][2] PROGMEM = {
	{PauseZ},{hitsound},{hitsound2},{PauseZ}
};
const static uint16_t killEnemy[][2] PROGMEM = {
	//{PauseZ},{A4S},{A4S},{A4S},{A4A},{F4A},{G4A},{A4S},{PauseS},{G4S},{A4V},{PauseZ}
	{PauseZ},{deathsound},{deathsound2},{deathsound3},{PauseZ}
};
const static uint16_t tem_shop[][2] PROGMEM = {
	{FIS3A},{AIS3A},{CIS6A},{AIS5A},{FIS5A},{GIS5S},{FIS5A},{CIS5S},{DIS5S},{FIS5S},
	{B3A},{DIS5A},{DIS5A},{FIS5A},{F5A},{FIS5S},{GIS5A},{GIS3S},{F4A},
	{FIS3A},{AIS3A},{CIS6A},{AIS5A},{FIS5A},{GIS5S},{FIS5A},{CIS5S},{DIS5S},{FIS5S},
	{A5S},{AIS5S},{GIS5A},{GIS5S},{FIS5S},{DIS5A},{DIS5A},{FIS5A},{FIS5A},{FIS3A},
	{FIS3A},{AIS3A},{CIS6A},{AIS5A},{FIS5A},{GIS5S},{FIS5A},{CIS5S},{DIS5S},{FIS5S},
	{B3A},{DIS5A},{DIS5A},{FIS5A},{F5A},{FIS5S},{GIS5A},{GIS3S},{F4A},
	{FIS3A},{AIS3A},{CIS6A},{AIS5A},{FIS5A},{GIS5S},{FIS5A},{CIS5S},{DIS5S},{FIS5S},
	{A5S},{AIS5S},{GIS5A},{GIS5S},{FIS5S},{DIS5A},{DIS5A},{FIS5A},{FIS5A},{FIS3A},
	{B3A},{DIS4A},{DIS5A},{DIS5A},{DIS5A},{FIS5S},{F5A},{DIS5S},{CIS5S},{DIS5S},
	{FIS3A},{AIS3A},{AIS4A},{AIS4A},{GIS4A},{AIS4S},{B4A},{AIS4S},{GIS4A},
	{B3A},{DIS4A},{DIS5A},{DIS5A},{DIS5A},{FIS5S},{F5A},{DIS5S},{CIS5S},{DIS5S},
	{CIS4A},{F4A},{F5A},{CIS5A},{F5A},{GIS5A},{FIS5A},{F5A},
	{FIS3A},{AIS3A},{AIS4A},{AIS4A},{GIS4A},{AIS4S},{B4A},{AIS4S},{GIS4A},
	{B3A},{DIS4A},{DIS5A},{DIS5A},{DIS5A},{FIS5S},{F5A},{DIS5S},{CIS5S},{DIS5S},
	{FIS3A},{AIS3A},{AIS4A},{AIS4A},{GIS4A},{AIS4S},{B4A},{AIS4S},{GIS4A},
	{CIS4A},{F4A},{F5A},{CIS5A},{F5A},{GIS5A},{FIS5A},{F5A}
};
const static uint16_t song_of_healing[][2] PROGMEM = {
	{sync},{B4A},{C4A},{A4A},{C4A},{F4A},{C4A},
	{sync},{B4A},{C4A},{A4A},{C4A},{F4A},{C4A},
	{sync},{B4A},{C4A},{A4A},{C4A},{E4A},{D4A},
	{sync},{E4V},{D3A},{D3V},{D3A},
	{sync},{B4A},{C4A},{A4A},{C4A},{F4A},{C4A},
	{sync},{B4A},{C4A},{A4A},{C4A},{F4A},{C4A},
	{sync},{B4A},{C4A},{A4A},{C4A},{E4A},{D4A},
	{sync},{E4V},{E4A},{D3A},{E4A},{D3A},
	{sync},{F4A},{A3A},{C4A},{A3A},{B3A},{A3A},
	{sync},{F4A},{A3A},{C4A},{A3A},{B3A},{A3A},
	{sync},{F4A},{G3A},{E4A},{G3A},{B3A},{A3A}, 
	{sync},{B3V},{C4A},{G3A},{B3A},{G3A},
	{sync},{F4A},{A3A},{C4A},{A3A},{B3A},{A3A},
	{sync},{F4A},{A3A},{C4A},{A3A},{B3A},{A3A},
	{sync},{F4A},{G3A},{E4A},{G3A},{B4A},{G3A},
	{sync},{G4A},{G3A},{C4A},{G3A},{B4A},{G3A},
	{sync},{A4A},{F3A},{A4A},{F3A},{A4A},{F3A},
	{sync},{D5A},{F3A},{D5A},{F3A},{D5A},{F3A},
	{sync},{G4A},{E3A},{G4A},{E3A},{G4A},{E3A},
	{sync},{C5A},{E3A},{G4V},{C4A},{D3A},
	{sync},{F4A},{D3A},{F4A},{D3A},{F4A},{D3A},
	{sync},{AIS4A},{D3A},{AIS4A},{D3A},{AIS4A},{D3A},
	{sync},{E4A},{C3A},{D4A},{C3A},{A4A},{C3A},
	{sync},{E4V},{A3A},{C3A},{G3A},{C3A},
	{sync},{A4A},{F3A},{A4A},{F3A},{A4A},{F3A},
	{sync},{D5A},{F3A},{C5A},{F3A},{D5A},{F3A},
	{sync},{G4A},{E3A},{G4A},{E3A},{G4A},{E3A},
	{sync},{C5A},{E3A},{G4V},{C4A},{E3A},
	{sync},{F4A},{D3A},{G4A},{D3A},{A4A},{D3A},     
	{sync},{AIS4A},{D3A},{C5A},{D3A},{D5A},{D3A},
	{sync},{A4A},{F3A},{B4A},{F3A},{D5A},{F3A},
	{sync},{E5H},{B3V}
};
const static uint16_t disco_descent[][2] PROGMEM = {
	//page one
	{sync},{A2S},{A2S},{PauseV},{A2S},{A2S},{PauseV},{C3S},{C3S},{PauseA},
	{sync},{A2S},{A2S},{PauseV},{A2S},{A2S},{PauseV},{A2S},{A2S},{PauseA},
	{sync},{A2S},{A2S},{PauseV},{A2S},{A2S},{PauseV},{C3S},{C3S},{PauseA},
	{sync},{A2S},{A2S},{PauseV},{A2S},{A2S},{PauseH},
	{sync},{A2A},{C4A},{A2A},{C4A},{A2A},{C4A},{A2A},{C4A},
	{sync},{G2A},{C4A},{G2A},{C4A},{G2A},{B3A},{G2A},{B3A},
	{sync},{F2A},{A3A},{F2A},{A3A},{F2A},{A3A},{F2A},{A3A},
	{sync},{D2A},{A3A},{D2A},{A3A},{E2A},{GIS3A},{E2A},{GIS3A},
	{sync},{A2A},{E4A},{A2A},{E4A},{A2A},{E4A},{A2A},{E4A},
	{sync},{G2A},{E4A},{G2A},{E4A},{G2A},{E4A},{G2A},{E4A},
	{sync},{F2A},{F4A},{F2A},{F4A},{F2A},{F4A},{F2A},{F4A},
	{sync},{D2A},{D4A},{D2A},{D4A},{E2A},{GIS4A},{E2A},{GIS4A},
	{sync},{A2A},{A3A},{A2A},{A3A},{A2A},{A3A},{A2A},{A3A},
	{sync},{A2A},{A3A},{A2A},{A3A},{A2A},{A3A},{A2A},{A3A},
	{sync},{A3S},{E3S},{C3S},{A2S},{E3S},{C3S},{A2S},{E2S},{A3S},{E3S},{C3S},{A2S},{E3S}, 
	{C3S},{A2S},{E2S},
	{sync},{C4S},{A3S},{E3S},{C3S},{A3S},{E3S},{C3S},{A2S},{A4S},{E4S},{C4S},{A3S},{C5S}, 
	{A4S},{E4S},{C4S},
	{sync},{A2A},{A3S},{G3S},{A4V},{A2A},{A3A},{A2A},{G4S},{A4S},
	{sync},{B4A},{C5A},{B4S},{C5S},{B4S},{G4S},
	{B4S},{PauseS},{C5S},{PauseS},{B4S},{G4A},{PauseS},
	{sync},{A4H},{F2A},{F3A},{F2A},{F4S},{G4S},
	{sync},{A4V},{E2A},{E3A},{GIS4V},{E2A},{E3A},
	//page two
	{sync},{A4S},{PauseA},{A4S},{E5V},{F2A},{A3A},{F2A},{D5S},{E5S},
	{sync},{F5A},{E5S},{F5S},{G5A},{F5S},{G5S},{C6A},{B5A},{A5A},{G5A},
	{sync},{A5H},{F3A},{F2A},{F2A},{F5S},{G5S},
	{sync},{A5V},{E2A},{E3S},{GIS5S},{B5V},{E5V},
	{sync},{E5H},{D5V},{C3A},{E5S},{D5S},
	{sync},{C5H},{B4V},{E4V},
	{sync},{A4V},{F4V},{A4V},{C5A},{B4S},{A4S},
	{sync},{B4V},{D2A},{A4S},{GIS4S},{A4V},{GIS4A},{E4A},
	{sync},{A2A},{A4S},{C5S},{A2A},{C5S},{C5S},{A2A},{C5S},{C5S},{A2A},{B4S},{A4S},
	{sync},{A2A},{A4S},{A4S},{A2A},{A4S},{A4S},{A2A},{A4S},{A4S},{A2A},{A4S},{E4S},
	{sync},{A2A},{E4S},{E4S},{A2A},{E4S},{E4S},{A2A},{E4S},{E4S},{A2A},{D4S},{C4S},
	{sync},{A2A},{C4S},{C4S},{A2A},{C4S},{C4S},{A2A},{C4S},{C4S},{A2A},{B3S},{A3S},
	{sync},{A3A},{A3A},{A3A},{PauseS},{A3S},{E4V},{A2A},{C4S},{D4S},
	{sync},{E4H},{D4V},{G4A},{F4S},{E4S},
	{sync},{DIS4S},{PauseS},{DIS4S},{DIS4S},{PauseS},{DIS4S},{DIS4S},{PauseS},
	{C4S},{C4S},{B3S},{C4S},{DIS4S},{DIS4S},{B3S},{C4S},
	{sync},{A2A},{B3S},{B3S},{A4S},{PauseS},{B3S},{B3S},{A3S},{PauseS},
	{B3S},{A4S},{A2A},{A4S},{B4S},
	{sync},{A4V},{F2A},{A4S},{B4S},{GIS4A},{E4S},{A4A},{E4S},{B4S},{E4S},
	{sync},{C5V},{A4V},{A4S},{C5S},{A5A},{E5V},
	{sync},{DIS5V},{DIS2A},{DIS5S},{E5S},{F5V},{F4A},{C5S},{B4S},
	//page three cant be translated
	//page four
	{sync},{A2S},{A2S},{A4S},{A4S},{A2S},{PauseS},{A4S},{A4S},{A2S},{PauseS},{A4S},{A4S},{A2S},{PauseS},{A4S},{A4S}, //53
	{sync},{A2S},{A2S},{C5S},{C5S},{A2S},{PauseS},{C5S},{C5S},{A2S},{PauseS},{B4S},{B4S},{A2S},{PauseS},{B4S},{B4S},
	{sync},{A2S},{A2S},{A4S},{A4S},{A2S},{PauseS},{A4S},{A4S},{A2S},{PauseS},{A4S},{A4S},{A2S},{PauseS},{A4S},{A4S}, //55
	{sync},{GIS2S},{GIS2S},{GIS4S},{GIS4S},{GIS2S},{PauseS},{GIS4S},{GIS4S},{GIS2S},{PauseS},{GIS4S},{E4S},{GIS2S},{PauseS},{GIS4S},{E4S},
	{sync},{A2A},{A3A},{A2A},{E4V},{A3A},{E4A},{C4S},{D4S},
	{sync},{E4S},{PauseS},{F4S},{PauseS},{E4S},{F4S},{E4S},{C4S},{E4S},{PauseS},{F4S},{PauseS},{E4S},{C4A},{PauseS}, //58
	{sync},{D4V},{F2A},{F3A},{C4V},{A3A},{C4A},
	{sync},{B3V},{E2A},{A3S},{GIS3S},{A3V},{GIS3A},{E3A},
	{sync},{A3A},{A3A},{E4V},{A2A},{A3A},{E4A},{C4S},{D4S}, //61
	{sync},{E4A},{D4S},{E4S},{F4A},{E4S},{F4S},{G4A},{F4S},{G4S},{C5A},{B4A},
	{sync},{A4V},{F2A},{F3A},{F4V},{F2A},{C4S},{A4S},
	{sync},{GIS4V},{GIS4S},{E4S},{GIS4S},{A4S},{B4V},{D5V},
	{sync},{E5V},{C2A},{C3A},{G5V},{E5A},{G5A}, //65
	{sync},{A5V},{A5A},{GIS5S},{FIS5S},{GIS5V},{E5A},{G5A},
	{sync},{A5V},{F5V},{A5V},{C6V},
	{sync},{D6V},{D6A},{C6S},{D6S},{E6V},{E2A},{E3A},
	{sync},{A2A},{E4A},{A2A},{E4A},{A2A},{E4A},{A2A},{E4A}, //69
	{sync},{A2A},{E4A},{A2A},{E4A},{A2A},{E4A},{A2A},{E4A}
};

const static uint16_t zelda_the_dark_world[][2] PROGMEM = {
    {sync},{A2A},{E4A},{E4A},{E4S},{E4S},{E4A},{FIS4V},{A2A},
    {sync},{A2A},{G4A},{G4A},{G4S},{G4S},{G4A},{AIS4V},{A2A},
    {sync},{A2A},{E4A},{E4A},{E4S},{E4S},{E4A},{FIS4V},{A2A},
    {sync},{A2A},{G4A},{G4A},{G4S},{G4S},{G4A},{AIS4A},{A2A},{A3A},
    //end intro
    {sync},{E4V},{A2V},{E4A},{A3A},{E4A},{A4A},
    {sync},{FIS4S},{G4S},{FIS4A},{FIS4V},{D3V},{D3A},{PauseS},{D4S},
    {sync},{E4V},{A3V},{F2V},{F2A},{PauseS},{D4S},
    {sync},{B3S},{C4S},{B3A},{B3V},{G2V},{G2A},{PauseS},{G3S},
    {sync},{A3V},{E3A},{E3A},{E3A},{FIS3A},{PauseA},{A4S},{E5S},
    {sync},{B4V},{G3A},{G3A},{G3A},{AIS3A},{PauseA},{A3A},
    //end first bracket
    {sync},{E4V},{A2V},{E4A},{A3A},{E4A},{A4A},
    {sync},{FIS4S},{G4S},{FIS4A},{FIS4V},{D3V},{D3A},{PauseS},{D4S},
    {sync},{E4V},{A3V},{F2V},{F2A},{PauseS},{D4S},
    {sync},{B3S},{C4S},{B3A},{B3V},{G2V},{G2A},{PauseS},{G3S},
    //begin of second bracket
    {sync},{A3V},{E3A},{E3A},{E3A},{FIS3A},{D5S},{C5S},{B4S},{A4S},
    {sync},{B4V},{G3A},{G3A},{G3A},{A3A},{PauseA},{E5S},{FIS5S},
    {sync},{G5V},{PauseA},{G3A},{G3A},{G3A},{E5A},{G5A},
    {sync},{FIS5S},{E5S},{D5A},{D5V},{FIS3A},{FIS3A},{PauseA},{C5S},{D5S}, 
    {sync},{E5V},{PauseA},{A3A},{A3A},{A3A},{A4A},{E5A},
    {sync},{D5S},{C5S},{B4A},{B4V},{G3A},{G3A},{PauseA},{E5S},{FIS5S},
    {sync},{G5V},{PauseA},{G3A},{G3A},{G3A},{E5A},{G5A},
    {sync},{FIS5S},{G5S},{A5A},{A5V},{FIS3A},{FIS3A},{PauseA},{FIS3A}, //18
    {sync},{G5V},{PauseA},{AIS3A},{AIS3A},{AIS3A},{FIS5A},{G5A},
    {sync},{FIS5S},{F5S},{FIS5A},{FIS5V},{FIS3A},{FIS3A},{PauseA},{FIS3A},
    {sync},{G5V},{B3A},{B3A},{CIS4A},{CIS4A},{PauseA},{E3A},
    //2. Seite
    {sync},{E3A},{D4A},{D4A},{D4A},{CIS4A},{CIS4A},{PauseA},{E3A}, //22
    {sync},{A4S},{E4S},{D4S},{E4S},{E4V},{A3A},{A3A},{A4V},
    {sync},{G4S},{E4S},{D4S},{E4S},{E4V},{G3A},{G3A},{G3V},
    {sync},{G4S},{D4S},{C4S},{D4S},{D4V},{F3A},{F3A},{G4V},
    {sync},{F4S},{C4S},{A3S},{C4S},{C4V},{F3A},{F3A},{F3V}, //26
    {sync},{E4S},{B3S},{A3S},{B3S},{B3V},{F3A},{F3A},{E4V},
    {sync},{A4S},{E4S},{D4S},{E4S},{E4V},{E3A},{E3A},{E4V},
    {sync},{B4V},{PauseA},{A3A},{A3A},{A3A},{A3V},
    {sync},{B4V},{PauseA},{A3A},{A3A},{GIS3A},{GIS3V}     
};
const static uint16_t Bonetrousle[][2] PROGMEM = {
    // Seite 1
    {sync},{C3A},{G3A},{C3A},{A3A},{C3A},{G3A},{C3A},{F3A},
    {sync},{C3A},{G3A},{C3A},{A3A},{C3A},{G3A},{C3A},{F3A},
    {sync},{C3A},{G3A},{C3A},{A3A},{C3A},{G3A},{C3A},{F3A},
    {sync},{C3A},{G3A},{C3A},{A3A},{C3A},{G3A},{C3A},{F3A},
    {sync},{C3A},{C4A},{G4A},{C5A},{DIS5V},{C5V}, //5
    {sync},{B3A},{C4A},{D5V},{C5A},{G4A},{DIS4A},{C4A},
    {sync},{D4A},{DIS4A},{FIS4V},{G4A},{DIS4A},{C4A},{A3A},
    {sync},{B3A},{C4A},{D4V},{C4V},{C3A},{F3A},
    {sync},{C3A},{C4A},{G4A},{C5A},{DIS5V},{G5S},{A5S},{AIS5A}, //9
    {sync},{A5A},{G5A},{A5V},{AIS5A},{G5A},{DIS5A},{C5A},
    {sync},{B4A},{C5A},{D5V},{DIS5A},{C5A},{G4A},{DIS4A},
    {sync},{B4A},{A4A},{B4S},{A4S},{B4A},{C5V},{C3A},{F3A}, //12
    {sync},{F3A},{F4A},{GIS4A},{C5A},{F5V},{C5V},
    {sync},{B4A},{C5A},{D5S},{C5S},{D5A},{B4A},{G4A},{B4A},{D5A},
    {sync},{DIS5A},{F5S},{DIS5S},{D5A},{C5A},{C6V},{AIS5A},{GIS5A},
    {sync},{G5A},{GIS5S},{G5S},{F5A},{GIS5A},{G5V},{G3A},{D4A}, //16
    {sync},{F3A},{F4A},{GIS4A},{C5A},{F5V},{C5V},
    {sync},{B4A},{C5A},{D5S},{C5S},{D5A},{B4A},{G4A},{B4A},{D5A},
    {sync},{DIS5A},{F5S},{DIS5S},{D5A},{C5A},{C6V},{D6A},{DIS6A},    
    // Seite 2
    {sync},{D6A},{C6A},{B5A},{GIS5A},{G5S},{GIS5S},{G5A},{DIS3A},{D3A}, //20
    {sync},{PauseG},
    {sync},{C2A},{G3A},{C2A},{A3A},{C2A},{G3A},{C2A},{F3A},
    {sync},{C2A},{G3A},{C2A},{A3A},{C2A},{G3A},{C2A},{F3A},
    {sync},{C2A},{G3A},{C2A},{A3A},{C2A},{G3A},{C2A},{F3A}, //24
    {sync},{C2A},{G3A},{C2A},{A3A},{C2A},{G3A},{C2A},{F3A},
    {sync},{C2A},{C4A},{G4A},{C5A},{DIS5V},{C5V},
    {sync},{B3A},{C4A},{D5V},{C5A},{G4A},{DIS4A},{C4A},
    {sync},{D4A},{DIS4A},{FIS4V},{G4A},{DIS4A},{C4A},{A3A}, //28
    {sync},{B3A},{C4A},{D4V},{C4V},{C2A},{F3A}, 
    {sync},{C2A},{C4A},{G4A},{C5A},{DIS5V},{G5S},{A5S},{AIS5A}, 
    {sync},{A5A},{G5A},{A5V},{AIS5A},{G5A},{DIS5A},{C5A}, //31
    {sync},{B4A},{C5A},{D5V},{DIS5A},{C5A},{G4A},{DIS4A},
    {sync},{B4A},{A4A},{B4S},{A4S},{B4A},{C5V},{C2A},{F3A},
    {sync},{C2A},{DIS5A},{C5A},{C5A},{DIS5V},{C5V},{GIS5V}, //34
    {sync},{G5A},{F5A},{G5V},{C6A},{G5A},{DIS5A},{C5A},
    {sync},{DIS5A},{DIS5A},{F5V},{G5A},{DIS5A},{DIS5A},{DIS5A},
    {sync},{B4A},{A4A},{D5V},{C5V},{C2A},{F3A},
    {sync},{C2A},{C4A},{G4A},{C5A},{DIS5V},{G5S},{A5S},{AIS5A}, //38
    {sync},{A5A},{G5A},{A5V},{AIS5A},{G5A},{DIS5A},{C5A},
    {sync},{B4A},{C5A},{D5V},{DIS5A},{C5A},{G4A},{DIS4A},       
    // Seite 3
    {sync},{B4A},{A4A},{B4S},{A4S},{C5V},{PauseV}, //41
    {sync},{F3A},{F4A},{GIS4A},{C5A},{F5V},{C5V},
    {sync},{B4A},{C5A},{D5S},{C5S},{D5A},{B4A},{G4A},{B4A},{D5A},
    {sync},{DIS5A},{F5S},{DIS5S},{D5A},{C5A},{C6V},{AIS5A},{GIS5A},  //44
    {sync},{G5A},{GIS5S},{G5S},{F5A},{GIS5A},{G5A},{G3A},{D4A},
    {sync},{F3A},{F4A},{GIS4A},{C5A},{F5V},{C5V},
    {sync},{B4A},{C5A},{D5S},{C5S},{D5A},{B4A},{G5A},{B4A},{D5A},
    {sync},{DIS5A},{F5S},{DIS5S},{D5A},{C5A},{C6V},{D6A},{DIS6A},  //48
    {sync},{D6A},{C6A},{B5A},{GIS5A},{G5S},{GIS5S},{G5A},{DIS3A},{D3A},
    {sync},{F3A},{F4A},{GIS4A},{C5A},{F5V},{C5V},
    {sync},{B4A},{C5A},{D5S},{C5S},{D5A},{B4A},{G4A},{B4A},{D5A},  //51
    {sync},{DIS5A},{F5S},{DIS5S},{D5A},{C5A},{C6V},{AIS5A},{GIS5A},
    {sync},{G5A},{GIS5S},{G5S},{F5A},{GIS5A},{G5V},{G3A},{D4A},
    {sync},{F3A},{F4A},{GIS4A},{C5A},{F5V},{GIS5V},
    {sync},{G5A},{D5A},{B4V},{G4V},{D5S},{DIS5S},{D5A},  //55
    {sync},{C5A},{B4A},{C5A},{D5A},{DIS5V},{C5V},
    {sync},{B4A},{C5A},{D5V},{C5V},{C6V}
};
const static uint16_t song_of_storms[][2] PROGMEM = {
    //172 BPM
    {D3V},{A4V},{A4V},
    {E3A},{E4A},{B4H},
    {F3V},{C5V},{C5V},
    {E3A},{E4A},{B4H},
    {D3V},{A4V},{A4V},
    {E3A},{E4A},{B4H},
    {F3V},{C5V},{C5V},
    {E3A},{E4A},{B4H},
    {D4A},{F4A},{D5V},{A3V},
    {D4A},{F4A},{D5H},
    {E5V},{C4V},{E5A},{F5A},
    {E5A},{C5A},{A4H},
    {A4V},{D4V},{F4A},{G4A},
    {A4H},{F3V},
    {A4V},{D4V},{F4A},{G4A},
    {E4H},{E3V},
    {D4A},{F4A},{D5V},{A3V},
    {D4A},{F4A},{D5H},
    {E5V},{C4V},{E5A},{F5A},
    {E5A},{C5A},{A4H},
    {A4V},{D4V},{F4A},{G4A},
    {A4H},{A4V},
    {D4V},{A4V},{A4V},
    {E3A},{E4A},{B4H},
    {F3V},{C5V},{C5V},
    {E3A},{E4A},{B4H},
    {D3V},{A4V},{A4V},
    {E3A},{E4A},{B4H},
    {F3V},{C5V},{C5V},
    {E3A},{E4A},{B4H}, //30
    {D5A},{F5A},{D6V},{A3V},
    {D5A},{F5A},{D6H},
    {E6V},{C4A},{F6A},{E6A},{F6A},
    {E6A},{C6A},{A5H},
    {A5V},{D5V},{F5A},{G5A},
    {A5V},{F4H},
    {A5V},{D5V},{F5A},{G5A},
    {E5V},{E4H},
    {D5A},{F5A},{D6V},{A3V}, //39
    {D5A},{F5A},{D6H},
    {E6V},{C4A},{F6A},{E6A},{F6A},
    {E6A},{C6A},{A5H},
    {A5V},{D5V},{F5A},{G5A},
    {A5V},{E4V},{E4V},
    {D5V},{A5V},{A5V},
    {E4A},{E5A},{B5H},
    {F4V},{C6V},{C6V},
    {E4A},{E5A},{B5H},
    {D5V},{A5V},{A5V},
    {E4A},{E5A},{B5H},
    {F4V},{C6V},{C6V},
    {E4A},{E5A},{B5H},
    {A5G}
};


void play(); //playSoundEffect needs declaration

uint8_t playSoundEffect(char c) {
	
	soundOn = 0;   
	if(c == 'h'){ // play hit sound
		c = '0';
		for(itt = 0; itt < sizeof(hitEnemy) / sizeof(hitEnemy[0]); itt++){
          play(pgm_read_word(&(hitEnemy[itt][0])), 
          pgm_read_word(&(hitEnemy[itt][1])));     
        }
        soundOn = 1;
    }else if(c == 'd'){ // play death sound
    	c = '0';
		for(itt = 0; itt < sizeof(killEnemy) / sizeof(killEnemy[0]); itt++){
          play(pgm_read_word(&(killEnemy[itt][0])), 
          pgm_read_word(&(killEnemy[itt][1])));     
        }
        soundOn = 1;
    }else if(c == 's'){
    	j = 0;
    	timeWindow = 0;
    	note_length = 0;
    	c = '0';
    	sendSync = 0;
    	TCCR1B &= ~(1<<CS11);
    }else if(c == 'f'){ // play merchant musik
    	c = '0';
    	soundOn = 1;
		while(1){
            play(pgm_read_word(&(tem_shop[itt][0])), 
		    pgm_read_word(&(tem_shop[itt][1])));
            if(itt == (sizeof(tem_shop) / sizeof(tem_shop[0]))){
                itt = 0;
            }
            if(merchantTheme == 0){
                break;
            }
            itt++;
        }
        soundOn = 1;
        itt = 0;
        _delay_ms(10);
        merchantTheme = 1;
        
    }else if(c == 'F'){
        c = '0';
        _delay_ms(5);
        merchantTheme = 0;
        _delay_ms(5);
    }else if(c == 'z'){
        zelda = 1;
        c = '0';
        sendSync = 0;
        soundOn = 1;
        j = 0;
		while(1){
            play(pgm_read_word(&(zelda_the_dark_world[j][0])), 
		    pgm_read_word(&(zelda_the_dark_world[j][1])));
            if(j == (sizeof(zelda_the_dark_world) / sizeof(zelda_the_dark_world[0]))){
                j = 0;
            }
            if(zelda == 0){
                break;
            }
            j++;
        }
        soundOn = 1;
        j = 0;
        _delay_ms(10);
        sendSync = 0;
    }else if(c == 'Z'){
        c = '0';
        zelda = 0;
    }else if(c == 'b'){
        bone = 1;
        c = '0';
        sendSync = 0;
        soundOn = 1;
        j = 0;
        intervall = 417;
		while(1){
            play(((pgm_read_word(&(Bonetrousle[j][0])) * 4 ) / 5), 
		    pgm_read_word(&(Bonetrousle[j][1])));
            if(j == (sizeof(Bonetrousle) / sizeof(Bonetrousle[0]))){
                j = 0;
            }
            if(bone == 0){
                break;
            }
            j++;
        }
        soundOn = 1;
        c = '0';
        intervall = 60000 / BPM;
        sendSync = 0;
        TCCR1B &= ~(1<<CS11); 
        _delay_ms(20);
    }else if(c == 'B'){
        c = '0';
        bone = 0;
    }else if(c == 'l'){
        title = 1;
        c = '0';
        sendSync = 0;
        soundOn = 1;
        j = 0;
		while(1){
            play(pgm_read_word(&(song_of_healing[j][0])), 
		    pgm_read_word(&(song_of_healing[j][1])));
            if(j == (sizeof(song_of_healing) / sizeof(song_of_healing[0]))){
                j = 0;
            }
            if(title == 0){
                break;
            }
            j++;
        }
        soundOn = 1;
        j = 0;
        _delay_ms(10);
        sendSync = 0;
    }else if(c == 'O'){
        c = '0';
        title = 0;
    }else if(c == 'c' || c == 'L'){
        disco = 1;
        c = '0';
        sendSync = 0;
        soundOn = 1;
        j = 0;
		while(1){
            play(pgm_read_word(&(disco_descent[j][0])), 
		    pgm_read_word(&(disco_descent[j][1])));
            if(j == (sizeof(disco_descent) / sizeof(disco_descent[0]))){
                j = 0;
            }
            if(disco == 0){
                break;
            }
            j++;
        }
        soundOn = 1;
        j = 0;
        _delay_ms(10);
        sendSync = 0;
    }else if(c == 'C'){
        c = '0';
        disco = 0;
    }else if(c == 'q'){ // q = song of storms
        storms = 1;
        c = '0';
        sendSync = 0;
        soundOn = 1;
        j = 0;
		while(1){
            play(((pgm_read_word(&(song_of_storms[j][0])) * 4 ) / 5), 
		    pgm_read_word(&(song_of_storms[j][1])));
            if(j == (sizeof(song_of_storms) / sizeof(song_of_storms[0]))){
                j = 0;
            }
            if(storms == 0){
                break;
            }
            j++;
        }
        soundOn = 1;
        j = 0;
        _delay_ms(10);
        sendSync = 0;
    }else if(c == 'Q'){
        lengthOfSoundEffect = 0;
        j = 0;
    	timeWindow = 0;
    	note_length = 0;
    	c = '0';
    	sendSync = 0;
    	TCCR1B &= ~(1<<CS11); 
    	storms = 0;
    	zelda = 0;
    	bone = 0;
    	disco = 0;
    	_delay_ms(10);
    }else if(c == 'r'){
    	return 0;
    /*}else if(c == 'L'){
        titleTheme = 0;*/
	}else{
		c = '0';
	    soundOn = 1;
		return 255;
	}
    return 255;
}


void play(uint16_t frequency, uint16_t length){
    
    //Soundeffect Setup
    if(soundOn == 0){
		lengthOfSoundEffect += length;
	}else if(lengthOfSoundEffect > 0){
		if(lengthOfSoundEffect >= length){
			lengthOfSoundEffect -= length;
			length = 0;
			return;
		} else {
			length -= lengthOfSoundEffect;
			lengthOfSoundEffect = 0;
		}
	}
    //end SoundEffect Setup  
    
    if(length == 0){ // sync note!
        //uart_putc('m');
        sendSync = 0;
        return;
    }
      
            
    if(frequency < 20){ // Falls: PAUSE
        for (i = 0; i < length; i++){
        
            //check for soundeffect
	    	c = uart_getc_no_blocking();
			if(c != '0'){
		    	tmp2 = playSoundEffect(c);
		    	if(tmp2 == 0){
			        return;
		        }
		    	if((length - i) > lengthOfSoundEffect){
		    	    tmp2 = lengthOfSoundEffect;
		            lengthOfSoundEffect = 0;
		            play(0, (length - i - tmp2));
		        }else{
		            lengthOfSoundEffect -= (length - i);
		        }
				return;
	        }
	        //end check for soundeffect
        
            _delay_ms(1);
            if(timeWindow > 0){ 
            	timeWindow--;
            	continue;
            }
            sendSync++;
            if(sendSync >= intervall - beforeBeat){
                uart_putc('m');
                sendSync = 0;
                timeWindow = beforeBeat;
            }
        }
        return;
    }
   

   // Calculate the Top Value
   // TOP = Board Clock Frequency / (2 x N x Notes Frequency)
   // Where N is Prescler: 8
   top_value = (F_CPU / (16 * frequency));

   // Reset the TIMER1 16 bit Counter
   TCNT1H = 0;
   TCNT1L = 0;

   // Set the TIMER1 Counter TOP value on ICR1H and ICR1L
   ICR1H = (top_value >> 8 ) & 0x00FF;
   ICR1L = top_value;   

   // Set the TIMER1 PWM Duty Cycle on OCR1AH and OCR1AL
   // Always use half of the TOP value (PWM Ducty Cycle ~ 50%)
   duty_cycle = top_value / 2;

   OCR1AH = (duty_cycle >> 8) & 0x00FF;
   OCR1AL = duty_cycle;
   
   // Turn ON the TIMER1 Prescaler of 8
   TCCR1B |= (1<<CS11); 

   // Notes Delay Duration
   note_length = (length * 4) / 5;
   for (i = 0; i < note_length; i++){
   
        //check for soundeffect
	    c = uart_getc_no_blocking();
		if(c != '0'){
			TCCR1B &= ~(1<<CS11);
			tmp2 = playSoundEffect(c);
			if(tmp2 == 0){
				return;
			}
			if((length - i) > lengthOfSoundEffect){
			    if((note_length - i) > lengthOfSoundEffect){
			        tmp2 = lengthOfSoundEffect;
					lengthOfSoundEffect = 0;
		            play(frequency, (length - i - tmp2));
		        }else{
		            tmp2 = lengthOfSoundEffect;
		            lengthOfSoundEffect = 0;
		            play(0, (length - i - tmp2));
		        }
		    }else{
		        lengthOfSoundEffect -= (length - i);
		    }
			return;
	    }
		//end check for soundeffect
   
        _delay_ms(1);
        if(timeWindow > 0){
           	timeWindow--;
           	continue;
        }
        sendSync++;
        if(sendSync >= intervall - beforeBeat){
            uart_putc('m');
            sendSync = 0;
            timeWindow = beforeBeat;
        }
   }
   
   // Turn OFF the TIMER1 Prescaler of 8
   TCCR1B &= ~(1<<CS11);

   // Delay Between Each Notes
   note_length = length / 5;
   for (i = 0; i < note_length; i++){
   
        //check for soundeffect
		c = uart_getc_no_blocking();
		if(c != '0'){
			tmp2 = playSoundEffect(c);
			if(tmp2 == 0){
				return;
			}
			if((note_length -i ) > lengthOfSoundEffect){
			    tmp2 = lengthOfSoundEffect;
			    lengthOfSoundEffect = 0;
			    play(0, (note_length -i - lengthOfSoundEffect));
			}else{
			    lengthOfSoundEffect -= (note_length - i);
			}
			return;
	    }
	    //end check for soundeffect
   
        _delay_ms(1);
        if(timeWindow > 0){
           	timeWindow--;
           	continue;
        }
        sendSync++;
        if(sendSync >= intervall - beforeBeat){
            uart_putc('m');
            sendSync = 0;
            timeWindow = beforeBeat;
        }
   }
}


// suppress implicit declaration warning
void soundInit();

int main(void)
{
	uartInit();
	//timerInit();
	soundInit();

	const static uint16_t flohWalzer[][2] PROGMEM = {
		{sync},{DIS4A},{CIS4A},{FIS3V},{FIS4V},{FIS4V},
		{sync},{DIS4A},{CIS4A},{FIS3V},{FIS4V},{FIS4V},
		{sync},{DIS4A},{CIS4A},{FIS3V},{FIS4V},{DIS3V},
		{sync},{FIS4V},{CIS3V},{F4V},{F4V},
		{sync},{DIS4A},{CIS4A},{DIS3V},{F4V},{F4V},
		{sync},{DIS4A},{CIS4A},{DIS3V},{F4V},{F4V},
		{sync},{DIS4A},{CIS4A},{DIS3V},{F4V},{DIS4V},
		{sync},{F4V},{F3V},{FIS4V},{FIS4V},
		{sync},{DIS4A},{CIS4A},{AIS4V},{FIS4V},{FIS4V},
		{sync},{DIS4A},{CIS4A},{AIS4V},{FIS4V},{FIS4V},
		{sync},{DIS4A},{CIS4A},{AIS4V},{FIS4V},{CIS5V},
		{sync},{FIS4V},{DIS5V},{F4V},{F4V}
	};
    const static uint16_t song_of_nothing[][2] PROGMEM = {
        {PauseG},{PauseG},{PauseG},{PauseG}
    };
    
    
    
	/*while(1){
	    if(uart_getc() == 'p'){
		    break;
	    }
	}*/
	
	z = 0;
    while(1){
        play(pgm_read_word(&(song_of_nothing[z][0])), 
		pgm_read_word(&(song_of_nothing[z][1])));
        if(z == (sizeof(song_of_nothing) / sizeof(song_of_nothing[0]))){
            z = 0;
        }
        z++;
    }

	/*while(1){
		if(uart_getc() == 'r') {
			break;
		}
	}
	
	//_delay_ms(100);
	while(1){
	    play(pgm_read_word(&(disco_descent[j][0])), 
		pgm_read_word(&(disco_descent[j][1])));
        if(j == (sizeof(disco_descent) / sizeof(disco_descent[0]))){
            j = 0;
        }
        j++;
	}*/

	return 0;	           // Standard Return Code
}

void soundInit(){
  // Initial PORT Used
  DDRB |= (1<<1);   // Set PB0 as Input and other as Output
  PORTB = 0x00;

  // Initial TIMER1 Phase and Frequency Correct PWM
  // Set the Timer/Counter Control Register
  TCCR1A = 0xC0; // Set OC1A when up counting, Clear when down counting
  TCCR1B = 0x10; // Phase/Freq-correct PWM, top value = ICR1, Prescaler: Off    	  

}


