/* 
 *	Basis
 *	2009 Benjamin Reh
 */

#include <avr/io.h>

//Pulsweite initialisieren
void PWMInit()
{
	//Pin B1 to output
	DDRB |= (1 << 1);

	//Timer/Counter als 8-Bit Fast PWM mit Compare Match
	TCCR1A = (1<<WGM12)|(1<<WGM11)|(1<<WGM10)|(1<<COM1A1)|(1<<COM1A0);

	TCCR1B = (1<<CS10);			     // Takt von CK / 1 generieren
}

//Pulsweite setzen
void setPWM(uint8_t pwmWert)
{
	//Wert in das Register schreiben
	OCR1A = pwmWert; //OC1A = B1
}
