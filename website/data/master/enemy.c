#include <inttypes.h>
#include "adc.h"
#include "enemy.h"
#include "sprites.h"

uint8_t currentEnemyID = 0;

void createEnemy(Enemy* e, uint8_t roomType, uint8_t levelIDcounter) {
    e->ID = currentEnemyID;
    e->position[0] = 0;
	e->position[1] = 0;
	
	if (roomType == 0) {
	    createSlime(e);
	    return;
	}
	uint8_t type = generateRand(0, 20);
	if(levelIDcounter == 1){
		if (type < 10) {
			createSlime(e);
		}else if (type < 13) {
		    createHeavy(e); 
		} else{
		    createGhost(e);
		}
	}else {
		if (type < 10) {
			createSlime(e); 
		} else if (type < 14) {
		    createGhost(e);
		} else if (type < 18) {
		    createSkeleton(e);
		} else if (type < 20) {
		    createHeavy(e);
		}
	}
    currentEnemyID++;
	return;
}

void createSlime(Enemy* e) {
	e->size[0] = 1;
	e->size[1] = 1;
	e->health = 2;
	e->damage = 1;
    e->moneyValue = 2;

	e->moveTime = 2;
	e->movementLength = 2;
	e->movementStep = 0;
	e->movement = &slime_moveset[0][0];
	
	e->remainingStunnedTicks = 0;
	e->movementState = IDLE;
	e->enemyType = WANDERING;

	e->sprite1 = &slime_sprite1[0][0];
	e->sprite2 = &slime_sprite2[0][0];
}


void createSkeleton(Enemy* e) {
	e->size[0] = 2;
	e->size[1] = 2;
	e->health = 4;
	e->damage = 3;
	e->moneyValue = 10;

	e->moveTime = 3;
	e->movementLength = 2;
	e->movementStep = 0;
	e->movement = &skeleton_moveset[0][0];
	
	e->remainingStunnedTicks = 0;
	e->movementState = IDLE;
	e->enemyType = FOLLOWING;
	
	e->sprite1 = &skeleton_sprite1[0][0];
	e->sprite2 = &skeleton_sprite2[0][0];
}

void createHeavy(Enemy* e){
	e->size[0] = 2;
	e->size[1] = 2;
	e->health = 4;
	e->damage = 4;
	e->moneyValue = 8;

	e->moveTime = 1;
	e->movementLength = 2;
	e->movementStep = 0;
	e->movement = &skeleton_moveset[0][0];
	
	e->remainingStunnedTicks = 0;
	e->movementState = IDLE;
	e->enemyType = CHARGING;
	
	e->sprite1 = &heavy_sprite1[0][0];
	e->sprite2 = &heavy_sprite2[0][0];

}
void createGhost(Enemy* e) {
	e->size[0] = 1;
	e->size[1] = 1;
	e->health = 1;
	e->damage = 1;
	e->moneyValue = 4;

	e->moveTime = 2;
	e->movementLength = 2;
	e->movementStep = 0;
	e->movement = &slime_moveset[0][0];
	
	e->remainingStunnedTicks = 0;
	e->movementState = IDLE;
	e->enemyType = RANDOM;

	e->sprite1 = &pacmanGhost_sprite1[0][0];
	e->sprite2 = &pacmanGhost_sprite2[0][0];
}

// BOSSES
void createSlimeBoss(Enemy* e){
	e->ID = currentEnemyID++;
    e->position[0] = 0;
	e->position[1] = 0;
	e->size[0] = 3;
	e->size[1] = 3;
	e->health = 10;
	e->damage = 3;
	e->moneyValue = 50;

	e->moveTime = 2;
	e->movementLength = 2;
	e->movementStep = 0;
	e->movement = &slime_moveset[0][0]; // doesnt matter. -> follows player
	
	e->remainingStunnedTicks = 0;
	e->movementState = IDLE;
	e->enemyType = FOLLOWING;

	e->sprite1 = &SlimeBoss_sprite1[0][0];
	e->sprite2 = &SlimeBoss_sprite2[0][0];
}

// Minibosses
// slime boss killed -> spawn 2 slime miniBosses
// slime miniBoss killed -> spawn 2 slimes
void createSlimeMiniBoss(Enemy* e){
    e->position[0] = 0;
	e->position[1] = 0;
	e->size[0] = 2;
	e->size[1] = 2;
	e->health = 5;
	e->damage = 2;
	e->moneyValue = 25;

	e->moveTime = 2;
	e->movementLength = 2;
	e->movementStep = 0;
	e->movement = &slime_moveset[0][0]; // doesnt matter. -> follows player
	
	e->remainingStunnedTicks = 2; // stunned on spawn, so that player has time to react
	e->movementState = STUNNED;
	e->enemyType = FOLLOWING;

	e->sprite1 = &SlimeMiniBoss_sprite1[0][0];
	e->sprite2 = &SlimeMiniBoss_sprite2[0][0];
}

void createSlimeMiniMiniBoss(Enemy* e) {
	e->size[0] = 1;
	e->size[1] = 1;
	e->health = 2;
	e->damage = 1;
    e->moneyValue = 2;

	e->moveTime = 2;
	e->movementLength = 2;
	e->movementStep = 0;
	e->movement = &slime_moveset[0][0];
	
	e->remainingStunnedTicks = 0;
	e->movementState = IDLE;
	e->enemyType = FOLLOWING;

	e->sprite1 = &slime_sprite1[0][0];
	e->sprite2 = &slime_sprite2[0][0];
}


//movesets for enemies
// slime
const uint8_t slime_moveset[2][2] = {{UP}, {DOWN}};
//skeleton
const uint8_t skeleton_moveset[2][2] = {{LEFT}, {RIGHT}};
