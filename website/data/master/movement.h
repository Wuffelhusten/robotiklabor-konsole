#ifndef MOVEMENT_H
#define MOVEMENT_H

#include "drawEntities.h"

void movePlayer(Player* p, Level* l, Enemy* enemies, const uint8_t dir); //moves player based on direction, also calls changeRoom if necessary
uint8_t checkEnemiesInPattern(Level* l, Player* p, Enemy* enemies, const uint8_t dir); // checks if an enemy is in weapon pattern and deals damage to it

void damageToEnemy(Player* p, Enemy* e, Room* room); // player damages Enemy
void damageToBOSS(Player* p, Enemy* enemies, Room* room, uint8_t damagedEnemyID); // player damages Enemy, but in Boss_ROOM

void moveEnemy(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p); // moves an enemy
void moveWandering(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p); // handles enemies which move according to their moveset
void moveFollowing(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p);// handles enemies that are following the player
void moveCharging(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p); //handles enemies which are charging at the player
void moveRandom(Enemy* e, const uint8_t noOfMovements, Room* room, Player* p);
//void changeRoom(Player* p, Level* level, Enemy* enemies, const uint8_t direction); //changes the Room the player is in, & saves the old room

void damageToPlayer(const Enemy* e, Player* p); // enemy damages player

void changeRoom(Player* p, Level* level, Enemy* enemies, const uint8_t direction); //changes the Room the player is in, & saves the old room

// helper functions for pathfinding, maybe check if they collide with libraries...
uint8_t abso(int x);
int sign(int x);

// helper fct for damageToBoss
uint8_t getMinimumFreeEnemyID(const Enemy* enemies);
uint8_t getFreeDirection(const Room* room, const Enemy* e);

#endif
