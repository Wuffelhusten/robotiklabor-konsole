#ifndef ENEMY_H
#define ENEMY_H

#include<avr/pgmspace.h>

#define UP 127, 126
#define DOWN 127, 128
#define LEFT 126, 127
#define RIGHT 128, 127

extern uint8_t currentEnemyID;

typedef enum{
    IDLE = 0,
    WALK_TOWARDS_PLAYER = 1,
    CHARGE_LEFT = 2,
    CHARGE_RIGHT = 3,
    CHARGE_UP = 4,
    CHARGE_DOWN = 5,
    STUNNED = 6
} MovementState;

typedef enum{
    STATIONARY = 0,             //does nothing
    WANDERING = 1,              // just does his movement pattern
    FOLLOWING = 2,              // walks towards player
    CHARGING = 3,    // charges towards player if it "sees" him
	RANDOM = 4 		// moves in random directions
         //TODO: More types...

} EnemyType;

typedef struct {
	uint8_t ID;				// unique for every enemy in a specific room
	uint8_t size[2];		// {width, height} given in fields of the room
	uint8_t position[2];	// {posX, posY} is calculated into field positions
	uint8_t health;			// health of the enemy given in amount of half hearts
	uint8_t damage;			// damage of the enemy given in amount of half hearts
	uint8_t moneyValue;     // money the player gets when killing the enemy // UNIQUE FOR BOSSES!!! SLIME BOSS = 50, slime miniboss = 20 -> used i damageTo enemy for special death functions

	// movement stuff
	uint8_t moveTime;		// time between the movement
	uint8_t movementLength; // says how much steps in a movement pattern is
	uint8_t movementStep;	// says at which step of the movement the enemy is
	const uint8_t* movement;// gives a fixed movement pattern that can be used during a movement
	
	uint8_t remainingStunnedTicks; // counts how many Ticks [4tel] the Enemy is till stunned
    MovementState movementState;	// says whether the enemy will walk towards the player
    EnemyType enemyType;
    

	// graphic stuff
	const uint8_t* sprite1;		// first sprite of the enemy animation
	const uint8_t* sprite2;		// second sprite of the enemy animation
} Enemy;


void createEnemy(Enemy* e, uint8_t roomType, uint8_t levelIDcounter);
void createSlime(Enemy* e);
void createSkeleton(Enemy* e);
void createHeavy(Enemy* e);
void createGhost(Enemy* e);

// Bosses

// slime boss killed -> spawn 2 slime miniBosses
void createSlimeBoss(Enemy* e);


// Mini-Bosses

// slime miniBoss killed -> spawn 2 slimes
void createSlimeMiniBoss(Enemy* e);
void createSlimeMiniMiniBoss(Enemy* e);


// movesets for enemies
extern const uint8_t slime_moveset[2][2]; //TODO PROGMEM
extern const uint8_t skeleton_moveset[2][2];


#endif
