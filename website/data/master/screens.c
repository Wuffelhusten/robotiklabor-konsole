#include <inttypes.h>
#include "screens.h"

const uint8_t anleitung0[] PROGMEM = "SPIELANLEITUNG:";
const uint8_t anleitung1[] PROGMEM = "BEWEGE DICH IM TAKT DER MUSIK.";
const uint8_t anleitung2[] PROGMEM = "DER SCHLEIM ZEIGT AN WANN DU DICH";
const uint8_t anleitung3[] PROGMEM = "BEWEGEN KANNST.";
const uint8_t anleitung4[] PROGMEM = "BESIEGE ALLE GEGNER IM RAUM INDEM";
const uint8_t anleitung5[] PROGMEM = "DU DICH AUF SIE BEWEGST!";
const uint8_t anleitung6[] PROGMEM = "EIN PORTAL BRINGT DICH IN EINE";
const uint8_t anleitung7[] PROGMEM = "NEUE EBENE.";
const uint8_t anleitung8[] PROGMEM = "B: ZURUECK!";
const uint8_t anleitung9[] PROGMEM = "IM LADEN KANN MAN ITEMS KAUFEN.";
const uint8_t anleitung10[] PROGMEM = "! FELDER SIND FALLEN";
const uint8_t anleitung11[] PROGMEM = "EINFACHER MODUS: TASTEN MUESSEN";
const uint8_t anleitung12[] PROGMEM = "NICHT JEDEN BEAT NEU GEDRUECKT";
const uint8_t anleitung13[] PROGMEM = "WERDEN.";
const uint8_t menu1[] PROGMEM = "START";
const uint8_t menu2[] PROGMEM = "ANLEITUNG";
const uint8_t menu3[] PROGMEM = "BESTENLISTE";
const uint8_t menu4[] PROGMEM = "NORMAL";
const uint8_t menu5[] PROGMEM = "EINFACH";



const uint8_t screenMiddlePoint[2] = { SCREEN_WIDTH / 2 , SCREEN_HEIGHT / 2 };
uint16_t score = 0;

// posX & posY defines the starting position of the screen to be drawed
void drawScreen(const Screen screen) {
	uint8_t i, j, posX, posY;

	switch (screen) {
		// Whitescreen
	case (WHITE_SCREEN):
		for (i = 0; i < SCREEN_HEIGHT; i++) {
			for (j = 0; j < SCREEN_WIDTH; j++) {
				page(j, i, 0x00);
			}
		}
		return;

		// Titlescreen
	case (TITLE_SCREEN):
		posX = screenMiddlePoint[0] - 66 / 2;
		posY = 7;
		for (i = 0; i < 9; i++) {
			for (j = 0; j < 66; j++) {
				page(posX + j, posY + i, pgm_read_byte(&titleScreen[i][j]));
			}
		}
		return;

	case (MENU_SCREEN):
	    drawScreen(WHITE_SCREEN);
		drawText_pgm(66,8,menu1,5);
		drawText_pgm(66,10,menu2,9);
		drawText_pgm(66,12,menu3,11);  
		
		return;
		
		
    case (MENU_EASY):;
		drawText_pgm(66,14,menu4,7);
		return;
		
    case (MENU_NORMAL):;
		drawText_pgm(66,14,menu5,7);
		return;
		
        //bonfire Setup
    case (BONFIRE_SETUP):
        posX = 15;
        posY = 6;
        for (i = 0; i < 3; i++) {
			for (j = 0; j < 6; j++) {
				page(posX + j, posY + i, pgm_read_byte(&bonfireTop_sprite[i][j]));
			}
		}
		posX = 16;
		posY = 9;
		for (i = 0; i < 8; i++) {
			for (j = 0; j < 23; j++) {
				page(posX + j, posY + i, pgm_read_byte(&bonfireMiddle_sprite1[i][j]));
			}
		}
        posX = 12;
        posY = 17;
        for (i = 0; i < 4; i++) {
			for (j = 0; j < 43; j++) {
				page(posX + j, posY + i, pgm_read_byte(&bonfireBottom_sprite[i][j]));
			}
		}
		return;
        
        //bonfire fire animation
    case (BONFIRE1):    
        posX = 16;
		posY = 9;
		for (i = 0; i < 8; i++) {
			for (j = 0; j < 23; j++) {
				page(posX + j, posY + i, pgm_read_byte(&bonfireMiddle_sprite1[i][j]));
			}
		}
		return;
    
        //bonfire fire animation
    case (BONFIRE2):
        posX = 16;
		posY = 9;
		for (i = 0; i < 8; i++) {
			for (j = 0; j < 23; j++) {
				page(posX + j, posY + i, pgm_read_byte(&bonfireMiddle_sprite2[i][j]));
			}
		}
		return;
        
        //slimes for selecting in the main Menu
    case (ARROW_UP):
        deleteRectangleAt(6,8,55,10);
        deleteRectangleAt(6,8,55,14);
        posX = 55;
        posY = 8;
        for (i = 0; i < 2; i++) {
			for (j = 0; j < 6; j++) {
				page(posX + j, posY + i, pgm_read_byte(&arrow[i][j]));
			}
		}
		return;
        
        //slimes for selecting in the main Menu
    case (ARROW_MIDDLE):
        deleteRectangleAt(6,8,55,8);
        deleteRectangleAt(6,8,55,12);
        posX = 55;
        posY = 10;
        for (i = 0; i < 2; i++) {
			for (j = 0; j < 6; j++) {
				page(posX + j, posY + i, pgm_read_byte(&arrow[i][j]));
			}
		}
		return;
		
	case (ARROW_MIDDLE_DOWN):
        deleteRectangleAt(6,8,55,10);
        deleteRectangleAt(6,8,55,14);
        posX = 55;
        posY = 12;
        for (i = 0; i < 2; i++) {
			for (j = 0; j < 6; j++) {
				page(posX + j, posY + i, pgm_read_byte(&arrow[i][j]));
			}
		}
		return;	
        
        //slimes for selecting in the main Menu
    case (ARROW_BOTTOM):
        deleteRectangleAt(6,8,55,8);
        deleteRectangleAt(6,8,55,12);
        posX = 55;
        posY = 14;
        for (i = 0; i < 2; i++) {
			for (j = 0; j < 6; j++) {
				page(posX + j, posY + i, pgm_read_byte(&arrow[i][j]));
			}
		}
		return;
    
        //draw HOW TO PLAY screen
    case(HOW_TO_PLAY):
        // clear the whole screen
		drawScreen(WHITE_SCREEN);
		drawText_pgm(2,1,anleitung0,15);
		drawText_pgm(4,4,anleitung1,30);
		drawText_pgm(4,7,anleitung2,33);
        drawText_pgm(4,9,anleitung3,15);
        drawText_pgm(4,12,anleitung4,33);
        drawText_pgm(4,14,anleitung5,24);
        drawText_pgm(4,17,anleitung6,30);
        drawText_pgm(4,19,anleitung7,11);
        drawText_pgm(2,24,anleitung8,11);		
        //PSTR("SPIELANLEITUNG:")
		
        posX = 120;
        posY = 24;
        for (i = 0; i < 2; i++) {
			for (j = 0; j < 6; j++) {
				page(posX + j, posY + i, pgm_read_byte(&arrow[i][j]));
			}
		}
		return;
    
    case(HOW_TO_PLAY2):
        
        drawScreen(WHITE_SCREEN);
        
        drawText_pgm(2,1,anleitung0,15);
        drawText_pgm(4,4,anleitung9,31);
        drawText_pgm(4,7,anleitung10,20);
        drawText_pgm(4,10,anleitung11,31);
        drawText_pgm(4,12,anleitung12,30);
		drawText_pgm(4,14,anleitung13,7);
        drawText_pgm(2,24,anleitung8,11);
        
        posX = 120;
        posY = 24;
        for (i = 0; i < 2; i++) {
			for (j = 0; j < 6; j++) {
				page(posX + j, posY + i, pgm_read_byte(&arrow[i][j]));
			}
		}
    
        return;
      
		// Deathscreen
	case (DEATH_SCREEN):
		// clear the whole screen
		drawScreen(WHITE_SCREEN);

		posX = screenMiddlePoint[0] - 67 / 2;
		posY = screenMiddlePoint[1] - 11 / 2;
		for (i = 0; i < 11; i++) {
			for (j = 0; j < 67; j++) {
				page(posX + j, posY + i, pgm_read_byte(&deathScreen[i][j]));
			}
		}
		return;

	case (ENTRY_NAME):
		drawScreen(WHITE_SCREEN);

		// Converting the score into string
		char str[7] = { "" }; // First 0, never reach 1 million coins, just astetics

		if (score < 100000) {
			strcat(str, "0");
		}
		if (score < 10000) {
			strcat(str, "0");
		}
		if (score < 1000) {
			strcat(str, "0");
		}
		if (score < 100) {
			strcat(str, "0");
		}
		if (score < 10) {
			strcat(str, "0");
		}

		char tmp[7];
		itoa(score, tmp, 10);
		strcat(str, tmp);

		// Drawing name entry screen
		uint8_t posX1 = 30;
		uint8_t posX2 = 125;
		
		posY = 10; // posY in amount of pages

		char text8[] = "DU HAST ";
		strcat(text8, str);
		strcat(text8, " PUNKTE");
		char text9[] = "ERREICHT.";
		char text10[] = "GEBE DEINEN NAMEN EIN:";

		drawText(posX1, posY - 9, text8, 21);
		drawText(posX1, posY - 7, text9, 9);
		drawText(posX1, posY - 4, text10, 22);

		// Draw name entry castles
		uint8_t i, j;
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 9; j++) {
				page(posX1 + j, posY + i, pgm_read_byte(&castle_sprite[i][j]));
				page(posX2 + j, posY + i, pgm_read_byte(&castle_sprite[i][j]));
			}
		}

		// Draw name entry underline
		for (i = posX1; i < posX2 + 9; i++) {
			page(i, posY + 2, 0x03);
		}

		// Draw alphabet A - T
		uint8_t posX_alph = 42;
		char begin = 65; // char A
		for (i = 0; i < 10; i++) {
			drawLetter(i * 8 + posX_alph, posY + 4, begin + i);
			drawLetter(i * 8 + posX_alph, posY + 8, begin + i + 10);
		}

		// Draw alphabet U - Z
		for (i = 0; i < 6; i++) {
			drawLetter(i * 8 + posX_alph, posY + 12, begin + i + 20);
		}

		// Draw backspace arrow
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 11; j++) {
				page(6 * 8 + posX_alph + j, posY + 12 + i, pgm_read_byte(&back_sprite[i][j]));
			}
		}

		// Draw OK button
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 12; j++) {
				page(6 * 8 + posX_alph + 15 + j, posY + 12 + i, pgm_read_byte(&ok_sprite[i][j]));
			}
		}

		drawSelectArrow(65);

		return;

	case (SCOREBOARD):
		drawScreen(WHITE_SCREEN);

		uint8_t posY = 2;
		uint8_t posX_name = 10;
		uint8_t posX_score = 110;

		char text11[] = "BESTENLISTE";
		drawText(50, posY, text11, 11);

        char name[13] = "MAXJANMARCEL";
        drawText(posX_name, posY + 3, name, 12);
        drawText(posX_score, posY + 3, "INFINIT", 7);
        
		char tmp_obj[13] = { "" };

		for (i = 0; i < maxScoreboardPlayer; i++) {
			tmp_obj[0] = '\0';

			char letter;
			for (j = 0; j < maxNameLength; j++) {
				letter = eeprom_read_byte((uint8_t*)((i * maxNameLength) + j));
				tmp_obj[j] = letter;
			}
			drawText(posX_name, posY + 3 + (i + 1) * 3, tmp_obj, maxNameLength);


			tmp_obj[0] = '\0';
			uint16_t* scorePtr = (uint16_t*)(maxScoreboardPlayer * maxNameLength);
			uint16_t scoreInProm = eeprom_read_word(scorePtr + i);

			if (scoreInProm < 100000) {
				strcat(tmp_obj, "0");
			}
			if (scoreInProm < 10000) {
				strcat(tmp_obj, "0");
			}
			if (scoreInProm < 1000) {
				strcat(tmp_obj, "0");
			}
			if (scoreInProm < 100) {
				strcat(tmp_obj, "0");
			}
			if (scoreInProm < 10) {
				strcat(tmp_obj, "0");
			}

			char tmp[7];
			itoa(scoreInProm, tmp, 10);
			strcat(tmp_obj, tmp);

			drawText(posX_score, posY + 3 + (i + 1) * 3, tmp_obj, scoreLength);
		}
		return;

	default:
		return;
	}
}


void drawSelectArrow(const uint8_t field) {
	uint8_t posX = 42;
	uint8_t posY = 16;
	uint8_t i = 65;
	uint8_t j;

	if (field > 74) {
		posY = 20;
		i = 75;
	}
	if (field > 84) {
		posY = 24;
		i = 85;
	}

	while (i < field) {
		posX += 8;
		i++;
	}

	if (field == 91) {
		posX = 94;
	}

	if (field == 92) {
		posX = 109;
	}

	for (i = 0; i < 2; i++) {
		for (j = 0; j < 5; j++) {
			page(posX + j, posY + i, pgm_read_byte(&selection_arrow[i][j]));
		}
	}
}


void deleteSelectArrow(const uint8_t field) {
	uint8_t posX = 42;
	uint8_t posY = 16;
	uint8_t i = 65;

	if (field > 74) {
		posY = 20;
		i = 75;
	}
	if (field > 84) {
		posY = 24;
		i = 85;
	}

	while (i < field) {
		posX += 8;
		i++;
	}

	if (field == 91) {
		posX = 94;
	}

	if (field == 92) {
		posX = 109;
	}

	deleteRectangleAt(5, 8, posX, posY);
}


uint8_t getScorePosition() {
	uint8_t i = maxScoreboardPlayer;
	uint16_t* scorePtr = (uint16_t*)(maxScoreboardPlayer * maxNameLength);
	while (score > eeprom_read_word(scorePtr + i - 1) && i > 0) {
		i--;
	}
    return i;
}


void prepareProm(const uint8_t scorePos) {
	uint8_t i;
	uint8_t* writePtr8 = (uint8_t*)((maxScoreboardPlayer * maxNameLength) - 1);
	for (i = (maxNameLength * scorePos); i < ((maxScoreboardPlayer - 1) * maxNameLength); i++) {
		eeprom_write_byte(writePtr8, eeprom_read_byte(writePtr8 - maxNameLength));
		writePtr8--;
	}

	uint16_t* writePtr16 = (uint16_t*)(maxScoreboardPlayer * maxNameLength);
	for (i = maxScoreboardPlayer - 1; i > scorePos; i--) {
		eeprom_write_word(writePtr16 + i, eeprom_read_word(writePtr16 + i - 1));
	}
}


const uint8_t castle_sprite[2][9] PROGMEM = {
	{0xFF,0x0F,0x3C,0x3F,0x0F,0x3F,0x3C,0x0F,0xFF},
	{0x03,0xFF,0x00,0xF0,0xFC,0xF0,0x00,0xFF,0x03}
};

const uint8_t back_sprite[2][11] PROGMEM = {
	{0x00,0xC0,0x30,0x0C,0x3C,0xCC,0x0C,0xCC,0x3C,0x0C,0xFC},
	{0x03,0x0C,0x30,0xC0,0xF0,0xCC,0xC3,0xCC,0xF0,0xC0,0xFF}
};

const uint8_t ok_sprite[2][12] PROGMEM = {
	{0xFC,0x0C,0xCC,0x3C,0x3C,0xCC,0x0C,0xFC,0x0C,0xFC,0x0C,0xFC},
	{0xFF,0xC0,0xCF,0xF0,0xF0,0xCF,0xC0,0xFF,0xC3,0xFC,0xC0,0xFF}
};

const uint8_t selection_arrow[2][5] PROGMEM = {
	{0xC0,0x30,0xFC,0x30,0xC0},
	{0x00,0x00,0x3F,0x00,0x00}
};
