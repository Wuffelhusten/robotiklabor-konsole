/* 
 *	Basis
 *	2009 Benjamin Reh
 */ 
#ifndef ADC_H
#define ADC_H

#include <inttypes.h>

//initialize ADC
void ADCInit(uint8_t kanal);

//Read adc-value (10 bit)
uint16_t getADCValue(uint8_t kanal);

// generate true 8-bit random int
uint8_t generateRand(uint8_t min, uint8_t max);

#endif
