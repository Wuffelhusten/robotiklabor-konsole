#include <inttypes.h>
#include "adc.h"
#include "map.h"
#include "uart.h"
#include "timer.h"

#include <util/delay.h>

char roomsOnLevel[] = { "" };
char tmpRoom[] = { "" };

uint8_t levelIDcounter = 0;
uint8_t roomIDcounter = 0;
uint8_t portalCreated = 0;
uint8_t fieldType = FREE;

uint8_t merchantItems[3] = {0,0,0}; // the items the merchant sells

void levelInit(Level* l, Player * p) {
    roomsOnLevel[0] = '\0';
    tmpRoom[0] = '\0';
	roomIDcounter = 0;
	portalCreated = 0;
	fieldType = FREE;

    p->position[0] = (map_x_length - 1) / 2;
	p->position[1] = (map_y_height - 1) / 2;
	
	if ((levelIDcounter + 1) % 3 == 0) {
	    generateBossLevel(l);
	    l->room.map[p->position[0]][p->position[1]] = PLAYER;
	    return;
	}
	
	l->amountOfRooms = generateRand(minRoomsPerLevel, maxRoomsPerLevel);
    generateMapLayout(l);

	uint8_t ID = ((mapLayoutSize - 1) / 2) * mapLayoutSize + ((mapLayoutSize - 1) / 2);
	uint8_t doors = l->mapLayout[(mapLayoutSize - 1) / 2][(mapLayoutSize - 1) / 2];
	if (levelIDcounter == 0) {
	    l->room = generateRoom(START_ROOM, ID, doors); // Start Room
	} else {
	    l->room = generateRoom(STANDARD_ROOM, ID, doors);
	}
	l->currentRoomID = l->room.ID; //Startroomposition aka middlepoint of the mapLayout
	l->room.map[p->position[0]][p->position[1]] = PLAYER;
	
	l->portalExists = 0;
	l->merchantExists = 0;
	
	// setup the items the merchant has
	merchantItems[0] = generateRand(1,9);
	merchantItems[1] = generateRand(1,9);
	merchantItems[2] = generateRand(1,9);
	// ------------------
	
	levelIDcounter++;
	resetUart();
}


void generateBossLevel(Level* l) {
    l->amountOfRooms = 3;
    
    // Generating Map Layout for Boss Level
    uint8_t i, j;
    for (i = 0; i < mapLayoutSize; i++) {
        for (j = 0; j < mapLayoutSize; j++) {
            l->mapLayout[i][j] = 0;
        }
    }
    
    l->mapLayout[4][4] = 2;
    l->mapLayout[4][3] = 2;//10;
    l->mapLayout[4][2] = 0;//8;
    
	l->room = generateRoom(BOSS_START_ROOM, (4 * mapLayoutSize + 4), 2);
	drawText(36, 4, "BOSS AHEAD", 10);
	
	l->currentRoomID = l->room.ID; //Startroomposition aka middlepoint of the mapLayout
    
    l->portalExists = 1;
    l->merchantExists = 1;
    levelIDcounter++;
}


Room generateRoom(RoomType type, const uint8_t ID, const uint8_t doors) {
	currentEnemyID = 0;
	roomIDcounter++;

	Room room;

	room.ID = ID;
	room.type = type;
	room.size[0] = generateRand(minRoomWidth, map_x_length);
	room.size[0] = (room.size[0] % 2 == 0) ? room.size[0] + 1 : room.size[0];
	room.size[1] = generateRand(minRoomHeight, map_y_height);
	room.size[1] = (room.size[1] % 2 == 0) ? room.size[1] + 1 : room.size[1];

	room.doors = doors;

    //uint8_t numEn = (room.size[0] + room.size[1]) / 2 -5 < maxEnemiesPerRoom ? (room.size[0] + room.size[1]) / 2 -5 : maxEnemiesPerRoom;
	room.enemiesAlive = generateRand(1, (room.size[0] + room.size[1]) / 2 -5 < maxEnemiesPerRoom ? (room.size[0] + room.size[1]) / 2 -5 : maxEnemiesPerRoom); //maxEnemiesPerRoom);

	if (type == START_ROOM) {
		room.size[0] = 15;
		room.size[1] = 13;
		room.enemiesAlive = 1;
	}
	if (type == PORTAL_ROOM) {
	    if (levelIDcounter % 3 == 0) {
	        room.enemiesAlive = 0;
	    }
	}
    if (type == MERCHANT_ROOM) {
    	room.size[0] = 15;
		room.size[1] = 13;
		room.enemiesAlive = 0;
    }
    if (type == BOSS_ROOM) {
        room.size[0] = 15;
		room.size[1] = 12;
		room.enemiesAlive = 1;

		room.startPos[0] = (map_x_length - room.size[0]) / 2;
		room.startPos[1] = (map_y_height - room.size[1]) / 2 + 1;
		room.endPos[0] = (map_x_length + room.size[0]) / 2 - 1;
		room.endPos[1] = (map_y_height + room.size[1]) / 2 - 1;

		writeRoomFields(&room);
		buildGrid(&room);
		drawText(40,0,"BOSS: 10", 8);
		return room;
    }
    if (type == BOSS_START_ROOM) {
        room.size[0] = 9;
        room.size[1] = 7;
        room.enemiesAlive = 0;
    }
    
	room.startPos[0] = (map_x_length - room.size[0]) / 2;
	room.startPos[1] = (map_y_height - room.size[1]) / 2;
	room.endPos[0] = (map_x_length + room.size[0]) / 2 - 1;
	room.endPos[1] = (map_y_height + room.size[1]) / 2 - 1;

	writeRoomFields(&room);
	setObstaclesAndTraps(&room);
	
	if (type == MERCHANT_ROOM) {        
        generateMerchant(&room);
	}
	
	
	buildGrid(&room);
    
	return room;
}
// helper fct for seeding, maybe put this in adc files... ranges in [min, max] NOT (min,max)
uint8_t randWithin(uint8_t min, uint8_t max){
	return (rand() % (max +1 - min)) + min;
}

// uses room attributes eg ID, size, ... to seed traps & obstacles and places them
void setObstaclesAndTraps(Room* room){
	if(room->type != STANDARD_ROOM){
		return;
	}
	uint16_t seed = room->ID + levelIDcounter * maxRoomsPerLevel * room->doors;
	srand(seed);
	uint8_t i, tmpX, tmpY;
	// obstacles
	uint8_t limit = randWithin(0,maxObstaclesPerRoom < room->size[0] * room->size[1] / 20 ? maxObstaclesPerRoom : room->size[0] * room->size[1] / 20);
	for(i = 0; i< limit; i++){
		tmpX = randWithin(room->startPos[0]+2, room->endPos[0] -2);
		tmpY = randWithin(room->startPos[1]+2, room->endPos[1] -2);
		if(room->map[tmpX][tmpY] == FREE){
			room->map[tmpX][tmpY] = OBSTACLE;
		} 
	}
	// traps
	limit = randWithin(0,maxTrapsPerRoom < room->size[0] * room->size[1] / 40 ? maxTrapsPerRoom : room->size[0] * room->size[1] / 20);
	for(i = 0; i< limit; i++){
		tmpX = randWithin(room->startPos[0]+2, room->endPos[0] -2);
		tmpY = randWithin(room->startPos[1]+2, room->endPos[1] -2);
		if(room->map[tmpX][tmpY] == FREE){
			room->map[tmpX][tmpY] = TRAP;
		} 
	}
	
}

void writeRoomFields(Room* room) {
	uint8_t x, y;

    uint8_t doors = room->doors;
    
	// set white fields outside the room
	for (x = 0; x < map_x_length; x++) {
		for (y = 0; y < map_y_height; y++) {
			room->map[x][y] = WHITE;
		}
	}
	// set the horizontal walls of the room
	for (x = room->startPos[0]; x <= room->endPos[0]; x++) {
		room->map[x][room->startPos[1]] = INDESTRUCTIBLE;
		room->map[x][room->endPos[1]] = INDESTRUCTIBLE;
	}

	// set the vertical walls of the room
	for (y = room->startPos[1]; y <= room->endPos[1]; y++) {
		room->map[room->startPos[0]][y] = INDESTRUCTIBLE;
		room->map[room->endPos[0]][y] = INDESTRUCTIBLE;
	}

	// set the inside of the room
	for (x = room->startPos[0] + 1; x < room->endPos[0]; x++) {
		for (y = room->startPos[1] + 1; y < room->endPos[1]; y++) {
			room->map[x][y] = FREE;
		}
	}

    uint8_t open_close = room->enemiesAlive == 0 ? DOOROPEN : DOORCLOSED;

	// set the door fields
	if (doors >= 8) { // unten
		room->map[(map_x_length - 1) / 2][room->endPos[1]] = open_close;
		doors -= 8;
	}
	if (doors >= 4) { // links
		room->map[room->startPos[0]][(map_y_height - 1) / 2] = open_close;
		doors -= 4; 
	}
	if (doors >= 2) { // oben
		room->map[(map_x_length - 1) / 2][room->startPos[1]] = open_close;
		doors -= 2;
	}
	if (doors >= 1) { // rechts
		room->map[room->endPos[0]][(map_y_height - 1) / 2] = open_close;
		doors -= 1;
	}
}


void buildGrid(const Room* room) {
	uint8_t i, j;
	for (i = 0; i < map_x_length; i++) {
		for (j = 0; j < map_y_height; j++) {
			drawField(i, j, room->map[i][j]);
		}
	}
}


void drawField(const uint8_t x, const uint8_t y, const uint8_t type) {
	uint8_t i, j;
	uint8_t hex = 0x00;
    
	switch (type) {
	case FREE:
		hex = ((x % 2 == 0 && y % 2 == 0) || (x % 2 == 1 && y % 2 == 1)) ? 0x55 : 0x00;
		break;

	case INDESTRUCTIBLE:
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 8; j++) {
				page(columnsPerField * x + j, rowsPerField * y + i, pgm_read_byte(&wall_sprite[i][j]));
			}
		}
		return;

	case WHITE:
		hex = 0x00;
		break;

	case DOOROPEN:
		hex = 0x00;
		break;

	case DOORCLOSED:
		if (x == (map_x_length - 1) / 2) { // Door is up or down
			for (i = 0; i < columnsPerField; i++) {
				page(columnsPerField * x + i, rowsPerField * y, door_horizontal[0][i]);
				page(columnsPerField * x + i, rowsPerField * y + 1, door_horizontal[1][i]);
			}
		}
		else { // Door is left or right
			for (i = 0; i < columnsPerField; i++) {
				page(columnsPerField * x + i, rowsPerField * y, door_vertical[0][i]);
				page(columnsPerField * x + i, rowsPerField * y + 1, door_vertical[1][i]);
			}
		}
		return;
	
	case BUYFIELD:
	    for (i = 0; i < 2; i++) {
	        for (j = 0; j < 8; j++) {
	            page(columnsPerField * x + j, rowsPerField * y + i, pgm_read_byte(&buyField_sprite[i][j]));
	        }
	    }
	    return;

	case MERCHANT_TABLE:
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 8; j++) {
				page(columnsPerField * x + j, rowsPerField * y + i, pgm_read_byte(&table_sprite[i][j]));
			}
		}
		return;
	case OBSTACLE:
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 8; j++) {
				page(columnsPerField * x + j, rowsPerField * y + i, pgm_read_byte(&obstacle_sprite[i][j]));
			}
		}
		return;
	case TRAP:
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 8; j++) {
				page(columnsPerField * x + j, rowsPerField * y + i, pgm_read_byte(&trap_sprite[i][j]));
			}
		}
		return;


    default:
        return;
	}

	for (i = 0; i < columnsPerField; i++) {
		page(columnsPerField * x + i, rowsPerField * y, hex);
		page(columnsPerField * x + i, rowsPerField * y + 1, hex);
	}
}

void freeRectangle(Room* room, const uint8_t sizeX, const uint8_t sizeY, const uint8_t x, const uint8_t y){ // used to set the fields of an old enemy pos to free and draw them free
	uint8_t i,j;
	for(i=0; i<sizeX; i++){
		for(j=0; j<sizeY; j++){
			room->map[x+i][y+j] = FREE;
			drawField(x+i,y+j,FREE);
		}
	}
}
void setRectangleToEnemy(Room* room, Enemy* e){ // used to set the fields of an enemy to its ID
	uint8_t i,j;
	for(i=0; i<e->size[0]; i++){
		for(j=0; j<e->size[1]; j++){
			room->map[e->position[0]+i][e->position[1]+j] = e->ID;
		}
	}
}


void generatePortal(Room* room) {
    uint8_t midX = (map_x_length - 1) / 2;
    uint8_t midY = (map_y_height - 1) / 2;
    uint8_t i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            if (room->map[midX - 1 + i][midY - 1 + j] == PLAYER) {
                fieldType = PORTAL_SPRITE;
            } else {
                room->map[midX - 1 + i][midY - 1 + j] = PORTAL_SPRITE;
            }
        }
    }
    if (room->map[midX][midY] == PLAYER) {
        fieldType = PORTAL;
    } else {
        room->map[midX][midY] = PORTAL;
    }
    drawPortal(room, 0);
}


void drawPortal(Room* room, const uint8_t noOfMovements) {
    uint8_t midX = (map_x_length - 1) / 2;
    uint8_t midY = (map_y_height - 1) / 2;
    uint8_t posX = (midX - 1) * columnsPerField;
    uint8_t posY = (midY - 1) * rowsPerField;
    
    uint8_t i, j;
    if (room->map[midX - 1][midY - 1] == PLAYER || room->map[midX - 1][midY] == PLAYER || room->map[midX - 1][midY + 1] == PLAYER
    || room->map[midX][midY - 1] == PLAYER || room->map[midX][midY] == PLAYER || room->map[midX][midY + 1] == PLAYER
    || room->map[midX + 1][midY - 1] == PLAYER || room->map[midX + 1][midY] == PLAYER || room->map[midX + 1][midY + 1] == PLAYER) {
    
        for (i = 0; i < 6; i++) {
            for (j = 0; j < 24; j++) {
                if (room->map[midX - 1][midY - 1] == PLAYER && i < 2 && j < 8) {
                    continue;
                } else if (room->map[midX - 1][midY] == PLAYER && i >= 2 && i < 4 && j < 8) {
                    continue;
                } else if (room->map[midX - 1][midY + 1] == PLAYER && i >= 4 && j < 8) {
                    continue;
                } else if (room->map[midX][midY - 1] == PLAYER && i < 2 && j >= 8 && j < 16) {
                    continue;
                } else if (room->map[midX][midY] == PLAYER && i >= 2 && i < 4 && j >= 8 && j < 16) {
                    continue;
                } else if (room->map[midX][midY + 1] == PLAYER && i >= 4 && j >= 8 && j < 16) {
                    continue;
                } else if (room->map[midX + 1][midY - 1] == PLAYER && i < 2 && j >= 16) {
                    continue;
                } else if (room->map[midX + 1][midY] == PLAYER && i >= 2 && i < 4 && j >= 16) {
                    continue;
                } else if (room->map[midX + 1][midY + 1] == PLAYER && i >= 4 && j >= 16) {
                    continue;
                }
                
                page(posX + j, posY + i, noOfMovements % 2 == 0 ?
                    pgm_read_byte(&portal_sprite1[i][j]) : pgm_read_byte(&portal_sprite2[i][j]));
            }
        }
    } else {
        for (i = 0; i < 6; i++) {
            for (j = 0; j < 24; j++) {
                page(posX + j, posY + i, noOfMovements % 2 == 0 ?
                    pgm_read_byte(&portal_sprite1[i][j]) : pgm_read_byte(&portal_sprite2[i][j]));
            }
        }
    }
}


void generateMerchant(Room* room) {
    uint8_t midX = (map_x_length - 1) / 2;
    uint8_t midY = (map_y_height - 1) / 2;
    
    uint8_t i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            room->map[midX - 1 + i][midY - 3 + j] = MERCHANT;
        }
    }
    
    for (i = 0; i < 5; i++) {
        for (j = 0; j < 2; j++) {
            room->map[midX - 2 + i][midY + j] = MERCHANT_TABLE;
        }
    }
    
    room->map[midX][midY + 2] = BUYFIELD;
    drawMerchant(room, 0);
}


void drawMerchant(Room* room, uint8_t noOfMovements) {
    uint8_t midX = (map_x_length - 1) / 2;
    uint8_t midY = (map_y_height - 1) / 2;
    uint8_t posX = (midX - 1) * columnsPerField;
    uint8_t posY = (midY - 3) * rowsPerField;
    
    uint8_t i, j;
    for (i = 0; i < 6; i++) {
        for (j = 0; j < 24; j++) {
            page(posX + j, posY + i, noOfMovements % 2 == 0 ?
                pgm_read_byte(&merchant_sprite1[i][j]) : pgm_read_byte(&merchant_sprite2[i][j]));
        }
    }
}


void generateEnemies(Enemy* enemies, Room* room, const Player* p) {
	if(room->type == BOSS_ROOM){ // -> create a Boss!
		createSlimeBoss(&enemies[0]);
		enemies[0].position[0] = (map_x_length -1 - enemies[0].size[0]) / 2;
		enemies[0].position[1] = (map_y_height -1 - enemies[0].size[1]) / 2;
		uint8_t i,j; // set Fields to BOSS
		for(i=0;i<enemies[0].size[0];i++){
			for(j=0;j<enemies[0].size[1];j++){
				room->map[enemies[0].position[0]+i][enemies[0].position[1]+j] = enemies[0].ID;
			}
		}
	
		return;
	}

    uint8_t i, tmp;
    tmp = room->enemiesAlive;
    for (i = 0; i < tmp; i++) {
        createEnemy(&enemies[i], room->type, levelIDcounter);
        calculateEnemyPos(room, p, &enemies[i]);
    }
}


void calculateEnemyPos(Room* room, const Player* p, Enemy* e) {
	uint8_t posX, posY, i, j;
	uint8_t acceptablePos = 0;
	uint32_t time = getMsTimer();
	while (!acceptablePos) {
		//for(k = 0; k < 10; k++){
		if(time + 200 < getMsTimer()){
		    e->ID = DEADENEMY;
		    room->enemiesAlive--;
		    return;
		}
		acceptablePos = 1;
		srand(getMsTimer());
		posX = (rand() % (room->endPos[0] - 1) - (e->size[0] - 1) - (room->startPos[0] + 1)) + (room->startPos[0] + 1);
		//generateRand(room->startPos[0] + 1, (room->endPos[0] - 1) - (e->size[0] - 1));
		posY = (rand() % (room->endPos[1] - 1) - (e->size[1] - 1) - (room->startPos[1] + 1)) + (room->startPos[1] + 1);
		//generateRand(room->startPos[1] + 1, (room->endPos[1] - 1) - (e->size[1] - 1));
		for (i = 0; i < e->size[0]; i++) {
			for (j = 0; j < e->size[1]; j++) {
				if (room->map[posX + i][posY + j] != FREE || // field occupied
					((posX + i >= p->position[0] - 2 && posX + i <= p->position[0] + 2) && // or too close to player
					(posY + j >= p->position[1] - 2 && posY + j <= p->position[1] + 2))) {
					acceptablePos = 0;
				}
			}
		}
	}

	e->position[0] = posX;
	e->position[1] = posY;
	for (i = 0; i < e->size[0]; i++) {
		for (j = 0; j < e->size[1]; j++) {
			room->map[e->position[0] + i][e->position[1] + j] = e->ID;
		}
	}
}


void drawOpenDoor(Room* room) {
    uint8_t doors = room->doors;
	// set the door fields
	if (doors >= 8) { // unten
		room->map[(map_x_length - 1) / 2][room->endPos[1]] = DOOROPEN;
		drawField((map_x_length - 1) / 2, room->endPos[1], DOOROPEN);
		doors -= 8;
	}
	if (doors >= 4) { // links
		room->map[room->startPos[0]][(map_y_height - 1) / 2] = DOOROPEN;
		drawField(room->startPos[0], (map_y_height - 1) / 2, DOOROPEN);
		doors -= 4;
	}
	if (doors >= 2) { // oben
		room->map[(map_x_length - 1) / 2][room->startPos[1]] = DOOROPEN;
		drawField((map_x_length - 1) / 2, room->startPos[1], DOOROPEN);
		doors -= 2;
	}
	if (doors >= 1) { // rechts
		room->map[room->endPos[0]][(map_y_height - 1) / 2] = DOOROPEN;
		drawField(room->endPos[0], (map_y_height - 1) / 2, DOOROPEN);
		doors -= 1;
	}
}

void generateMapLayout(Level *l){
    uint8_t h,i,j,k;
    uint8_t dungeon[mapLayoutSize][mapLayoutSize];
    
    for(i=0;i<mapLayoutSize;i++){
		for(j=0;j<mapLayoutSize;j++){
		    dungeon[i][j] = 0; //init map
		    l->mapLayout[i][j] = 0;
		}
	}
	
    k = (mapLayoutSize-1)/2;	
	dungeon[k][k] = 1;
	dungeon[k-1][k] = 1;
	dungeon[k+1][k] = 1;
	dungeon[k][k-1] = 1;
	dungeon[k][k+1] = 1;
	k = l->amountOfRooms - 5;
	
	srand(getMsTimer());
	// setup rooms
    while(k > 0){
        h = 0;
        i = rand() % mapLayoutSize;  //generateRand(1,mapLayoutSize - 1);
        j = rand() % mapLayoutSize;  //generateRand(1,mapLayoutSize - 1);
        if(dungeon[i][j] == 1){
			continue;
		}else{ 
			if(dungeon[i+1][j] == 1){
				h++;
			}
			if(dungeon[i-1][j] == 1){
				h++;
			}
			if(dungeon[i][j+1] == 1){
				h++;
			}
			if(dungeon[i][j-1] == 1){
				h++;
			}
			if(h == 1){
				dungeon[i][j] = 1;
				k--;
			}
		}
    }
    //setup doors without the edges
    for(i=1;i<mapLayoutSize-1;i++){
        for(j=1;j<mapLayoutSize-1;j++){
            if(dungeon[i][j] == 1){
                if(dungeon[i][j+1] == 1){ //unten
                    l->mapLayout[i][j] += 8;
                }
                if(dungeon[i][j-1] == 1){ //oben
                    l->mapLayout[i][j] += 2;
                }
                if(dungeon[i+1][j] == 1){ //rechts
                    l->mapLayout[i][j] += 1;
                }   
                if(dungeon[i-1][j] == 1){ //links
                    l->mapLayout[i][j] += 4;
                }
            }else{
                continue;
            }
        }
    }
    //setup doors of the edges of the mapLayout
    for(i=0;i<mapLayoutSize;i++){ // the edges of the maplayout, bc i-1 may be oob
        if(dungeon[i][mapLayoutSize-1] == 1){ // unten
            if(dungeon[i][mapLayoutSize-1-1] == 1){ //oben
                l->mapLayout[i][mapLayoutSize-1] += 2;
            }
            if(dungeon[i+1][mapLayoutSize-1] == 1){ //rechts
                l->mapLayout[i][mapLayoutSize-1] += 1;
            }   
            if(dungeon[i-1][mapLayoutSize-1] == 1){ //links
                l->mapLayout[i][mapLayoutSize-1] += 4;
            }
        }
        if(dungeon[i][0]){ //oben
            if(dungeon[i][0+1] == 1){ //unten
                l->mapLayout[i][0] += 8;
            }
            if(dungeon[i+1][0] == 1){ //rechts
                l->mapLayout[i][0] += 1;
            }   
            if(dungeon[i-1][0] == 1){ //links
                l->mapLayout[i][0] += 4;
            }        
        }
        if(dungeon[mapLayoutSize-1][i]){ //rechts
            if(dungeon[mapLayoutSize-1][i+1] == 1){ //unten
                l->mapLayout[mapLayoutSize-1][i] += 8;
            }
            if(dungeon[mapLayoutSize-1][i-1] == 1){ //oben
                l->mapLayout[mapLayoutSize-1][i] += 2;
            }
            if(dungeon[mapLayoutSize-1-1][i] == 1){ //links
                l->mapLayout[mapLayoutSize-1][i] += 4;
            }        
        }
        if(dungeon[0][i]){ //links
            if(dungeon[0][i+1] == 1){ //unten
                l->mapLayout[0][i] += 8;
            }
            if(dungeon[0][i-1] == 1){ //oben
                l->mapLayout[0][i] += 2;
            }
            if(dungeon[0+1][i] == 1){ //rechts
                l->mapLayout[0][i] += 1;
            }
        }
    }	
}


// ROOM SAVING AND RELOADING

// returns a pointer to the first occurence of the room id in global room string
char* getRoomData(uint8_t ID) {
    tmpRoom[0] = '\0';
    
	char str[maxRoomsPerLevel * 9];
	strcpy(str, roomsOnLevel);

	// room id to char[] conversion; PROBLEM: id < 10 (solved)
	char searchID[4] = { 'r' };
	if (ID < 10) {
		strcat(searchID, "0");
	}
	char id[3];
	itoa(ID, id, 10);
	strcat(searchID, id);

	char* ptr;
	ptr = strstr(str, searchID);

	if (ptr == NULL) {
		return ptr;
	}

	ptr = strtok(ptr, "r");

	char tmp[9];
	strcpy(tmp, ptr);
	strcpy(tmpRoom, tmp);

	char* ptr1;
	ptr1 = tmpRoom;
	return ptr1;
}


void saveRoom(Room* room) {
	// SAVING THE ID
	char tmp[10] = { 'r' };
	if (room->ID < 10) {
		strcat(tmp, "0");
	}
	char id[3];
	itoa(room->ID, id, 10);
	strcat(tmp, id);

	// SAVING THE ROOM SIZE
	if (room->size[0] < 10) {
		strcat(tmp, "0");
	}
	id[0] = '\0';
	itoa(room->size[0], id, 10);
	strcat(tmp, id);

	if (room->size[1] < 10) {
		strcat(tmp, "0");
	}
	id[0] = '\0';
	itoa(room->size[1], id, 10);
	strcat(tmp, id);

    id[0] = '\0';
	itoa(room->type, id, 10);
	strcat(tmp, id);

	strcat(roomsOnLevel, tmp);
}


Room reloadRoom(Level* l, uint8_t ID, uint8_t doors) {
    
	Room room;

	room.ID = ID;
	room.doors = doors;
	
	char t[8] = { "" };
	strcat(t, getRoomData(ID));
	char buffer[3] = { "" };
	
	//Getting the type
	strcpy(buffer, &t[6]);
	room.type = atoi(buffer);
	t[6] = '\0';
	
	//Getting the room height
	strcpy(buffer, &t[4]);
	room.size[1] = atoi(buffer);
	t[5] = '\0';
	t[4] = '\0';	
	
	//Getting the room width
	strcpy(buffer, &t[2]);
	room.size[0] = atoi(buffer);

    //Calculating the start and endposition
	room.startPos[0] = (map_x_length - room.size[0]) / 2;
	room.startPos[1] = (map_y_height - room.size[1]) / 2;
	room.endPos[0] = (map_x_length + room.size[0]) / 2 - 1;
	room.endPos[1] = (map_y_height + room.size[1]) / 2 - 1;

	room.enemiesAlive = 0;
	room.roomChanged = 0;

	writeRoomFields(&room);
	setObstaclesAndTraps(&room);
	
	if (room.type == PORTAL_ROOM) {
	    generatePortal(&room);
	}
	
	if (room.type == MERCHANT_ROOM) {
	    uint8_t midX = (map_x_length - 1) / 2;
        uint8_t midY = (map_y_height - 1) / 2;
        
	    uint8_t i, j;
	    for (i = 0; i < 5; i++) {
            for (j = 0; j < 2; j++) {
                room.map[midX - 2 + i][midY + j] = INDESTRUCTIBLE;
            }
        }
        
        generateMerchant(&room);
	}
	
	buildGrid(&room);
    
	return room;
}

uint8_t isFree(const Room* room, const uint8_t x, const uint8_t y, const uint8_t sizeX, const uint8_t sizeY){
	uint8_t i,j;
	for(i=0; i<sizeX; i++){
        for(j=0; j<sizeY; j++){
            if(room->map[x+i][y+j] != FREE){
               return 0; 
            }
        }
    }
    return 1;
}

uint8_t isWallAt(const Room* room, const uint8_t x, const uint8_t y, const uint8_t sizeX, const uint8_t sizeY){
    uint8_t i,j;
    for(i=0; i<sizeX; i++){
        for(j=0; j<sizeY; j++){
            if(room->map[x+i][y+j] == INDESTRUCTIBLE || room->map[x+i][y+j] == DOORCLOSED || room->map[x+i][y+j] == WHITE){
               return 1; 
            }
        }
    }
    return 0;
}
uint8_t isPlayerAt(const Room* room, const uint8_t x, const uint8_t y, const uint8_t sizeX, const uint8_t sizeY){
    uint8_t i,j;
    for(i=0; i<sizeX; i++){
        for(j=0; j<sizeY; j++){
            if(room->map[x+i][y+j] == PLAYER){
               return 1; 
            }
        }
    }
    return 0;
}

//WARing: also returns 1 if the player is in Reach!!
uint8_t isFreeForEnemy(const Room* room, const uint8_t newX, const uint8_t newY, const Enemy* e){
	uint8_t x,y;
	for(x=0; x< e->size[0];x++){
		for(y=0;y<e->size[1];y++){
			if( room->map[newX+x][newY+y] != FREE && room->map[newX+x][newY+y] != e->ID && room->map[newX+x][newY+y] != PLAYER){
				return 0;
			}
		}
	}
	return 1;
} 

// sprite for doors
uint8_t door_horizontal[2][8] = {
	{0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0},
	{0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03}
};


uint8_t door_vertical[2][8] = {
	{0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00},
	{0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00}
};
