#ifndef DRAW_PLAYER
#define DRAW_PLAYER

#define maxAmountOfItems 3		// defines the maximum amount of items carried by the player at the same time
#include <avr/pgmspace.h>


typedef enum {
    DAGGER = 1,
    SWORD = 2,
    SPEAR = 3,
	BROADSWORD = 4
} WeaponType;


typedef enum {
    NOARMOR = 0,
    BOOTS = 1,
    PANTS = 2,
	HELMET = 3,
	CHESTPLATE = 4
} Armor;


typedef enum {
	// TODO: Add more items, probably add sprites to draw them freely into the room
	NOITEM = 0,
	POTION = 1
} Item;


typedef struct {
	uint8_t damage;			// defines the damage of a weapon
	const uint8_t* pattern;	// defines the fields where the enemies can be hit by the player
	const uint8_t* uiSprite1;     // part 1 of the sprite which gets shown in the weapon slot
} Weapon;


typedef struct {
	Weapon weapon;							// weapon of the player, start = dagger {2,1}
	uint16_t money;							// money of the player, start = 0
	uint8_t armor;                          // armor value of the player
	uint8_t item;		                    // item of the player like heal
} Inventory;


typedef struct {
	uint8_t position[2];		// {posX, posY} is calculated into field positions
	uint8_t health;				// health of the player given in amount of half hearts
	uint8_t maxHealth;			// maximum health of the player given in amount of half hearts
	Inventory inventory;		// Inventory of the player containing a weapon, money and items
	uint8_t* sprite;			// sprite of the player
} Player;


Player playerInit();

void drawPlayerLife(Player * player);
void drawFullHeart(uint8_t posX, uint8_t posY);
void drawHalfHeart(uint8_t posX, uint8_t posY);
void drawEmptyHeart(uint8_t posX, uint8_t posY);

Weapon createDagger();
Weapon createSpear();
Weapon createSword();
Weapon createBroadsword();

void drawUI();

void drawDagger();
void drawSword();
void drawSpear();
void drawBroadsword();

void drawPotion();

void drawBoots();
void drawPants();
void drawChestPlate();
void drawHelmet();

void drawUIslime(uint8_t posX, uint8_t posY);

void drawMoney(const uint16_t money);

void swapWeapon(Player* p, const uint8_t weapon);
void swapItem(Player* p, const uint8_t item);
void swapArmor(Player* p, const uint8_t armor);

void useItem(Player* p);

extern uint8_t character_sprite[2][8];
extern uint8_t standardPattern[3][3];
extern uint8_t longRangePattern[3][3];
extern uint8_t sweepAttackPattern[3][3];

#endif
