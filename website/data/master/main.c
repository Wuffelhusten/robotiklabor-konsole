/* 
 *	Basis
 *	2009 Benjamin Reh und Joachim Schleicher
 */
#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include <stdio.h>
#include "adc.h"
#include "buttons.h"
#include "timer.h"
#include "uart.h"

#include "movement.h"

#define moveTime 250

void init();


typedef enum {
	TITLESCREEN = 0,
	MENU = 1,
	SETUP = 2,
	PLAYING = 3,
	DEAD = 4
} PlayingState;


int main(void) {

    uint8_t mainMenuSelect = 0;
    uint8_t i = 0;
	uint8_t noOfMovements = 0;			// used to determine which enemies move on this note
	uint8_t allButtonsUp = 0;
	uint32_t uartTime = 0;
	uint8_t easyMode = 0;
	uint8_t songOfHealing = 0;

	Player player;
	Level level;
	
	PlayingState playingState = TITLESCREEN;

	//Initialisierung ausfuehren
	init();
    
    Enemy dummy;
    createEnemy(&dummy, 0,0);
    dummy.ID = DEADENEMY;
    Enemy enemies[maxEnemiesPerRoom];
    
    for (i = 0; i < maxEnemiesPerRoom; i++) {
        enemies[i] = dummy;
    }
    
    // used to clear eeprom
    /*
    uint8_t* iptr1 = 0;
    for (i = 0; i < (maxScoreboardPlayer * maxNameLength); i++) { // 0 - 59
        eeprom_write_byte(iptr1, 88);
        iptr1++;
    }
    
    uint16_t* iptr2 = (uint16_t*)(maxScoreboardPlayer * maxNameLength);
    for (i = 0; i < maxScoreboardPlayer; i++) { // 60 - 64
        eeprom_write_word(iptr2, 0);
        iptr2++;
    }
    */
    
	while (1) {

	    //TITLESCREEN ------------------------------------------- 
		if(playingState == TITLESCREEN){
			drawScreen(TITLE_SCREEN);
            drawText_pgm(53, 18, (uint8_t*)PSTR("PRESS START"), 11);
            uart_putc('l'); //song of healing
            songOfHealing = 1;
			while (1) {
				if (B_PAUSE) {
					//uart_putc('p');
					break;
				}
			}
			playingState = MENU;

        //MENU ----------------------------------------------
		}else if(playingState == MENU){
		    if (!songOfHealing) {
		        _delay_ms(10);
		        uart_putc('l');
		    }
		    uint8_t breaker = 0;
		    uint8_t instructions = 0;
		    drawScreen(MENU_SCREEN);
		    if(easyMode == 0){
		        drawScreen(MENU_EASY);
		    }else{
		        drawScreen(MENU_NORMAL);
		    }
		    drawScreen(BONFIRE_SETUP);
		    uartTime = getMsTimer();
		    drawScreen(ARROW_UP);
		    i = 0;
		    while (1) {
		        if(uartTime + 500 < getMsTimer()){
		            uartTime = getMsTimer();
		            if(i == 0){
		                drawScreen(BONFIRE2);
		                i = 1;
		            }else{
		                drawScreen(BONFIRE1);
		                i = 0;
		            }
		        }
				if (B_UP) {
					mainMenuSelect = (mainMenuSelect + 3) % 4;
					mainMenuSelect == 0 ? drawScreen(ARROW_UP) : mainMenuSelect == 1 ? drawScreen(ARROW_MIDDLE) : mainMenuSelect == 2 ? drawScreen(ARROW_MIDDLE_DOWN) : drawScreen(ARROW_BOTTOM);
					_delay_ms(200);
				}
				else if (B_DOWN) {
					mainMenuSelect = (mainMenuSelect + 1) % 4;
					mainMenuSelect == 0 ? drawScreen(ARROW_UP) : mainMenuSelect == 1 ? drawScreen(ARROW_MIDDLE) : mainMenuSelect == 2 ? drawScreen(ARROW_MIDDLE_DOWN) : drawScreen(ARROW_BOTTOM);
					_delay_ms(200);
				}
				else if (B_A || B_PAUSE || B_SELECT) {
					if (mainMenuSelect == 1) {
						drawScreen(HOW_TO_PLAY);
						instructions = 1;
						_delay_ms(200);
					}
					else if (mainMenuSelect == 2) {
						drawScreen(SCOREBOARD);
					}
					else if(mainMenuSelect == 3){
					    if(easyMode == 0){
					        easyMode = 1;
					        drawScreen(MENU_NORMAL);
					        breaker = 1;
					    }else{
					        easyMode = 0;
					        drawScreen(MENU_EASY);
					        _delay_ms(200);
					        breaker = 1;
					    }
					}
					else {
						break;
					}
					while (1) {
					    if(instructions == 1 || instructions == 2){
						    if (B_B || B_A || B_PAUSE || B_SELECT) {
						        instructions = 0;
							    break;
							}
							if(B_LEFT || B_RIGHT){
							    if(instructions == 1){
							        instructions++;
							        drawScreen(HOW_TO_PLAY2);
							        _delay_ms(200);
							    }else{
							        instructions = 1;
							        drawScreen(HOW_TO_PLAY);
							        _delay_ms(200);
							    }
							}
					    }
					    if (B_B || B_A || B_PAUSE || B_SELECT) {
					        break;
					    }
						if(breaker == 1){
						    breaker = 0;
						    break;
						}			
					}
					drawScreen(MENU_SCREEN);
					if(easyMode == 0){
		                drawScreen(MENU_EASY);
		            }else{
		                drawScreen(MENU_NORMAL);
		            }
					drawScreen(BONFIRE_SETUP);
					drawScreen(ARROW_UP);
					mainMenuSelect = 0;
				}
			}
		    playingState = SETUP;
		    
        // SETUP --------------------------------------------
		}else if(playingState == SETUP){
		    uart_putc('O');
		    _delay_ms(10);
		    uart_putc('L'); //stop song of healing
		    _delay_ms(30);
		    levelIDcounter = 0;
			score = 0;
			drawScreen(WHITE_SCREEN);

			player = playerInit();
			levelInit(&level, &player);
			
			drawCharacter(&player);
			for(i=0;i< maxEnemiesPerRoom; i++){
				enemies[i].ID = DEADENEMY; // HARD RESET of enemies, just to be sure...
			}            

            generateEnemies(enemies, &level.room, &player);
            
			for (i = 0; i < maxEnemiesPerRoom; i++) { // draw alive enemies
				drawEnemy(&enemies[i], noOfMovements);
			}
			
			noOfMovements = 0;
			allButtonsUp = 0;				// checks if player lets go of the buttons

			playingState = PLAYING;
			resetUart();
	        _delay_ms(5);
			uart_putc('r');		            // start music, put directly before move functionality
			
		// PLAYING --------------------------------------------
		}else if(playingState == PLAYING){
		    //player movement
			if (!allButtonsUp) {
				allButtonsUp = !(B_UP || B_DOWN || B_RIGHT || B_LEFT);
			}

			if (B_SELECT) {
				//uart_putc(1);
			}

			if (B_PAUSE) {
				uart_putc('Q');
				songOfHealing = 0;
				playingState = MENU;
			}
            
             
       
			if (uart_getc() == 'm') { // -> Sync-Signal from Slave-MC
			    uartTime = getMsTimer();
			    //deleteRectangleAt(12,12,134,20); // delete upper pos of slime
                drawUIslime(134,22);           // draw slime at upper pos
			    while (getMsTimer() <= uartTime + moveTime) {
				   
				    if (B_UP && (allButtonsUp || easyMode)) {
					    movePlayer(&player, &level, enemies, 0);
					    allButtonsUp = 0;
					    break;
				    }
				    else if (B_DOWN && (allButtonsUp || easyMode)) {
					    movePlayer(&player, &level, enemies,  1);
					    allButtonsUp = 0;
					    break;
				    }
				    else if (B_RIGHT && (allButtonsUp || easyMode)) {
					    movePlayer(&player, &level, enemies, 2);
					    allButtonsUp = 0;
					    break;
				    }
				    else if (B_LEFT && (allButtonsUp || easyMode)) {
					    movePlayer(&player, &level, enemies, 3);
					    allButtonsUp = 0;
					    break;
				    }
				    else if (B_A) {
					    //uart_putc('t');
					    if (player.inventory.item) {
					        useItem(&player);
					        break;
					    }
					    
				    }
				    else if (B_B) {
					    ;//uart_putc('t');
				    }
				    //}
				}

				for (i = 0; i < maxEnemiesPerRoom; i++) {
					if (enemies[i].ID == DEADENEMY) { // ID = 0; enemy is dead; REASON: there is no other NULL ptr handling
						continue;
					}
					moveEnemy(&enemies[i], noOfMovements, &level.room, &player);
				}
				
				noOfMovements = (noOfMovements + 1) % 12;
				while(1){   // wait at least 150 ms then delete slime
				    if(uartTime + moveTime < getMsTimer()){
				        break;
				    }
					if (!allButtonsUp) {
						allButtonsUp = !(B_UP || B_DOWN || B_RIGHT || B_LEFT);
					}
				}
				deleteRectangleAt(12,12,134,22); // delete lower pos of slime
				
			}
			
			
			if (level.room.enemiesAlive == 0) {
			    if (level.room.type == PORTAL_ROOM && !portalCreated) {
			        generatePortal(&level.room);
			        drawCharacter(&player);
			        portalCreated = 1;
			    } else if (level.room.type == PORTAL_ROOM && portalCreated) {
			        drawPortal(&level.room, noOfMovements);
			    }
				drawOpenDoor(&level.room);
			}
            
            if (level.room.type == MERCHANT_ROOM) {
                drawMerchant(&level.room, noOfMovements);
            }

			if (player.health == 0) {
				playingState = DEAD;
				score = player.inventory.money;
				uart_putc('q');
			}
			
		// DEAD --------------------------------------------------
		}else if(playingState == DEAD){
		    drawScreen(DEATH_SCREEN);

			_delay_ms(2000); // Wait 2 seconds

			if (getScorePosition() < maxScoreboardPlayer) {
				drawScreen(ENTRY_NAME);
				uint8_t field = 65; // OK button = 65 + 27 = 92
				uint8_t nameLength = 0;
				char name[13] = { "" };

				for (i = 0; i < maxNameLength; i++) {
					name[i] = 32;
				}

				// Name entry routine
				while (1) {
					if (B_UP) {
						deleteSelectArrow(field);
						if (field > 74) {
							field -= 10;
						}
						drawSelectArrow(field);
						_delay_ms(300);
					}
					else if (B_DOWN) {
						deleteSelectArrow(field);
						if (field < 85) {
							field += 10;
						}
						if (field > 92) {
							field = 92;
						}
						drawSelectArrow(field);
						_delay_ms(300);
					}
					else if (B_RIGHT) {
						deleteSelectArrow(field);
						field += 1;
						if (field > 92) {
							field = 65;
						}
						drawSelectArrow(field);
						_delay_ms(300);
					}
					else if (B_LEFT) {
						deleteSelectArrow(field);
						field -= 1;
						if (field < 65) {
							field = 92;
						}
						drawSelectArrow(field);
						_delay_ms(300);
					}
					else if (B_A) {
						if (field == 92) {
							if (nameLength > 0) {
								uint8_t scorePos = getScorePosition();

								prepareProm(scorePos);

								uint8_t letter;
								uint8_t* writeNamePtr = (uint8_t*)(scorePos * maxNameLength);
								for (i = 0; i < maxNameLength; i++) {
									letter = name[i];
									eeprom_write_byte(writeNamePtr + i, letter);
								}

								uint16_t* writeScorePtr = (uint16_t*)(maxScoreboardPlayer * maxNameLength);
								eeprom_write_word(writeScorePtr + scorePos, score);

								break;
							}
						}
						else if (field == 91) {
							if (nameLength > 0) {
								nameLength--;
								name[nameLength] = 32;
								deleteRectangleAt(80, 8, 42, 10);
								drawText(46, 10, name, maxNameLength);
							}
						}
						else {
							if (nameLength < 12) {
								char c = field;
								name[nameLength] = c;
								nameLength++;
								deleteRectangleAt(80, 8, 42, 10);
								drawText(46, 10, name, maxNameLength);
							}
						}

						if (nameLength == 12) {
							deleteSelectArrow(field);
							field = 92;
							drawSelectArrow(92);
						}
						_delay_ms(300);
					}
				}
			}

			drawScreen(SCOREBOARD);

			while (1) {
				if (B_PAUSE || B_A || B_B || B_SELECT) {				// player accepts death
					uart_putc('Q');
					songOfHealing = 0;
					playingState = MENU;
					break;
				}
			}
		}
	}
}


//INIT
void init()
{
	uartInit();			// serielle Ausgabe an PC
	ADCInit(0);			// Analoge Werte einlesen
	ADCInit(1);
	timerInit();		// "Systemzeit" initialisieren
	buttonsInit();
	displayInit();
}
