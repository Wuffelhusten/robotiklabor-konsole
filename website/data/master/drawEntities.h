#ifndef DRAWENTITIES_H
#define DRAWENTITIES_H

#include "map.h"

//TODO: Draw bouncing slime from the GUI

void drawCharacter(const Player* p);

void drawEnemy(const Enemy * e, const uint8_t noOfMovements);

// drawFunctions for the Merchant menu
void drawMerchantMenu(Player* p, Room* room); // pauses the game and has all the funct. for the merchant window, incl buying stuff, call in movement when player moves to buyfield
void drawMerchantItemFrame(const uint8_t posX, const uint8_t posY);
void drawSelectionArrow(const uint8_t selectionPos);
void drawIteminSlot(const uint8_t selectionPos, const uint8_t item, const uint8_t frame1X, const uint8_t frame1Y);
// TODO: Draw merchant

//--------- Possibly in map->generateRoom ----------
// TODO: Draw trap
// TODO: Draw chest
// TODO: Draw item in rooms
// TODO: Draw portal


#endif
